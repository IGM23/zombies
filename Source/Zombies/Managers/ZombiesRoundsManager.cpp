#include "ZombiesRoundsManager.h"

#include "Kismet/GameplayStatics.h"
#include "Zombies/Actors/ZombiesEnemySpawnerArea.h"
#include "Zombies/Characters/ZombiesEnemyCharacter.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"
#include "Zombies/Components/Players/ZombiesUpdateUIComponent.h"
#include "Zombies/Controllers/ZombiesEnemyBaseController.h"
#include "Zombies/DataAssets/DA_RoundsManager.h"
#include "Zombies/GameStates/ZombiesGameState.h"

void UZombiesRoundsManager::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	ZombiesCurrentlyAlive.Empty();
}

void UZombiesRoundsManager::InitializeFromDataAsset(const UDA_RoundsManager* data)
{
	EnemiesToSpawnClasses = data->EnemiesToSpawnClasses;
	EnemySpawnerAreas = data->EnemySpawnerAreas;
	ZombiesNumberIncreasePerRound = data->ZombiesNumberIncreasePerRound;
	MaximumZombiesAlive = data->MaximumZombiesAlive;
	SecondsBetweenRounds = data->SecondsBetweenRounds;
	ActivateRoundsManager = data->ActivateRoundsManager;
	RoundStartSound = data->RoundStartSound;
	RoundEndSound = data->RoundEndSound;
	GameEndSound = data->GameEndSound;
	RoundsSFXVolumeMultiplier = data->RoundsSFXVolumeMultiplier;
	ZombiesScreams = data->ZombiesScreams;
	TimeBetweenZombiesScreams = data->TimeBetweenZombiesScreams;
	DeviationTimeBetweenZombieScreams = data->DeviationTimeBetweenZombieScreams;

	CurrentTimeBetweenZombiesScreams = TimeBetweenZombiesScreams;
	GetWorld()->GetTimerManager().SetTimer(RandomZombieScreamTimerHandle,this, &UZombiesRoundsManager::PlayRandomZombieScreamLoop,
			CurrentTimeBetweenZombiesScreams,false);
	
	SetIsDataInitialized(true);
}

void UZombiesRoundsManager::StartRound()
{
	AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
	if(!ActivateRoundsManager || !gameState)
		return;
	
	gameState->ReplicateImportant2dSFX_Multicast(RoundStartSound, RoundsSFXVolumeMultiplier);
	
	GetWorld()->GetTimerManager().ClearTimer(StartRoundTimerHandle);
	CurrentRound += 1;
	UpdatePlayersRoundsHUD();
	
	TotalZombiesThisRound += (ZombiesNumberIncreasePerRound * gameState->GetNumberOfPlayers());
	RatioZombiesPerPlayer = TotalZombiesThisRound / gameState->GetNumberOfPlayers();
	
	for(int32 safetyIndex = 0; (ZombiesSpawnedThisRound < MaximumZombiesAlive) && (safetyIndex < 100); ++safetyIndex)
		SpawnZombie();
}

void UZombiesRoundsManager::FinishRound()
{
	ZombiesCurrentlyAlive.Empty();
	ZombiesSpawnedThisRound = 0;
	LastRoundZombiesHealth = LastRoundZombiesHealth * ZombiesHealthGainPerRoundMultiplier;

	AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
	if(gameState)
		gameState->ReplicateImportant2dSFX_Multicast(RoundEndSound, RoundsSFXVolumeMultiplier);
	
	//TODO: Revive all Players here, before the Round starts.

	GetWorld()->GetTimerManager().SetTimer(StartRoundTimerHandle, this, &UZombiesRoundsManager::StartRound,
		SecondsBetweenRounds,false);
}

bool UZombiesRoundsManager::CheckIfRoundFinished()
{
	return ((ZombiesSpawnedThisRound >= TotalZombiesThisRound) && ZombiesCurrentlyAlive.Num() == 0);
}

AZombiesEnemyCharacter* UZombiesRoundsManager::SpawnZombie()
{
	AZombiesEnemyCharacter* spawnedEnemy = nullptr;

	if(ZombiesSpawnedThisRound < TotalZombiesThisRound)
	{
		TArray<TSoftObjectPtr<AZombiesEnemySpawnerArea>> areasWithPlayersInside;
		ObtainAreasWithPlayersInside(areasWithPlayersInside);
		const TSoftObjectPtr<AZombiesEnemySpawnerArea> areaToSpawn = ChooseAreaToSpawnZombie(areasWithPlayersInside);
		if(areaToSpawn)
		{
			const int32 randomChosenEnemyClassIndex = FMath::RandRange(0, EnemiesToSpawnClasses.Num() - 1);
			spawnedEnemy = areaToSpawn->SpawnEnemy(EnemiesToSpawnClasses[randomChosenEnemyClassIndex].Get());
			if(spawnedEnemy->IsValidLowLevel())
			{
				++ZombiesSpawnedThisRound;
				ZombiesCurrentlyAlive.AddUnique(spawnedEnemy);
				areaToSpawn->AddZombieToThisArea(spawnedEnemy);
				spawnedEnemy->SetAreaCurrentlyIn(areaToSpawn.Get());
				UpdateZombieRoundHealth(spawnedEnemy);
			}
		}
	}

	return spawnedEnemy;
}

void UZombiesRoundsManager::OnCharacterDied(AZombiesCharacter* deadCharacter)
{
	AZombiesEnemyCharacter* deadZombie = Cast<AZombiesEnemyCharacter>(deadCharacter);
	if(deadZombie)
	{
		deadZombie->GetAreaCurrentlyIn()->RemoveZombieFromThisArea(deadZombie);
		ZombiesCurrentlyAlive.Remove(deadZombie);

		if(CheckIfRoundFinished())
			FinishRound();
		else
			SpawnZombie();
			
		return;
	}

	AZombiesPlayerCharacter* deadPlayer = Cast<AZombiesPlayerCharacter>(deadCharacter);
	if(deadPlayer)
		deadPlayer->GetAreaCurrentlyIn()->RemovePlayerFromThisArea(deadPlayer);
}

void UZombiesRoundsManager::EndAllAliveZombiesBehavior()
{
	for(AZombiesCharacter* currentZombie : ZombiesCurrentlyAlive)
	{
		AZombiesEnemyBaseController* enemyController = Cast<AZombiesEnemyBaseController>(currentZombie->GetController());
		if(enemyController)
			enemyController->EndBehavior();
	}
}

void UZombiesRoundsManager::ObtainAreasWithPlayersInside(TArray<TSoftObjectPtr<AZombiesEnemySpawnerArea>>& areasWithPlayersInside)
{
	for(TSoftObjectPtr<AZombiesEnemySpawnerArea> currentArea : EnemySpawnerAreas)
	{
		if(currentArea.Get()->AreAnyPlayersInsideThisArea())
			areasWithPlayersInside.AddUnique(currentArea);
	}
}

TSoftObjectPtr<AZombiesEnemySpawnerArea> UZombiesRoundsManager::ChooseAreaToSpawnZombie(TArray<TSoftObjectPtr<AZombiesEnemySpawnerArea>>& areas) const
{
	// We will try to fulfil the RatioZombiesPerPlayer in each area.
	for(TSoftObjectPtr<AZombiesEnemySpawnerArea> currentArea : areas)
	{
		if(currentArea->GetZombiesInThisArea().Num() / currentArea->GetPlayersInThisArea().Num() < RatioZombiesPerPlayer)
			return currentArea;
	}

	if (areas.Num() > 0)
		return areas[0];

	return nullptr;
}

void UZombiesRoundsManager::UpdateZombieRoundHealth(AZombiesCharacter* spawnedEnemy) const
{
	if(spawnedEnemy->GetCharStatsComponent())
	{
		const float currentRoundHealth = LastRoundZombiesHealth * ZombiesHealthGainPerRoundMultiplier;
		spawnedEnemy->GetCharStatsComponent()->SetMaxHealth(currentRoundHealth);
		spawnedEnemy->GetCharStatsComponent()->SetCurrentHealth(currentRoundHealth);
	}
}

void UZombiesRoundsManager::UpdatePlayersRoundsHUD() const
{
	AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
	if(gameState)
	{
		for(AZombiesPlayerCharacter* player : gameState->GetPlayerCharacters())
			player->GetUpdateUIComponent()->SetNewRoundNumber_Client(CurrentRound);
	}
}

void UZombiesRoundsManager::PlayRandomZombieScreamLoop()
{
	CurrentTimeBetweenZombiesScreams = FMath::RandRange(TimeBetweenZombiesScreams - DeviationTimeBetweenZombieScreams,
		TimeBetweenZombiesScreams + DeviationTimeBetweenZombieScreams);
	
	GetWorld()->GetTimerManager().ClearTimer(RandomZombieScreamTimerHandle);
	GetWorld()->GetTimerManager().SetTimer(RandomZombieScreamTimerHandle,this, &UZombiesRoundsManager::PlayRandomZombieScreamLoop,
			CurrentTimeBetweenZombiesScreams,false);

	AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
	if(!gameState || ZombiesCurrentlyAlive.Num() == 0 || ZombiesScreams.Num() == 0)
		return;
	
	const int32 randomScreamIndex = FMath::RandRange(0, ZombiesScreams.Num() - 1);
	const int32 randomEnemy = FMath::RandRange(0, ZombiesCurrentlyAlive.Num() - 1);
	USoundBase* chosenSFX = ZombiesScreams[randomScreamIndex];
	AZombiesEnemyCharacter* chosenEnemy = ZombiesCurrentlyAlive[randomEnemy];
	gameState->ReplicateNotImportant3dSFX_Multicast(chosenSFX, chosenEnemy);
}