#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "ZombiesRoundsManager.generated.h"

class AZombiesEnemySpawnerArea;
class AZombiesEnemyCharacter;
class UDA_RoundsManager;
class AZombiesCharacter;
class USoundBase;

UCLASS(Blueprintable, BlueprintType)
class ZOMBIES_API UZombiesRoundsManager : public UTickableWorldSubsystem
{
	GENERATED_BODY()

#pragma region DA_RoundsManager
	UPROPERTY()
	TArray<TSubclassOf<AZombiesEnemyCharacter>> EnemiesToSpawnClasses;
	
	UPROPERTY()
	TArray<TSoftObjectPtr<AZombiesEnemySpawnerArea>> EnemySpawnerAreas;

	/** This amount is per Player */
	UPROPERTY()
	int32 ZombiesNumberIncreasePerRound = 20;

	/** Every round, the zombies will multiply their last round health by this value. */
	UPROPERTY()
	float ZombiesHealthGainPerRoundMultiplier = 1.2f;

	/** How many zombies can be alive spawned at the same time in the map. */
	UPROPERTY()
	int32 MaximumZombiesAlive = 20;

	/** How many seconds it takes to start the next round. */
	UPROPERTY()
	float SecondsBetweenRounds = 10.f;

	/** SFX Played at the start of the Round. */
	UPROPERTY(BlueprintReadOnly, Category = "RoundSounds", meta = (AllowPrivateAccess = "true"))
	USoundBase* RoundStartSound = nullptr;

	/** SFX Played at the end of the Round. */
	UPROPERTY(BlueprintReadOnly, Category = "RoundSounds", meta = (AllowPrivateAccess = "true"))
	USoundBase* RoundEndSound = nullptr;

	/** SFX Played when all players are downed and the game ends. */
	UPROPERTY(BlueprintReadOnly, Category = "RoundSounds", meta = (AllowPrivateAccess = "true"))
	USoundBase* GameEndSound = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "RoundSounds", meta = (AllowPrivateAccess = "true"))
	float RoundsSFXVolumeMultiplier = 1.f;

	UPROPERTY()
	TArray<USoundBase*> ZombiesScreams;

	UPROPERTY()
	float TimeBetweenZombiesScreams = 5.f;

	UPROPERTY()
	float DeviationTimeBetweenZombieScreams = 1.f;

	/** For testing and debugging purposes. It enables/disables the Rounds Manager */
	UPROPERTY()
	bool ActivateRoundsManager = true;
#pragma endregion DA_RoundsManager

	UPROPERTY()
	float CurrentTimeBetweenZombiesScreams = 0.f;
	
	UPROPERTY()
	FTimerHandle StartRoundTimerHandle;

	UPROPERTY()
	FTimerHandle RandomZombieScreamTimerHandle;

	UPROPERTY()
	bool IsDataInitialized = false;

	UPROPERTY()
	float LastRoundZombiesHealth = 100.f;

	/** (RatioZombiesPerPlayer = TotalZombiesThisRound / Number of players) It is used to choose in which area spawn the
	 * next zombie taking into account how many players and zombies are there, trying to maintain this ratio. */
	UPROPERTY()
	float RatioZombiesPerPlayer = 0;
	
	UPROPERTY(BlueprintReadOnly, Category=RoundSetup, meta = (AllowPrivateAccess = "true"))
	int32 CurrentRound = 0;
	
	/** The amount of zombies that the players need to kill to finish the current round. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	int32 TotalZombiesThisRound = 0;

	/** The current amount of zombies spawned this round. It resets at the end of each round. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	int32 ZombiesSpawnedThisRound = 0;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<AZombiesEnemyCharacter*> ZombiesCurrentlyAlive;

public:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;

	virtual void Tick(float DeltaTime) override { Super::Tick(DeltaTime); }

	virtual TStatId GetStatId() const override { return TStatId(); }

	UFUNCTION()
	void InitializeFromDataAsset(const UDA_RoundsManager* data);
	
	UFUNCTION(BlueprintCallable)
	void StartRound();
	UFUNCTION(BlueprintCallable)
	void FinishRound();
	UFUNCTION(BlueprintCallable)
	bool CheckIfRoundFinished();

	UFUNCTION()
	AZombiesEnemyCharacter* SpawnZombie();
	UFUNCTION()
	void OnCharacterDied(AZombiesCharacter* deadCharacter);

	UFUNCTION()
	void EndAllAliveZombiesBehavior();

	UFUNCTION()
	USoundBase* GetGameEndSound() const { return GameEndSound; }
	UFUNCTION()
	float GetRoundsSFXMultiplier() const { return RoundsSFXVolumeMultiplier; }
	
	UFUNCTION()
	bool GetIsDataInitialized() const { return IsDataInitialized; }
	UFUNCTION()
	void SetIsDataInitialized(bool isInitialized) { IsDataInitialized = isInitialized; }

	UFUNCTION(BlueprintCallable)
	int32 GetCurrentRound() const { return CurrentRound; }

private:
	UFUNCTION()
	void ObtainAreasWithPlayersInside(TArray<TSoftObjectPtr<AZombiesEnemySpawnerArea>>& areasWithPlayersInside);

	/** Chooses the most appropriate area to Spawn a zombie from "areas". More Zombies will spawn in Areas with more players. */
	UFUNCTION()
	TSoftObjectPtr<AZombiesEnemySpawnerArea> ChooseAreaToSpawnZombie(TArray<TSoftObjectPtr<AZombiesEnemySpawnerArea>>& areas) const;

	UFUNCTION()
	void UpdateZombieRoundHealth(AZombiesCharacter* spawnedEnemy) const;
	
	UFUNCTION()
	void UpdatePlayersRoundsHUD() const;

	UFUNCTION()
	void PlayRandomZombieScreamLoop();
};
