#include "ZombiesInteractablesManager.h"

#include "Zombies/Actors/Interactables/Z_InteractPower.h"

void UZombiesInteractablesManager::SetAndBindPowerInteractable(AZ_InteractPower* power)
{
	PowerInteractable = power;
	PowerInteractable->OnPowerTurnedOnDelegate.AddDynamic(this, &UZombiesInteractablesManager::OnPowerActivationHandler);
}

void UZombiesInteractablesManager::OnPowerActivationHandler()
{
	OnPowerActivationHandler_Multicast();
}

void UZombiesInteractablesManager::OnPowerActivationHandler_Multicast_Implementation()
{
	PowerInteractable->OnPowerTurnedOnDelegate.RemoveDynamic(this, &UZombiesInteractablesManager::OnPowerActivationHandler);

	for (AZombiesInteractableActor* interactableActor : InteractableActors)
		interactableActor->OnPowerActivation();
}
