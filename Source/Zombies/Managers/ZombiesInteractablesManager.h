#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "ZombiesInteractablesManager.generated.h"

class AZombiesInteractableActor;
class AZ_InteractPower;

/**
 * Manages the Interactable Actors from the map, having a list of all of them. It also stores a pointer to
 * the Power Interactable, since it is the most important one and we need it for bindings.
 */

UCLASS(BlueprintType)
class ZOMBIES_API UZombiesInteractablesManager : public UWorldSubsystem
{
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	AZ_InteractPower* PowerInteractable = nullptr;
	
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<AZombiesInteractableActor*> InteractableActors;

public:
	UFUNCTION()
	AZ_InteractPower* GetPowerInteractable() const { return PowerInteractable; }
	UFUNCTION(BlueprintCallable)
	void SetAndBindPowerInteractable(AZ_InteractPower* power);
	
	UFUNCTION()
	void StoreAndBindInteractableActor(AZombiesInteractableActor* interactable) { InteractableActors.AddUnique(interactable); }
	
	/** This function is called when any Player activates the Power. */
	UFUNCTION()
	void OnPowerActivationHandler();

private:
	UFUNCTION(NetMulticast, Reliable)
	void OnPowerActivationHandler_Multicast();
};
