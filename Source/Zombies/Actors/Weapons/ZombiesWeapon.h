#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ZombiesWeapon.generated.h"

class USceneComponent;
class USkeletalMeshComponent;
class AZombiesCharacter;
class AZombiesPlayerCharacter;
class AZombiesPlayerController;
class UZombiesWeaponsManagerCmp;
class UNiagaraSystem;
class USoundWave;

UCLASS(Blueprintable, BlueprintType)
class ZOMBIES_API AZombiesWeapon : public AActor
{
	GENERATED_BODY()

	/** If the WeaponMeshComponent forward Vector is Y, mark this as true to use the appropriate socket when attaching
	 * this weapon to a Player. */
	UPROPERTY(EditAnywhere, Category = "WeaponCustomization", meta = (AllowPrivateAccess = "true"))
	bool isYForward = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponCustomization",meta = (AllowPrivateAccess = "true"))
	FName WeaponName = "Weapon";

	/** Damage per single shot */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponCustomization", meta = (AllowPrivateAccess = true))
	float DamageDealt = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponCustomization", meta = (AllowPrivateAccess = true))
	float HeadShootDamageMultiplier = 2.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponCustomization", meta = (AllowPrivateAccess = true))
	float Range = 50000.f;

	UPROPERTY()
	FVector ShootingDirection = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponCustomization", meta = (AllowPrivateAccess = "true"))
	FName ShootVectorStartSocketName = "SpawnBullet_Socket";

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponCustomization",meta = (AllowPrivateAccess = "true"))
	float FiringRate = 0.3f;

	UPROPERTY()
	float FiringRateAccumulated = 0.f;

	UPROPERTY()
	bool ReadyToShoot = true;

	/** How many seconds it takes to reload the weapon. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponCustomization", meta = (AllowPrivateAccess = "true"))
	float ReloadTime = 2.5f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponCustomization", meta = (AllowPrivateAccess = "true"))
	int32 MaxReserveAmmo = 200;

	UPROPERTY(BlueprintReadWrite, Category = "WeaponCustomization", meta = (AllowPrivateAccess = "true"))
	int32 CurrentReserveAmmo = 200;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponCustomization", meta = (AllowPrivateAccess = "true"))
	int32 MaxMagazineAmmo = 35;

	UPROPERTY(BlueprintReadWrite, Category = "WeaponCustomization", meta = (AllowPrivateAccess = "true"))
	int32 CurrentMagazineAmmo = 35;

	UPROPERTY(EditAnywhere, Category = "WeaponCustomization", meta = (AllowPrivateAccess = "true"))
	UNiagaraSystem* FireEffectMuzzle = nullptr;

	UPROPERTY(EditAnywhere, Category = "WeaponCustomization|Sound", meta = (AllowPrivateAccess = "true"))
	TArray<USoundWave*> FireSoundWaves;

	UPROPERTY(EditAnywhere, Category = "WeaponCustomization|Sound", meta = (AllowPrivateAccess = "true"))
	float FireSoundVolumeMultiplier = 1.f;

	UPROPERTY()
	AZombiesPlayerCharacter* OwnerPlayer = nullptr;
	UPROPERTY()
	AZombiesPlayerController* OwnerPlayerController = nullptr;
	
	UPROPERTY()
	UZombiesWeaponsManagerCmp* WeaponsManagerCmp = nullptr;

	UPROPERTY()
	USkeletalMeshComponent* WeaponMeshComponent = nullptr;
	
public:	
	AZombiesWeapon();

	virtual void Tick(float DeltaTime) override;

	/** First step of shooting. Checks if the weapon is ready to shoot and uses ammo. */
	UFUNCTION()
	bool Shoot_CheckAndUseAmmo();

	/** Second step of shooting. Called from Server. If "Shoot_CheckAndUseAmmo" has returned "true", then we spawn the
	 * Line trace for shooting and the Multicast of shooting VFX. */
	UFUNCTION()
	void Shoot_FromServer(FVector shootTrajectory) const;

	UFUNCTION()
	void SpawnShootFX();
	
	UFUNCTION()
	void DecrementMagazineAmmo();

	UFUNCTION()
	void ReplenishAmmo();
	
	UFUNCTION()
	bool HasFullReserveAmmo() const;

	UFUNCTION()
	bool CheckCanReloadWeapon() const;

	UFUNCTION()
	void ReloadWeapon();

	UFUNCTION()
	bool HasAmmoLeftInMagazine() const;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void SetWeaponHidden(bool isHidden);

	UFUNCTION() 
	FVector GetNextTraceSpawnLocation() const;

	UFUNCTION(BlueprintCallable)
	FName GetWeaponName() const { return WeaponName; }

	UFUNCTION(BlueprintCallable)
	int32 GetMaxReserveAmmo() const { return MaxReserveAmmo; }
	UFUNCTION(BlueprintCallable)
	int32 GetCurrentReserveAmmo() const { return CurrentReserveAmmo; }
	UFUNCTION(BlueprintCallable)
	int32 GetMaxMagazineAmmo() const { return MaxMagazineAmmo; }
	UFUNCTION(BlueprintCallable)
	int32 GetCurrentMagazineAmmo() const { return CurrentMagazineAmmo; }

	UFUNCTION(BlueprintCallable)
	float GetReloadTime() const { return ReloadTime; }
	
	UFUNCTION(BlueprintCallable)
	AZombiesPlayerCharacter* GetOwnerPlayer(){ return OwnerPlayer; }
	UFUNCTION()
	void SetOwnerPlayer(AZombiesPlayerCharacter* newOwner){ OwnerPlayer = newOwner; }

	UFUNCTION(BlueprintCallable)
	AZombiesPlayerController* GetOwnerPlayerController(){ return OwnerPlayerController; }
	UFUNCTION()
	void SetOwnerPlayerController(AZombiesPlayerController* newController){ OwnerPlayerController = newController; }

	UFUNCTION(BlueprintCallable)
	UZombiesWeaponsManagerCmp* GetWeaponsManagerCmp(){ return WeaponsManagerCmp; }
	UFUNCTION()
	void SetWeaponsManagerCmp(UZombiesWeaponsManagerCmp* newWeaponsManagerCmp){ WeaponsManagerCmp = newWeaponsManagerCmp; }

	UFUNCTION()
	bool GetIsYForward() const { return isYForward; }
	
protected:
	virtual void BeginPlay() override;
};
