#include "ZombiesWeapon.h"

#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Characters/ZombiesEnemyCharacter.h"
#include "Zombies/Components/Players/ZombiesWeaponsManagerCmp.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Zombies/GameStates/ZombiesGameState.h"

AZombiesWeapon::AZombiesWeapon()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AZombiesWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Firing rate management
	if(!ReadyToShoot)
	{
		FiringRateAccumulated += DeltaTime;
		if(FiringRateAccumulated >= FiringRate)
		{
			FiringRateAccumulated = 0.f;
			ReadyToShoot = true;
		}
	}
}

void AZombiesWeapon::BeginPlay()
{
	Super::BeginPlay();

	FiringRateAccumulated = 0.f;
	ReadyToShoot = true;
	CurrentReserveAmmo = MaxReserveAmmo;
	CurrentMagazineAmmo = MaxMagazineAmmo;

	// Store the Skeletal Mesh Component of the weapon added in BP.
	TArray<USkeletalMeshComponent*> skeletalMeshes;
	GetComponents(skeletalMeshes);
	WeaponMeshComponent = skeletalMeshes[0];
}

FVector AZombiesWeapon::GetNextTraceSpawnLocation() const
{
	return WeaponMeshComponent->GetSocketLocation(ShootVectorStartSocketName);
}

void AZombiesWeapon::DecrementMagazineAmmo()
{
	--CurrentMagazineAmmo;
}

void AZombiesWeapon::ReplenishAmmo()
{
	CurrentReserveAmmo = MaxReserveAmmo;
}

bool AZombiesWeapon::HasFullReserveAmmo() const
{
	if(CurrentReserveAmmo == MaxReserveAmmo)
		return true;

	return false;
}

bool AZombiesWeapon::CheckCanReloadWeapon() const
{
	if(CurrentMagazineAmmo == MaxMagazineAmmo || CurrentReserveAmmo == 0)
		return false;

	return true;
}

void AZombiesWeapon::ReloadWeapon()
{
	const int32 bulletsLeftInMagazine = MaxMagazineAmmo - CurrentMagazineAmmo;

	if(CurrentReserveAmmo >= bulletsLeftInMagazine)
	{
		CurrentReserveAmmo -= bulletsLeftInMagazine;
		CurrentMagazineAmmo = MaxMagazineAmmo;
	}
	else
	{
		CurrentMagazineAmmo += CurrentReserveAmmo;
		CurrentReserveAmmo = 0;
	}
}

bool AZombiesWeapon::HasAmmoLeftInMagazine() const
{
	if(CurrentMagazineAmmo > 0)
		return true;

	return false;
}

bool AZombiesWeapon::Shoot_CheckAndUseAmmo()
{
	if(HasAmmoLeftInMagazine() && ReadyToShoot && WeaponsManagerCmp)
	{
		ReadyToShoot = false;
		DecrementMagazineAmmo();
		return true;
	}
	return false;
}

void AZombiesWeapon::Shoot_FromServer(FVector shootTrajectory) const
{
	if(OwnerPlayer && WeaponsManagerCmp)
	{
		const FTransform muzzleSocketTransform = WeaponMeshComponent->GetSocketTransform(ShootVectorStartSocketName);
		FVector const startLocation = muzzleSocketTransform.GetLocation();
		FVector const endLocation = startLocation + (shootTrajectory * Range);
		FHitResult hit;
		FCollisionQueryParams params;
		params.AddIgnoredActor(this);
		AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
		if(gameState)
		{
			for(APawn* playerPawn : gameState->GetPlayerPawns())
				params.AddIgnoredActor(playerPawn);
		}
		FCollisionObjectQueryParams objectQueryParams;
		objectQueryParams.AddObjectTypesToQuery(ECC_Pawn);
		
		bool hasHit = GetWorld()->LineTraceSingleByObjectType(hit, startLocation, endLocation, objectQueryParams, params);
		//DrawDebugLine(GetWorld(), startLocation, endLocation, FColor::Blue, false, 0.1f, 0, 3.f);
		if(hasHit && hit.GetActor() && hit.GetComponent())
		{
			AZombiesEnemyCharacter* hitEnemy = Cast<AZombiesEnemyCharacter>(hit.GetActor());
			if(!hitEnemy)
				return;
			
			float damage = DamageDealt;
			const bool isHeadShoot = hit.GetComponent()->ComponentHasTag("head");
			if(isHeadShoot)
				damage *= HeadShootDamageMultiplier;
			
			WeaponsManagerCmp->BulletHitHandle(hitEnemy, damage, isHeadShoot);
		}
	}
}

void AZombiesWeapon::SpawnShootFX()
{
	if(FireEffectMuzzle)
	{
		UNiagaraFunctionLibrary::SpawnSystemAttached(FireEffectMuzzle, WeaponMeshComponent, ShootVectorStartSocketName,
			FVector::ZeroVector, FRotator(0.f), EAttachLocation::Type::SnapToTarget, true);
	}
	if(FireSoundWaves.Num() > 0)
	{
		const int32 randomIndex = FMath::RandRange(0, FireSoundWaves.Num()-1);
		USoundWave* usedSound = FireSoundWaves[randomIndex];
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), usedSound, GetActorLocation(), GetActorRotation(), FireSoundVolumeMultiplier);
	}
}
