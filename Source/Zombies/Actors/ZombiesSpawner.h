#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ZombiesSpawner.generated.h"

class AZombiesCharacter;

UCLASS(Blueprintable, BlueprintType)
class ZOMBIES_API AZombiesSpawner : public AActor
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float DistanceToCheckPlayer = 150.f;
	
public:	
	AZombiesSpawner();

	UFUNCTION()
	AZombiesCharacter* SpawnCharacter_FromServer(UClass* classToSpawn) const;

	UFUNCTION()
	bool IsPlayerNearby_FromServer();

};
