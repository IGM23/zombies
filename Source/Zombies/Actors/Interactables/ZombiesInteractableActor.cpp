#include "ZombiesInteractableActor.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesInteractionComponent.h"
#include "Zombies/Managers/ZombiesInteractablesManager.h"

AZombiesInteractableActor::AZombiesInteractableActor()
{
	PrimaryActorTick.bCanEverTick = false;

	SceneRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRootComponent"));
	RootComponent = SceneRootComponent;
	
	InteractionBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("InteractionBoxComponent"));
	InteractionBoxComponent->SetupAttachment(SceneRootComponent);
}
void AZombiesInteractableActor::BeginPlay()
{
	Super::BeginPlay();

	InteractionBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AZombiesInteractableActor::OnPlayerEntersInteractionRadius);
	InteractionBoxComponent->OnComponentEndOverlap.AddDynamic(this, &AZombiesInteractableActor::OnPlayerLeavesInteractionRadius);

	UZombiesInteractablesManager* interactablesManager = GetWorld()->GetSubsystem<UZombiesInteractablesManager>();
	if(interactablesManager)
		interactablesManager->StoreAndBindInteractableActor(this);
}

void AZombiesInteractableActor::OnPlayerEntersInteractionRadius(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
		UPrimitiveComponent* otherComponent, int32 otherBodyIndex, bool bFromSweep, const FHitResult& hit)
{
	AZombiesPlayerCharacter* player = Cast<AZombiesPlayerCharacter>(otherActor);
	if(player && player->GetInteractionComponent())
	{
		player->GetInteractionComponent()->SetInteractableActorAvailable(this);
	}
}

void AZombiesInteractableActor::OnPlayerLeavesInteractionRadius(UPrimitiveComponent* overlappedComp, AActor* otherActor,
	UPrimitiveComponent* otherComp, int32 otherBodyIndex)
{
	AZombiesPlayerCharacter* player = Cast<AZombiesPlayerCharacter>(otherActor);
	if(player && player->GetInteractionComponent())
	{
		player->GetInteractionComponent()->SetInteractableActorAvailable(nullptr);
	}
}

void AZombiesInteractableActor::PlayInteractionSound() const
{
	if(InteractionSound)
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), InteractionSound, GetActorLocation(), GetActorRotation(), InteractionSoundMultiplier);
}
