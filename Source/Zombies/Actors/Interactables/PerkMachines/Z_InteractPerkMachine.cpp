#include "Z_InteractPerkMachine.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"
#include "Zombies/Components/ZombiesInteractionComponent.h"
#include "Zombies/Components/Players/ZombiesPerksComponent.h"
#include "Zombies/Objects/ZombiesPerkObject.h"

void AZ_InteractPerkMachine::BeginPlay()
{
	Super::BeginPlay();
	
	Perk = NewObject<UZombiesPerkObject>(this, PerkClass);
	InteractionInfoText = FText::FromString("The Power must be activated first!");
}

bool AZ_InteractPerkMachine::CheckInteractionRequisites(AZombiesPlayerCharacter* playerInstigator)
{
	if( !IsActivated ||
		!Perk->IsValidLowLevel() ||
		playerInstigator->GetCharStatsComponent()->GetPlayerPoints() < PerkPrice ||
		playerInstigator->GetPerksComponent()->HasMaxPerksEquipped() ||
		playerInstigator->GetPerksComponent()->HasPerkEquipped(Perk))
	{
		return false;
	}
	
	return true;
}

void AZ_InteractPerkMachine::Interact(AZombiesPlayerCharacter* playerInstigator)
{
	if(!CheckInteractionRequisites(playerInstigator))
		return;

	PlayInteractionSound();
	playerInstigator->GetInteractionComponent()->AddPointsAndUpdateUI(-PerkPrice);
	playerInstigator->GetPerksComponent()->AddAndActivatePerk(Perk);
}

void AZ_InteractPerkMachine::OnPowerActivation()
{
	Super::OnPowerActivation();

	const FString infoString = FString::Printf(TEXT("Press F/Square(PS)/A(XBOX) to buy %s"
												 "\n Cost: %i points"), *Perk->GetPerkName(), static_cast<int>(PerkPrice));
	InteractionInfoText = FText::AsCultureInvariant(infoString);
}