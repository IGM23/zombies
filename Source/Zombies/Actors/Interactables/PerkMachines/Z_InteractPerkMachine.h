#pragma once

#include "CoreMinimal.h"
#include "Zombies/Actors/Interactables/ZombiesInteractableActor.h"
#include "Z_InteractPerkMachine.generated.h"

class UZombiesPerkObject;

/** A Perk Machine placed in a wall of the map. If the player buys it, he will obtain this machine's exclusive perk.
 * If the player is downed or dies, he will lose all the perks and will have to buy them again.
 * Each player can have a limited amount of perks. */

UCLASS(Blueprintable, BlueprintType)
class ZOMBIES_API AZ_InteractPerkMachine : public AZombiesInteractableActor
{
	GENERATED_BODY()

	/** The Perk that players can buy from this machine. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup Perk" , meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UZombiesPerkObject> PerkClass;

	UPROPERTY()
	UZombiesPerkObject* Perk;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup Perk" , meta = (AllowPrivateAccess = "true"))
	float PerkPrice = 2000.f;

public:	
	AZ_InteractPerkMachine(){}

	/** The player obtains the perk. */
	virtual void Interact(AZombiesPlayerCharacter* instigator) override;

	UFUNCTION()
	float GetPerkPrice() const { return PerkPrice; }

	virtual void OnPowerActivation() override;

protected:
	virtual void BeginPlay() override;
	
	/** The player must not have the perk, must not have the maximum amount of perks reached and have points to buy it. */
	virtual bool CheckInteractionRequisites(AZombiesPlayerCharacter* playerInstigator) override;
};
