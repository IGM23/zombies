#pragma once

#include "CoreMinimal.h"
#include "Zombies/Actors/Interactables/ZombiesInteractableActor.h"
#include "Z_InteractPower.generated.h"

/**
 * This interactable starts always off. Players must turn the Power on in order to use a lot of interactables of the map.
 * Once the Power is on, it stays always on. When activated, it notifies to the Interactables which need the power,
 * and activate some of them instantly as well.
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPowerTurnedOn);

UCLASS(Blueprintable, BlueprintType)
class ZOMBIES_API AZ_InteractPower : public AZombiesInteractableActor
{
	GENERATED_BODY()
	
public:
	FOnPowerTurnedOn OnPowerTurnedOnDelegate;

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UStaticMeshComponent* PowerMeshComp = nullptr;

public:
	AZ_InteractPower();
	
	/** The player permanently turns on the Power. */
	virtual void Interact(AZombiesPlayerCharacter* playerInstigator) override;

	virtual void ReplicateInteraction() override;

	virtual void OnPowerActivation() override;

protected:
	virtual void BeginPlay() override;
	
	virtual bool CheckInteractionRequisites(AZombiesPlayerCharacter* playerInstigator) override;
	
	UFUNCTION(BlueprintImplementableEvent)
	void StartActivatePowerAnimation();
};
