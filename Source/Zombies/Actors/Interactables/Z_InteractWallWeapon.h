#pragma once

#include "CoreMinimal.h"
#include "Zombies/Actors/Interactables/ZombiesInteractableActor.h"
#include "Z_InteractWallWeapon.generated.h"

/** A Weapon placed in a wall of the map. If the player buys it, he will obtain it, replacing their current equipped
 * Weapon. The player will lose their current equipped Weapon if they reached the maximum amount of weapons equipped.
 * If the player buys it when they already have this Weapon, they will buy ammunition instead. */

class UBoxComponent;
class AZombiesWeapon;

UCLASS(Blueprintable, BlueprintType)
class ZOMBIES_API AZ_InteractWallWeapon : public AZombiesInteractableActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup WallWeapon" , meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AZombiesWeapon> WeaponToBuyClass = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup WallWeapon" , meta = (AllowPrivateAccess = "true"))
	float WeaponPrice = 1000.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup WallWeapon" , meta = (AllowPrivateAccess = "true"))
	float AmmoPrice = 500.f;

public:	
	AZ_InteractWallWeapon(){}

	/** The player obtains the weapon/ammo. */
	virtual void Interact(AZombiesPlayerCharacter* instigator) override;

	UFUNCTION()
	float GetWeaponPrice() const { return WeaponPrice; }
	UFUNCTION()
	float GetAmmoPrice() const { return AmmoPrice; }

protected:
	virtual void BeginPlay() override;
	
	/** The player must have money to buy the weapon/ammo. */
	virtual bool CheckInteractionRequisites(AZombiesPlayerCharacter* playerInstigator) override;
};
