#include "Z_InteractPower.h"

#include "Components/BoxComponent.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesInteractionComponent.h"
#include "Zombies/Managers/ZombiesInteractablesManager.h"

AZ_InteractPower::AZ_InteractPower()
{
	SetActorTickEnabled(false);
	
	PowerMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PowerMeshComp"));
	PowerMeshComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	PowerMeshComp->SetupAttachment(RootComponent);
}

void AZ_InteractPower::BeginPlay()
{
	Super::BeginPlay();

	IsActivated = false;
	InteractionInfoText = FText::FromString("Press F/Square(PS)/A(XBOX) to activate the Power");

	UZombiesInteractablesManager* interactablesManager = GetWorld()->GetSubsystem<UZombiesInteractablesManager>();
	if(interactablesManager)
		interactablesManager->SetAndBindPowerInteractable(this);
}

bool AZ_InteractPower::CheckInteractionRequisites(AZombiesPlayerCharacter* playerInstigator)
{
	return !IsActivated;
}

void AZ_InteractPower::Interact(AZombiesPlayerCharacter* playerInstigator)
{
	if(!CheckInteractionRequisites(playerInstigator))
		return;

	PlayInteractionSound();
	playerInstigator->GetInteractionComponent()->ReplicateInteractableActorInteraction(true);
}

void AZ_InteractPower::ReplicateInteraction()
{
	IsActivated = true;
	OnPowerTurnedOnDelegate.Broadcast();
}

void AZ_InteractPower::OnPowerActivation()
{
	InteractionBoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StartActivatePowerAnimation();
}