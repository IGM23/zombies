#include "Z_InteractDoor_Power.h"

#include "Components/BoxComponent.h"

void AZ_InteractDoor_Power::BeginPlay()
{
	Super::BeginPlay();
	InteractionInfoText = FText::FromString("Activate the Power to open this door!");
}

void AZ_InteractDoor_Power::OnPowerActivation()
{
	Super::OnPowerActivation();
	InteractionInfoText = FText::FromString("");
	InteractionBoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	DoorMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	OpenAndDeactivateDoor();
}