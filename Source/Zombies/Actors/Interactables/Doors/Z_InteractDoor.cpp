#include "Z_InteractDoor.h"

#include "Components/BoxComponent.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"
#include "Zombies/Components/ZombiesInteractionComponent.h"

AZ_InteractDoor::AZ_InteractDoor()
{
	InteractionBoxComponent->SetCanEverAffectNavigation(false);
	
	DoorMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorMeshComp"));
	DoorMeshComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	DoorMeshComp->SetCanEverAffectNavigation(false);
	DoorMeshComp->SetupAttachment(RootComponent);

	NavigationBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("NavigationBoxComponent"));
	NavigationBoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	NavigationBoxComponent->SetCanEverAffectNavigation(true);
	NavigationBoxComponent->SetupAttachment(RootComponent);
}

void AZ_InteractDoor::BeginPlay()
{
	Super::BeginPlay();

	IsActivated = true;
	const FString infoString = FString::Printf(TEXT("Press F/Square(PS)/A(XBOX) to open the door"
												 "\n Cost: %i points"), static_cast<int>(DoorPrice));
	InteractionInfoText = FText::AsCultureInvariant(infoString);
}

bool AZ_InteractDoor::CheckInteractionRequisites(AZombiesPlayerCharacter* playerInstigator)
{
	if(IsActivated && !HasBeenUsed && playerInstigator->GetCharStatsComponent()->GetPlayerPoints() >= DoorPrice)
		return true;

	return false;
}

void AZ_InteractDoor::Interact(AZombiesPlayerCharacter* playerInstigator)
{
	if(!CheckInteractionRequisites(playerInstigator))
		return;

	PlayInteractionSound();
	HasBeenUsed = true;
	playerInstigator->GetInteractionComponent()->AddPointsAndUpdateUI(-DoorPrice);
	playerInstigator->GetInteractionComponent()->ReplicateInteractableActorInteraction(true);
}

void AZ_InteractDoor::ReplicateInteraction()
{
	OpenAndDeactivateDoor();
}