#pragma once

#include "CoreMinimal.h"
#include "Zombies/Actors/Interactables/ZombiesInteractableActor.h"
#include "Z_InteractDoor.generated.h"

class UStaticMeshComponent;

/** This interactable doors start always closed. Players must pay their price once and they permanently open.
 * They are essential to advance to different areas of the map.
 */

UCLASS(Blueprintable, BlueprintType)
class ZOMBIES_API AZ_InteractDoor : public AZombiesInteractableActor
{
	GENERATED_BODY()

protected:
	/** This component affects the Navmesh. When a door opens, we want that part of the Navmesh to be rebuilt,
	 * so the enemies can pass between rooms.*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"))
	UBoxComponent* NavigationBoxComponent = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UStaticMeshComponent* DoorMeshComp = nullptr;

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup Door" , meta = (AllowPrivateAccess = "true"))
	float DoorPrice = 1000.f;

	UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	bool HasBeenUsed = false;

public:
	AZ_InteractDoor();
	
	/** The player permanently opens the door. */
	virtual void Interact(AZombiesPlayerCharacter* playerInstigator) override;

	virtual void ReplicateInteraction() override;

protected:
	virtual void BeginPlay() override;
	
	/** The player must have money to open the door. */
	virtual bool CheckInteractionRequisites(AZombiesPlayerCharacter* playerInstigator) override;
	
	UFUNCTION(BlueprintImplementableEvent)
	void OpenAndDeactivateDoor();
};
