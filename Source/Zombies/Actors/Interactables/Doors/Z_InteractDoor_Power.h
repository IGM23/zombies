#pragma once

#include "CoreMinimal.h"
#include "Zombies/Actors/Interactables/Doors/Z_InteractDoor.h"
#include "Z_InteractDoor_Power.generated.h"

/**
 * This door opens automatically when the Power is turned on, and they remain permanently open.
 */

UCLASS(Blueprintable, BlueprintType)
class ZOMBIES_API AZ_InteractDoor_Power : public AZ_InteractDoor
{
	GENERATED_BODY()

public:
	virtual void OnPowerActivation() override;

protected:
	virtual void BeginPlay() override;
};
