#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ZombiesInteractableActor.generated.h"

class AZombiesPlayerCharacter;
class UBoxComponent;

/** Interactable Actor are every actor in the map that the players can interact with.
 * Examples: Weapons, Doors, Perks, Traps...
 * If an Interactable is not Activated by default, then the Power must be turned on to use it.
 */

UCLASS(Blueprintable, BlueprintType, Abstract)
class ZOMBIES_API AZombiesInteractableActor : public AActor
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USceneComponent* SceneRootComponent = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* InteractionBoxComponent = nullptr;

	/** Text that will appear in the player's screen when the player is inside the InteractionBoxComponent */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FText InteractionInfoText = FText::FromString("Information text");
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	bool IsActivated = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USoundBase* InteractionSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float InteractionSoundMultiplier = 0.2f;
	
public:	
	AZombiesInteractableActor();
	
	UFUNCTION()
	virtual void Interact(AZombiesPlayerCharacter* playerInstigator) PURE_VIRTUAL(AZombiesInteractableActor::Interact, ;)

	/** Only called if the Interactable Actor has to replicate something to everyone.
	 * Example: if we open a door, the door must be opened for everyone. */
	UFUNCTION()
	virtual void ReplicateInteraction(){}

	/** If this Interactable Actor needs the Power to be interacted with,
	 * this function will be executed when the Power is just turned on. */
	UFUNCTION()
	virtual void OnPowerActivation(){ IsActivated = true; }
	
	UFUNCTION()
	FText GetInteractionInfoText() const { return InteractionInfoText; }

	UFUNCTION()
	bool GetIsActivated() const { return IsActivated; }
	
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual bool CheckInteractionRequisites(AZombiesPlayerCharacter* playerInstigator) PURE_VIRTUAL(AZombiesInteractableActor::CheckInteractionRequisites, return true;)

	UFUNCTION()
	virtual void OnPlayerEntersInteractionRadius(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
		UPrimitiveComponent* otherComponent, int32 otherBodyIndex, bool bFromSweep, const FHitResult& hit);
	
	UFUNCTION()
	virtual void OnPlayerLeavesInteractionRadius(UPrimitiveComponent* overlappedComp, AActor* otherActor,
	UPrimitiveComponent* otherComp, int32 otherBodyIndex);

	UFUNCTION()
	void PlayInteractionSound() const;
};
