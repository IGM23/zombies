#include "Z_InteractWallWeapon.h"
#include "Zombies/Actors/Weapons/ZombiesWeapon.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"
#include "Zombies/Components/ZombiesInteractionComponent.h"
#include "Zombies/Components/Players/ZombiesWeaponsManagerCmp.h"

void AZ_InteractWallWeapon::BeginPlay()
{
	Super::BeginPlay();

	IsActivated = true;
	const FString weaponString = WeaponToBuyClass->GetDefaultObject<AZombiesWeapon>()->GetWeaponName().ToString();
	const FString infoString = FString::Printf(TEXT("Press F/Square(PS)/A(XBOX) to buy %s or Ammo"
												 "\n Weapon cost: %i points. Ammo cost: %i points"),
		*weaponString, static_cast<int>(WeaponPrice), static_cast<int>(AmmoPrice));
	InteractionInfoText = FText::AsCultureInvariant(infoString);
}

bool AZ_InteractWallWeapon::CheckInteractionRequisites(AZombiesPlayerCharacter* playerInstigator)
{
	if(!IsActivated)
		return false;
		
	float priceToCheck = WeaponPrice;
	AZombiesWeapon* weaponEquipped = playerInstigator->GetWeaponsManagerComponent()->ReturnWeaponEquipped(WeaponToBuyClass);
	if(weaponEquipped)
	{
		priceToCheck = AmmoPrice;
		if(weaponEquipped->HasFullReserveAmmo())
			return false;
	}
	
	if(playerInstigator->GetCharStatsComponent()->GetPlayerPoints() >= priceToCheck)
		return true;

	return false;
}

void AZ_InteractWallWeapon::Interact(AZombiesPlayerCharacter* playerInstigator)
{
	if(!CheckInteractionRequisites(playerInstigator))
		return;

	PlayInteractionSound();
	AZombiesWeapon* weaponEquipped = playerInstigator->GetWeaponsManagerComponent()->ReturnWeaponEquipped(WeaponToBuyClass);
	if(weaponEquipped)
	{
		playerInstigator->GetInteractionComponent()->AddPointsAndUpdateUI(-AmmoPrice);
		playerInstigator->GetWeaponsManagerComponent()->ReplenishEquippedWeaponAmmo(weaponEquipped);
	}
	else
	{
		playerInstigator->GetInteractionComponent()->AddPointsAndUpdateUI(-WeaponPrice);
		playerInstigator->GetWeaponsManagerComponent()->PickupWeapon(WeaponToBuyClass);
	}
}
