#include "Z_InteractTrap.h"

#include "Components/BoxComponent.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"
#include "Zombies/Components/ZombiesInteractionComponent.h"

AZ_InteractTrap::AZ_InteractTrap()
{
	PrimaryActorTick.bCanEverTick = true;

	DamageBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("DamageBoxComponent"));
	DamageBoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	DamageBoxComponent->SetCanEverAffectNavigation(false);
	DamageBoxComponent->SetupAttachment(SceneRootComponent);

	DoorActivationButtonMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorActivationButtonMesh"));
	DoorActivationButtonMesh->SetCanEverAffectNavigation(false);
	DoorActivationButtonMesh->SetupAttachment(SceneRootComponent);

	InteractionComponent = CreateDefaultSubobject<UZombiesInteractionComponent>(TEXT("InteractionComponent"));
}

void AZ_InteractTrap::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if(TrapState == ETrapState::ON)
	{
		DurationAcc += DeltaSeconds;
		if(DurationAcc >= TrapDuration)
		{
			DurationAcc = 0.f;
			StartCooldown();
		}
	}
	else if(TrapState == ETrapState::COOLDOWN)
	{
		CooldownAcc += DeltaSeconds;
		if(CooldownAcc >= TrapCooldown)
		{
			CooldownAcc = 0.f;
			FinishCooldown();
		}
	}
}

void AZ_InteractTrap::BeginPlay()
{
	Super::BeginPlay();

	TrapState = ETrapState::OFF;
	DurationAcc = 0.f;
	CooldownAcc = 0.f;
	InteractionInfoText = FText::FromString("The Power must be activated first!");

	if(GetNetMode() != NM_Client)
	{
		DamageBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AZ_InteractTrap::DamageCharacter);
		DamageBoxComponent->OnComponentEndOverlap.AddDynamic(this, &AZ_InteractTrap::OnAliveCharacterLeaveArea);
	}
}

bool AZ_InteractTrap::CheckInteractionRequisites(AZombiesPlayerCharacter* playerInstigator)
{
	if( !IsActivated ||
		playerInstigator->GetCharStatsComponent()->GetPlayerPoints() < TrapPrice ||
		TrapState != ETrapState::OFF)
	{
		return false;
	}
	
	return true;
}

void AZ_InteractTrap::Interact(AZombiesPlayerCharacter* instigator)
{
	if(!CheckInteractionRequisites(instigator))
		return;

	PlayInteractionSound();
	instigator->GetInteractionComponent()->AddPointsAndUpdateUI(-TrapPrice);

	// We put the ON state here as well so the clients don't have to wait for it and spend all their points.
	TrapState = ETrapState::ON;
	instigator->GetInteractionComponent()->ReplicateInteractableActorInteraction(true);
}

void AZ_InteractTrap::ReplicateInteraction()
{
	ActivateTrap();
}

void AZ_InteractTrap::OnPowerActivation()
{
	Super::OnPowerActivation();

	const FString infoString = FString::Printf(TEXT("Press F/Square(PS)/A(XBOX) to activate TRAP"
												 "\n Cost: %i points"), static_cast<int>(TrapPrice));
	InteractionInfoText = FText::AsCultureInvariant(infoString);
}

void AZ_InteractTrap::ActivateTrap()
{
	TrapState = ETrapState::ON;
	InteractionInfoText = FText::FromString("The Trap is Active!");
	DamageBoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	ActivateTrapBPVFX();
	OnTrapActivatedBP();
}

void AZ_InteractTrap::StartCooldown()
{
	TrapState = ETrapState::COOLDOWN;
	InteractionInfoText = FText::FromString("The Trap is in Cooldown!");
	DamageBoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StopTrapBPVFX();
	OnCooldownStartedBP();
}

void AZ_InteractTrap::FinishCooldown()
{
	TrapState = ETrapState::OFF;
	InteractionInfoText = FText::FromString("The Trap is in Cooldown!");
	const FString infoString = FString::Printf(TEXT("Press F/Square(PS)/A(XBOX) to activate TRAP"
												 "\n Cost: %i points"), static_cast<int>(TrapPrice));
	InteractionInfoText = FText::AsCultureInvariant(infoString);
	OnCooldownFinishedBP();
}

void AZ_InteractTrap::DamageCharacter(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
	UPrimitiveComponent* otherComponent, int32 otherBodyIndex, bool bFromSweep, const FHitResult& hit)
{
	AZombiesCharacter* character = Cast<AZombiesCharacter>(otherActor);
	if(character && character->GetCharStatsComponent() && character->GetCharStatsComponent()->IsCharacterAlive())
	{
		InteractionComponent->DamageTarget_FromServer(character, character->GetCharStatsComponent()->GetDefaultMaxHealth());
		if(character->GetCharStatsComponent()->GetCurrentHealth() > 0.f)
		{
			PlayersInsideAlive.Add(character);
			FTimerDelegate damageCharacterInsideDelegate;
			damageCharacterInsideDelegate.BindUFunction(this, "DamageCharacterAliveInside", character);
			GetWorld()->GetTimerManager().SetTimer(DamageCharacterInsideTimerHandle, damageCharacterInsideDelegate,
			DamageIntervalSeconds, false);
		}
	}
}

void AZ_InteractTrap::OnAliveCharacterLeaveArea(UPrimitiveComponent* overlappedComp, AActor* otherActor,
	UPrimitiveComponent* otherComp, int32 otherBodyIndex)
{
	AZombiesCharacter* character = Cast<AZombiesCharacter>(otherActor);
	if(character && character->GetCharStatsComponent() && character->GetCharStatsComponent()->IsCharacterAlive())
		PlayersInsideAlive.Remove(character);
}

void AZ_InteractTrap::DamageCharacterAliveInside(AZombiesCharacter* characterInside)
{
	if(PlayersInsideAlive.Contains(characterInside))
		InteractionComponent->DamageTarget_FromServer(characterInside, characterInside->GetCharStatsComponent()->GetCurrentHealth());
}
