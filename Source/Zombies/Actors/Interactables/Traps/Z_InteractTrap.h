#pragma once

#include "CoreMinimal.h"
#include "Zombies/Actors/Interactables/ZombiesInteractableActor.h"
#include "Z_InteractTrap.generated.h"

class UStaticMeshComponent;
class UZombiesInteractionComponent;
class AZombiesCharacter;

UENUM(BlueprintType)
enum class ETrapState : uint8
{
	OFF UMETA(DisplayName = "Off"),
	ON UMETA(DisplayName = "On"),
	COOLDOWN UMETA(DisplayName = "Cooldown")
};

/**
 * A Trap placed in the map that can be activated unlimited amounts of times. In order to activate it every single time,
 * a player must pay. The trap has a time duration and a cooldown. The trap insta-kills zombies and players, unless they
 * have the Juggernog perk.
 */

UCLASS(Blueprintable, BlueprintType)
class ZOMBIES_API AZ_InteractTrap : public AZombiesInteractableActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UStaticMeshComponent* DoorActivationButtonMesh= nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = CustomComponents, meta = (AllowPrivateAccess = "true"))
	UZombiesInteractionComponent* InteractionComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* DamageBoxComponent = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup Trap" , meta = (AllowPrivateAccess = "true"))
	float TrapPrice = 2000.f;

	/** Duration in seconds. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup Trap" , meta = (AllowPrivateAccess = "true"))
	float TrapDuration = 30.f;

	UPROPERTY()
	float DurationAcc = 0.f;

	/** Cooldown in seconds. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup Trap" , meta = (AllowPrivateAccess = "true"))
	float TrapCooldown = 90.f;

	UPROPERTY()
	float CooldownAcc = 0.f;

	UPROPERTY()
	ETrapState TrapState = ETrapState::OFF;

	/** Player with Juggernog can survive the trap. If they stay inside for some seconds, we kill them. */
	UPROPERTY()
	TArray<AZombiesCharacter*> PlayersInsideAlive;

	/** For players with Juggernog who stay inside the trap, every DamageIntervalSeconds they receive damage. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup Trap" , meta = (AllowPrivateAccess = "true"))
	float DamageIntervalSeconds = 1.f;

	UPROPERTY()
	FTimerHandle DamageCharacterInsideTimerHandle;

public:
	AZ_InteractTrap();

	virtual void Tick(float DeltaSeconds) override;

	/** The player activates the trap. */
	virtual void Interact(AZombiesPlayerCharacter* instigator) override;

	virtual void ReplicateInteraction() override;

	virtual void OnPowerActivation() override;
	
protected:
	virtual void BeginPlay() override;
	
	/** The player must have points to buy it and the trap must not be active or in cooldown. */
	virtual bool CheckInteractionRequisites(AZombiesPlayerCharacter* playerInstigator) override;

	UFUNCTION(BlueprintImplementableEvent)
	void ActivateTrapBPVFX();
	UFUNCTION(BlueprintImplementableEvent)
	void StopTrapBPVFX();

	UFUNCTION(BlueprintImplementableEvent)
	void OnTrapActivatedBP();
	UFUNCTION(BlueprintImplementableEvent)
	void OnCooldownStartedBP();
	UFUNCTION(BlueprintImplementableEvent)
	void OnCooldownFinishedBP();
	
	UFUNCTION()
	void ActivateTrap();
	UFUNCTION()
	void StartCooldown();
	UFUNCTION()
	void FinishCooldown();
	
	UFUNCTION()
	void DamageCharacter(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
		UPrimitiveComponent* otherComponent, int32 otherBodyIndex, bool bFromSweep, const FHitResult& hit);
	UFUNCTION()
	void OnAliveCharacterLeaveArea(UPrimitiveComponent* overlappedComp, AActor* otherActor,
		UPrimitiveComponent* otherComp, int32 otherBodyIndex);

	UFUNCTION()
	void DamageCharacterAliveInside(AZombiesCharacter* characterInside);
};
