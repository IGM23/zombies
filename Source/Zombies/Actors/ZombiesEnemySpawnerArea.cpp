#include "ZombiesEnemySpawnerArea.h"

#include "ZombiesSpawner.h"
#include "Components/BoxComponent.h"
#include "Zombies/Characters/ZombiesEnemyCharacter.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"

AZombiesEnemySpawnerArea::AZombiesEnemySpawnerArea()
{
	PrimaryActorTick.bCanEverTick = false;
	PhysicalArea = CreateDefaultSubobject<UBoxComponent>(TEXT("PhysicalArea"));

	//ECC_GameTraceChannel1 = TriggerObjectChannel
	//ECC_GameTraceChannel2 = TriggerTraceChannel
	PhysicalArea->SetCollisionObjectType(ECollisionChannel::ECC_EngineTraceChannel1);
}

AZombiesEnemyCharacter* AZombiesEnemySpawnerArea::SpawnEnemy(UClass* classToSpawn)
{
	AZombiesEnemyCharacter* spawnedEnemy = nullptr;
	
	if(classToSpawn)
	{
		const int32 randomSpawnerIndex = FMath::RandRange(0, Spawners.Num() - 1);
		spawnedEnemy = Cast<AZombiesEnemyCharacter>(Spawners[randomSpawnerIndex]->SpawnCharacter_FromServer(classToSpawn));
	}

	return spawnedEnemy;
}

void AZombiesEnemySpawnerArea::BeginPlay()
{
	Super::BeginPlay();

	PlayersInThisArea.Empty();
	if(GetLocalRole() == ROLE_Authority)
	{
		PhysicalArea->OnComponentBeginOverlap.AddDynamic(this, &AZombiesEnemySpawnerArea::OnPawnEnteredPhysicalArea);
		PhysicalArea->OnComponentEndOverlap.AddDynamic(this, &AZombiesEnemySpawnerArea::OnPawnLeavedPhysicalArea);
	}
}

void AZombiesEnemySpawnerArea::OnPawnEnteredPhysicalArea(UPrimitiveComponent* overlappedComp, AActor* otherActor,
		UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AZombiesPlayerCharacter* playerCharacter = Cast<AZombiesPlayerCharacter>(otherActor);
	if(playerCharacter)
	{
		AddPlayerToThisArea(playerCharacter);
		playerCharacter->SetAreaCurrentlyIn(this);
		return;
	}
	
	AZombiesEnemyCharacter* enemyCharacter = Cast<AZombiesEnemyCharacter>(otherActor);
	if(enemyCharacter)
	{
		AddZombieToThisArea(enemyCharacter);
		enemyCharacter->SetAreaCurrentlyIn(this);
	}
}

void AZombiesEnemySpawnerArea::OnPawnLeavedPhysicalArea(UPrimitiveComponent* overlappedComp, AActor* otherActor,
	UPrimitiveComponent* otherComp, int32 otherBodyIndex)
{
	AZombiesPlayerCharacter* playerCharacter = Cast<AZombiesPlayerCharacter>(otherActor);
	if(playerCharacter)
	{
		RemovePlayerFromThisArea(playerCharacter);
		return;
	}

	AZombiesEnemyCharacter* enemyCharacter = Cast<AZombiesEnemyCharacter>(otherActor);
	if(enemyCharacter)
		RemoveZombieFromThisArea(enemyCharacter);
}
