#include "ZombiesSpawner.h"

#include "Kismet/GameplayStatics.h"
#include "Zombies/Characters/ZombiesCharacter.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"

AZombiesSpawner::AZombiesSpawner()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
}

AZombiesCharacter* AZombiesSpawner::SpawnCharacter_FromServer(UClass* classToSpawn) const
{
	FActorSpawnParameters spawnParameters;
	spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	AZombiesCharacter* spawnedCharacter = GetWorld()->SpawnActor<AZombiesCharacter>(classToSpawn, GetActorTransform(), spawnParameters);

	return spawnedCharacter;
}

bool AZombiesSpawner::IsPlayerNearby_FromServer()
{
	// 1- Sphere collision to search possible targets
	TArray<AActor*> possibleTargets;
	
	TArray<TEnumAsByte<EObjectTypeQuery>> traceObjectTypes;
	traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));
	
	TArray<AActor*> ignoreActors;
	ignoreActors.Init(this, 1);

	UClass* seekClass = AZombiesPlayerCharacter::StaticClass();
	
	UKismetSystemLibrary::SphereOverlapActors(GetWorld(), GetActorLocation(), DistanceToCheckPlayer,
		traceObjectTypes, seekClass, ignoreActors, possibleTargets);
	
	// 2- From Collided Actors, check that are players
	for(AActor* currentActor : possibleTargets)
	{
		AZombiesPlayerCharacter* player = Cast<AZombiesPlayerCharacter>(currentActor);
		if(player)
			return true;
	}

	return false;
}
