#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ZombiesEnemySpawnerArea.generated.h"

class AZombiesSpawner;
class UBoxComponent;
class AZombiesPlayerCharacter;
class AZombiesEnemyCharacter;

/**
 * This Manager's functions always run on the Server.
 **/

UCLASS(Blueprintable, BlueprintType)
class ZOMBIES_API AZombiesEnemySpawnerArea : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TArray<AZombiesSpawner*> Spawners;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* PhysicalArea = nullptr;

	UPROPERTY()
	TArray<AZombiesPlayerCharacter*> PlayersInThisArea;

	UPROPERTY()
	TArray<AZombiesEnemyCharacter*> ZombiesInThisArea;
	
public:	
	AZombiesEnemySpawnerArea();

	UFUNCTION()
	AZombiesEnemyCharacter* SpawnEnemy(UClass* classToSpawn);

	UFUNCTION()
	void AddPlayerToThisArea(AZombiesPlayerCharacter* player) { PlayersInThisArea.AddUnique(player); }
	UFUNCTION()
	void RemovePlayerFromThisArea(AZombiesPlayerCharacter* player) { PlayersInThisArea.Remove(player); }

	UFUNCTION()
	void AddZombieToThisArea(AZombiesEnemyCharacter* zombie) { ZombiesInThisArea.AddUnique(zombie); }
	UFUNCTION()
	void RemoveZombieFromThisArea(AZombiesEnemyCharacter* zombie) { ZombiesInThisArea.Remove(zombie); }
	
	UFUNCTION()
	TArray<AZombiesPlayerCharacter*>& GetPlayersInThisArea() { return PlayersInThisArea; }
	UFUNCTION()
	TArray<AZombiesEnemyCharacter*>& GetZombiesInThisArea() { return ZombiesInThisArea; }

	UFUNCTION()
	bool AreAnyPlayersInsideThisArea() const { return PlayersInThisArea.Num() > 0; }

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnPawnEnteredPhysicalArea(UPrimitiveComponent* overlappedComp, AActor* otherActor,
		UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnPawnLeavedPhysicalArea(UPrimitiveComponent* overlappedComp, AActor* otherActor,
		UPrimitiveComponent* otherComp, int32 otherBodyIndex);
};
