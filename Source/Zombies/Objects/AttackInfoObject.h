#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "AttackInfoObject.generated.h"

UCLASS(Abstract, BlueprintType, Blueprintable)
class ZOMBIES_API UAttackInfoObject : public UObject
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Attack", meta = (AllowPrivateAccess = "true"))
	UAnimMontage* AttackMontage = nullptr;

	UPROPERTY(EditAnywhere, Category = "Attack", meta = (AllowPrivateAccess = "true"))
	float AttackDamage = 10.f;

public:
	UAttackInfoObject(){}

	UFUNCTION()
	UAnimMontage* GetAttackMontage() const { return AttackMontage; }

	UFUNCTION()
	float GetAttackDamage() const { return AttackDamage; }
};
