#pragma once

#include "CoreMinimal.h"
#include "Zombies/Objects/ZombiesPerkObject.h"
#include "ZombiesJuggernogPerk.generated.h"

/** Doubles the health of the player. */

UCLASS(Abstract, BlueprintType, Blueprintable)
class ZOMBIES_API UZombiesJuggernogPerk : public UZombiesPerkObject
{
	GENERATED_BODY()

public:
	virtual void ApplyPerkToPlayer(AZombiesPlayerCharacter* player) override;
	
	virtual void RemovePerkFromPlayer(AZombiesPlayerCharacter* player) override;
};
