#include "ZombiesQuickRevivePerk.h"

#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/Players/ZombiesPlayerDeathComponent.h"

void UZombiesQuickRevivePerk::ApplyPerkToPlayer(AZombiesPlayerCharacter* player)
{
	const float newReviveTime = player->GetPlayerDeathComponent()->GetDefaultReviveTime() * 0.5f;
	player->GetPlayerDeathComponent()->SetAndSynchroNewReviveTime(newReviveTime);
}

void UZombiesQuickRevivePerk::RemovePerkFromPlayer(AZombiesPlayerCharacter* player)
{
	const float newReviveTime = player->GetPlayerDeathComponent()->GetDefaultReviveTime();
	player->GetPlayerDeathComponent()->SetAndSynchroNewReviveTime(newReviveTime);
}
