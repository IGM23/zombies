#pragma once

#include "CoreMinimal.h"
#include "Zombies/Objects/ZombiesPerkObject.h"
#include "ZombiesStaminUpPerk.generated.h"

/** Rises movement speed of the player. */

UCLASS(Abstract, BlueprintType, Blueprintable)
class ZOMBIES_API UZombiesStaminUpPerk : public UZombiesPerkObject
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, meta = ( AllowPrivateAccess = "true" ))
	float MovementSpeedExtra = 150.f;

	UPROPERTY(EditAnywhere, meta = ( AllowPrivateAccess = "true" ))
	float AccelerationExtra = 300.f;

public:
	virtual void ApplyPerkToPlayer(AZombiesPlayerCharacter* player) override;
	
	virtual void RemovePerkFromPlayer(AZombiesPlayerCharacter* player) override;
};
