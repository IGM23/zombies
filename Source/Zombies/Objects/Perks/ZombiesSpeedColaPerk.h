#pragma once

#include "CoreMinimal.h"
#include "Zombies/Objects/ZombiesPerkObject.h"
#include "ZombiesSpeedColaPerk.generated.h"

/** Halves the reloading time. */

UCLASS(Abstract, BlueprintType, Blueprintable)
class ZOMBIES_API UZombiesSpeedColaPerk : public UZombiesPerkObject
{
	GENERATED_BODY()

	virtual void ApplyPerkToPlayer(AZombiesPlayerCharacter* player) override;
	
	virtual void RemovePerkFromPlayer(AZombiesPlayerCharacter* player) override;
	
};
