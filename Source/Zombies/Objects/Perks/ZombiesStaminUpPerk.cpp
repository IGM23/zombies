#include "ZombiesStaminUpPerk.h"

#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"

void UZombiesStaminUpPerk::ApplyPerkToPlayer(AZombiesPlayerCharacter* player)
{
	player->GetCharStatsComponent()->AddMovementSpeedAndAcc(MovementSpeedExtra, AccelerationExtra);
}

void UZombiesStaminUpPerk::RemovePerkFromPlayer(AZombiesPlayerCharacter* player)
{
	player->GetCharStatsComponent()->AddMovementSpeedAndAcc(-MovementSpeedExtra, -AccelerationExtra);
}
