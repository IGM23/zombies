#include "ZombiesSpeedColaPerk.h"

#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/Players/ZombiesWeaponsManagerCmp.h"

void UZombiesSpeedColaPerk::ApplyPerkToPlayer(AZombiesPlayerCharacter* player)
{
	player->GetWeaponsManagerComponent()->SetReloadTimeModifier(0.5f);
}

void UZombiesSpeedColaPerk::RemovePerkFromPlayer(AZombiesPlayerCharacter* player)
{
	player->GetWeaponsManagerComponent()->SetReloadTimeModifier(1.f);
}
