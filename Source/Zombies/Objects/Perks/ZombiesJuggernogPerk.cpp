#include "ZombiesJuggernogPerk.h"

#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"

void UZombiesJuggernogPerk::ApplyPerkToPlayer(AZombiesPlayerCharacter* player)
{
	player->GetCharStatsComponent()->DoubleHealth();
}

void UZombiesJuggernogPerk::RemovePerkFromPlayer(AZombiesPlayerCharacter* player)
{
	player->GetCharStatsComponent()->RestoreDefaultHealth();
}
