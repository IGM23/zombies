#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ZombiesPerkObject.generated.h"

class AZombiesPlayerCharacter;

UCLASS(Abstract, BlueprintType, Blueprintable)
class ZOMBIES_API UZombiesPerkObject : public UObject
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup Perk" , meta = (AllowPrivateAccess = "true"))
	FString PerkName = "Perk name";

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup Perk" , meta = (AllowPrivateAccess = "true"))
	UTexture2D* PerkIcon = nullptr;

public:
	UFUNCTION()
	virtual void ApplyPerkToPlayer(AZombiesPlayerCharacter* player) PURE_VIRTUAL(UZombiesPerkObject::ApplyPerkToPlayer, ;)
	UFUNCTION()
	virtual void RemovePerkFromPlayer(AZombiesPlayerCharacter* player) PURE_VIRTUAL(UZombiesPerkObject::RemovePerkFromPlayer, ;)

	UFUNCTION()
	FString GetPerkName() const { return PerkName; }
	
	UFUNCTION()
	UTexture2D* GetPerkIcon() const { return PerkIcon; }
};
