#include "ZombiesCharStatsComponent.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerState.h"
#include "Zombies/Characters/ZombiesCharacter.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"

UZombiesCharStatsComponent::UZombiesCharStatsComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UZombiesCharStatsComponent::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = MaxHealth;
	DefaultMaxHealth = MaxHealth;
}

void UZombiesCharStatsComponent::AddMaxHealth(float amountToAdd)
{
	const float newHealth = FMath::Clamp(MaxHealth + amountToAdd, 0.f, MaxHealth);
	MaxHealth = newHealth;
}

void UZombiesCharStatsComponent::AddCurrentHealth(float amountToAdd)
{
	const float newHealth = FMath::Clamp(CurrentHealth + amountToAdd, 0.f, MaxHealth);
	CurrentHealth = newHealth;

	if(amountToAdd < 0.f)
		OnReceivedDamageDelegate.Broadcast();
	else if(CurrentHealth == MaxHealth)
		OnMaxHealedDelegate.Broadcast();
}

void UZombiesCharStatsComponent::RestoreCurrentHealthToMax()
{
	CurrentHealth = MaxHealth;
	OnMaxHealedDelegate.Broadcast();
}

void UZombiesCharStatsComponent::DoubleHealth()
{
	if(GetNetMode() == NM_Client)
		DoubleHealth_Server();
	else
		DoubleHealth_Internal();
}

void UZombiesCharStatsComponent::DoubleHealth_Server_Implementation()
{
	DoubleHealth_Internal();
}

void UZombiesCharStatsComponent::DoubleHealth_Internal()
{
	const float newMaxHealth = MaxHealth * 2.f;
	SetMaxHealth(newMaxHealth);
	SetCurrentHealth(newMaxHealth);
}

void UZombiesCharStatsComponent::RestoreDefaultHealth()
{
	if(GetNetMode() == NM_Client)
		RestoreDefaultHealth_Server();
	else
		RestoreDefaultHealth_Internal();
}

void UZombiesCharStatsComponent::RestoreDefaultHealth_Server_Implementation()
{
	RestoreDefaultHealth_Internal();
}

void UZombiesCharStatsComponent::RestoreDefaultHealth_Internal()
{
	SetMaxHealth(DefaultMaxHealth);
	SetCurrentHealth(DefaultMaxHealth);
}

void UZombiesCharStatsComponent::AddMovementSpeedAndAcc(float speedToAdd, float accToAdd)
{
	if(GetNetMode() == NM_Client)
		AddMovementSpeedAndAcc_Server(speedToAdd, accToAdd);
	else
		AddMovementSpeedAndAcc_Multicast(speedToAdd, accToAdd);
}

void UZombiesCharStatsComponent::AddMovementSpeedAndAcc_Server_Implementation(float speedToAdd, float accToAdd)
{
	AddMovementSpeedAndAcc_Multicast(speedToAdd, accToAdd);
}

void UZombiesCharStatsComponent::AddMovementSpeedAndAcc_Multicast_Implementation(float speedToAdd, float accToAdd)
{
	AZombiesPlayerCharacter* ownerPlayer = Cast<AZombiesPlayerCharacter>(GetOwner());
	if(ownerPlayer)
	{
		ownerPlayer->GetCharacterMovement()->MaxWalkSpeed += speedToAdd;
		ownerPlayer->GetCharacterMovement()->MaxAcceleration += accToAdd;
	}
}

float UZombiesCharStatsComponent::GetPlayerPoints() const
{
	const AZombiesCharacter* ownerCharacter = Cast<AZombiesCharacter>(GetOwner());
	return ownerCharacter->GetPlayerState()->GetScore();
}

void UZombiesCharStatsComponent::SetPlayerPoints(float newPoints)
{
	const AZombiesCharacter* ownerCharacter = Cast<AZombiesCharacter>(GetOwner());
	ownerCharacter->GetPlayerState()->SetScore(newPoints);
}

void UZombiesCharStatsComponent::AddPlayerPoints(float pointsToAdd)
{
	const AZombiesCharacter* ownerCharacter = Cast<AZombiesCharacter>(GetOwner());
	
	float playerPoints = ownerCharacter->GetPlayerState()->GetScore() + pointsToAdd;
	if(playerPoints < 0.f)
		playerPoints = 0.f;
	
	ownerCharacter->GetPlayerState()->SetScore(playerPoints);
}