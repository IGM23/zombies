#include "ZombiesEnemiesDeathComponent.h"

#include "Zombies/Characters/ZombiesEnemyCharacter.h"
#include "Zombies/Controllers/ZombiesEnemyBaseController.h"

void UZombiesEnemiesDeathComponent::BeginPlay()
{
	Super::BeginPlay();
	OwnerEnemyCharacter = Cast<AZombiesEnemyCharacter>(GetOwner());
}

void UZombiesEnemiesDeathComponent::OnCharacterLifeReachesZero()
{
	AZombiesEnemyBaseController* enemyController = Cast<AZombiesEnemyBaseController>(OwnerEnemyCharacter->GetController());
	if(enemyController)
		enemyController->EndBehavior();
	
	KillCharacter();
}
