#pragma once

#include "CoreMinimal.h"
#include "Zombies/Components/ZombiesDeathComponent.h"
#include "ZombiesEnemiesDeathComponent.generated.h"

class AZombiesEnemyCharacter;

UCLASS(Abstract, Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesEnemiesDeathComponent : public UZombiesDeathComponent
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	AZombiesEnemyCharacter* OwnerEnemyCharacter = nullptr;

public:
	UZombiesEnemiesDeathComponent() {}

	virtual void OnCharacterLifeReachesZero() override;

protected:
	virtual void BeginPlay() override;
};
