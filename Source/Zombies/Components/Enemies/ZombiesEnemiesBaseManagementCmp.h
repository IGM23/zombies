#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ZombiesEnemiesBaseManagementCmp.generated.h"

class AZombiesEnemyBaseController;
class AZombiesCharacter;
class AZombiesPlayerCharacter;
class AZombiesEnemyCharacter;

/*
 * This component manages information that will be used on enemies Behavior Tree's Tasks, Decorators and Services.
 **/

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesEnemiesBaseManagementCmp : public UActorComponent
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly)
	AZombiesEnemyCharacter* OwnerEnemyCharacter = nullptr;

	UPROPERTY(BlueprintReadOnly)
	AZombiesEnemyBaseController* OwnerEnemyController = nullptr;

	UPROPERTY(BlueprintReadOnly)
	AZombiesPlayerCharacter* TargetCharacter = nullptr;

	/** After this amount of time has passed, the enemy will search again for the closest Player. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float TimeBeforeChangingTarget = 20.f;

private:
	UPROPERTY()
	float TimeBeforeChangingTarget_Acc = 0.f;

public:	
	UZombiesEnemiesBaseManagementCmp();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/** Searches for the closest ALIVE player. */
	UFUNCTION()
	void SearchForClosestPlayer();

	UFUNCTION()
	bool HasToChangeTarget() const;

	UFUNCTION()
	void ResetChangingTargetTimer();

	UFUNCTION()
	bool IsTargetCharacterDown() const;

	UFUNCTION(BlueprintCallable)
	AZombiesEnemyCharacter* GetOwnerEnemyCharacter() { return OwnerEnemyCharacter; }

	UFUNCTION(BlueprintCallable)
	AZombiesPlayerCharacter* GetTargetCharacter() { return TargetCharacter; }

protected:
	virtual void BeginPlay() override;

private:
	bool CheckIsCharacterDown(AZombiesCharacter* characterToCheck) const;
};
