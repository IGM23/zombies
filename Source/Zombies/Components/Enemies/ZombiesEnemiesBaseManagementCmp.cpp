#include "ZombiesEnemiesBaseManagementCmp.h"

#include "Zombies/Characters/ZombiesEnemyCharacter.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"
#include "Zombies/Controllers/ZombiesEnemyBaseController.h"
#include "Zombies/GameStates/ZombiesGameState.h"


UZombiesEnemiesBaseManagementCmp::UZombiesEnemiesBaseManagementCmp()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UZombiesEnemiesBaseManagementCmp::TickComponent(float DeltaTime, ELevelTick TickType,
	FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	TimeBeforeChangingTarget_Acc += DeltaTime;
}

void UZombiesEnemiesBaseManagementCmp::SearchForClosestPlayer()
{
	AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
	if(gameState)
	{
		TargetCharacter = nullptr;
		TArray<AZombiesPlayerCharacter*> players = gameState->GetPlayerCharacters();
		for(int32 i = 0; i < players.Num(); ++i)
		{
			if((!CheckIsCharacterDown(players[i])) && (!TargetCharacter ||
				FVector::DistSquared(OwnerEnemyCharacter->GetActorLocation(), players[i]->GetActorLocation()) <
				FVector::DistSquared(OwnerEnemyCharacter->GetActorLocation(), TargetCharacter->GetActorLocation())))
			{
				TargetCharacter = players[i];
			}
		}
	}
	
	if(TargetCharacter)
		OwnerEnemyController->SetTargetPlayerAndLocation(TargetCharacter);
	else
		OwnerEnemyController->ClearTargetPlayerAndLocation();
}

bool UZombiesEnemiesBaseManagementCmp::HasToChangeTarget() const
{
	if(TimeBeforeChangingTarget_Acc >= TimeBeforeChangingTarget || TargetCharacter == nullptr)
		return true;

	return false;
}

void UZombiesEnemiesBaseManagementCmp::ResetChangingTargetTimer()
{
	TimeBeforeChangingTarget_Acc = 0.f;
}

bool UZombiesEnemiesBaseManagementCmp::IsTargetCharacterDown() const
{
	if(TargetCharacter && TargetCharacter->GetCharStatsComponent()->IsCharacterDowned())
		return true;

	return false;
}

bool UZombiesEnemiesBaseManagementCmp::CheckIsCharacterDown(AZombiesCharacter* characterToCheck) const
{
	if(characterToCheck && characterToCheck->GetCharStatsComponent()->IsCharacterDowned())
		return true;

	return false;
}

void UZombiesEnemiesBaseManagementCmp::BeginPlay()
{
	Super::BeginPlay();

	OwnerEnemyCharacter = Cast<AZombiesEnemyCharacter>(GetOwner());
	if(OwnerEnemyCharacter)
		OwnerEnemyController = Cast<AZombiesEnemyBaseController>(OwnerEnemyCharacter->GetController());
}
