#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ZombiesAttacksComponent.generated.h"

class AZombiesCharacter;
class UAttackInfoObject;

/**
 * Component which contains different attacks (animation montages) and each attack's damage.
 * For enemies, their AI will trigger them. For players, an Input Action will trigger them.
 **/
UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesAttacksComponent : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY()
	AZombiesCharacter* OwnerCharacter = nullptr;
	
	UPROPERTY()
	bool IsAttacking = false;

	UPROPERTY()
	FTimerHandle AttackEndedTimerHandle;

	UPROPERTY()
	UAttackInfoObject* LastAttack = nullptr;
	
	UPROPERTY(EditAnywhere, Category = "Attacks", meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<UAttackInfoObject>> AttacksMap;

	/** When an attack lands, all characters hit by that single attack are stored here to apply damage ONLY ONCE.
	 * When the attack ends, the Array is emptied. */
	UPROPERTY()
	TArray<AZombiesCharacter*> AttackedCharacters;

public:	
	UZombiesAttacksComponent();

	/** The BT calls this function, so it is always from the Server */
	UFUNCTION()
	void UseRandomAttack_FromServer();

	UFUNCTION()
	void EndAttack();

	UFUNCTION()
	void OnAttackLanded(AZombiesCharacter* targetCharacter);

	UFUNCTION(BlueprintCallable)
	bool GetIsAttacking() const { return IsAttacking; }
	UFUNCTION(BlueprintCallable)
	void SetIsAttacking(bool newAttackingStatus);

	UFUNCTION(BlueprintCallable)
	UAttackInfoObject* GetLastAttack() { return LastAttack; }
	UFUNCTION(BlueprintCallable)
	void SetLastAttack(UAttackInfoObject* newAttack) { LastAttack = newAttack; }
	
	UFUNCTION()
	TArray<TSubclassOf<UAttackInfoObject>>& GetAttacksMap() { return AttacksMap; }

protected:
	virtual void BeginPlay() override;

private:
	UFUNCTION(Server, Reliable)
	void OnAttackLanded_Server(AZombiesCharacter* targetCharacter);

	UFUNCTION(NetMulticast, Reliable)
	void PlayAttackAnimation_Multicast(AZombiesCharacter* character, UAnimMontage* attackMontage);

	UFUNCTION(NetMulticast, Reliable)
	void StopAttackAnimation_Multicast(AZombiesCharacter* character, UAnimMontage* attackMontage);
};
