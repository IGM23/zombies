#include "ZombiesInteractionComponent.h"

#include "ZombiesCharStatsComponent.h"
#include "Players/ZombiesUpdateUIComponent.h"
#include "Zombies/Actors/Interactables/ZombiesInteractableActor.h"
#include "Zombies/Characters/ZombiesEnemyCharacter.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"


UZombiesInteractionComponent::UZombiesInteractionComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UZombiesInteractionComponent::BeginPlay()
{
	Super::BeginPlay();
	
	OwnerPlayerCharacter = Cast<AZombiesPlayerCharacter>(GetOwner());
}

void UZombiesInteractionComponent::DamageTarget_FromServer(AZombiesCharacter* targetToDamage, float damageAmount) const
{
	bool hasTargetDied = false;
	if(CheckIfPlayerHasHitAnEnemy(targetToDamage))
	{
		float pointsToAdd = targetToDamage->GetCharStatsComponent()->GetPointsGivenPerShoot();
		targetToDamage->GetCharStatsComponent()->AddCurrentHealth(-damageAmount);
		if(targetToDamage->GetCharStatsComponent()->GetCurrentHealth() <= 0.f)
		{
			pointsToAdd =+ targetToDamage->GetCharStatsComponent()->GetPointsGivenPerDeath();
			hasTargetDied = true;
		}

		AddPointsAndUpdateUI_Internal(pointsToAdd);
	}
	else
	{
		targetToDamage->GetCharStatsComponent()->AddCurrentHealth(-damageAmount);
		UpdateTargetHealthUIAndDamageScreenAndSound(targetToDamage);

		if(targetToDamage->GetCharStatsComponent()->GetCurrentHealth() <= 0.f)
			hasTargetDied = true;
	}

	if(hasTargetDied)
		targetToDamage->TryToKillCharacter();
}

void UZombiesInteractionComponent::AddPointsAndUpdateUI(float pointsToAdd) const
{
	if(GetOwner()->GetLocalRole() == ROLE_Authority)
		AddPointsAndUpdateUI_Internal(pointsToAdd);
	else
		AddPointsAndUpdateUI_Server(pointsToAdd);
}

void UZombiesInteractionComponent::AddPointsAndUpdateUI_Server_Implementation(float pointsToAdd) const
{
	AddPointsAndUpdateUI_Internal(pointsToAdd);
}

void UZombiesInteractionComponent::AddPointsAndUpdateUI_Internal(float pointsToAdd) const
{
	if(OwnerPlayerCharacter)
	{
		OwnerPlayerCharacter->GetCharStatsComponent()->AddPlayerPoints(pointsToAdd);
		OwnerPlayerCharacter->GetUpdateUIComponent()->UpdatePointsUI();
	}
}

void UZombiesInteractionComponent::UpdateTargetHealthUIAndDamageScreenAndSound(AZombiesCharacter* characterToUpdate) const
{
	const AZombiesPlayerCharacter* playerCharacter = Cast<AZombiesPlayerCharacter>(characterToUpdate);
	if(playerCharacter)
	{
		playerCharacter->GetUpdateUIComponent()->UpdateHealthUI();
		playerCharacter->GetUpdateUIComponent()->ActivateDamageScreenAndSound(DamageDealtToPlayerSound);
	}
}

bool UZombiesInteractionComponent::CheckIfPlayerHasHitAnEnemy(AZombiesCharacter* targetToCheck) const
{
	if(Cast<AZombiesEnemyCharacter>(targetToCheck) && Cast<AZombiesPlayerCharacter>(GetOwner()))
		return true;

	return false;
}

void UZombiesInteractionComponent::InteractWithInteractableActor() const
{
	if(OwnerPlayerCharacter && InteractableActorAvailable)
		InteractableActorAvailable->Interact(OwnerPlayerCharacter);
}

void UZombiesInteractionComponent::ReplicateInteractableActorInteraction(bool isMulticast)
{
	if(GetOwner()->GetLocalRole() == ROLE_Authority)
	{
		if(isMulticast)
			ReplicateInteractableActorInteraction_Multicast(InteractableActorAvailable);
		else
			ReplicateInteractableActorInteraction_Internal(InteractableActorAvailable);
	}
	else
		ReplicateInteractableActorInteraction_Server(isMulticast, InteractableActorAvailable);
}

void UZombiesInteractionComponent::ReplicateInteractableActorInteraction_Server_Implementation(bool isMulticast, AZombiesInteractableActor* interactableActor)
{
	if(isMulticast)
		ReplicateInteractableActorInteraction_Multicast(interactableActor);
	else
		ReplicateInteractableActorInteraction_Internal(interactableActor);
}

void UZombiesInteractionComponent::ReplicateInteractableActorInteraction_Multicast_Implementation(AZombiesInteractableActor* interactableActor)
{
	ReplicateInteractableActorInteraction_Internal(interactableActor);
}

void UZombiesInteractionComponent::ReplicateInteractableActorInteraction_Internal(AZombiesInteractableActor* interactableActor)
{
	if(interactableActor)
		interactableActor->ReplicateInteraction();
}

void UZombiesInteractionComponent::SetInteractableActorAvailable(AZombiesInteractableActor* interactable)
{
	InteractableActorAvailable = interactable;
	if(OwnerPlayerCharacter->GetUpdateUIComponent())
	{
		if(InteractableActorAvailable)
			OwnerPlayerCharacter->GetUpdateUIComponent()->UpdateInformationText(InteractableActorAvailable->GetInteractionInfoText());
		else
			OwnerPlayerCharacter->GetUpdateUIComponent()->HideInformationText();
	}
}
