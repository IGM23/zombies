#include "ZombiesDeathComponent.h"

#include "ZombiesCharStatsComponent.h"
#include "Zombies/Animations/AnimInstances/ZombiesCharacterAnimInstance.h"
#include "Zombies/Characters/ZombiesCharacter.h"
#include "Zombies/Managers/ZombiesRoundsManager.h"

UZombiesDeathComponent::UZombiesDeathComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UZombiesDeathComponent::BeginPlay()
{
	Super::BeginPlay();
	OwnerCharacter = Cast<AZombiesCharacter>(GetOwner());
}

void UZombiesDeathComponent::KillCharacter()
{
	ReplicateDeadState_Multicast();
	
	DisableCollisions(true);
	
	UZombiesRoundsManager* roundsManager = GetWorld()->GetSubsystem<UZombiesRoundsManager>();
	if(roundsManager)
		roundsManager->OnCharacterDied(OwnerCharacter);

	GetWorld()->GetTimerManager().SetTimer(DestroyCharacterTimerHandle, this,
		&UZombiesDeathComponent::DestroyCharacter, TimeToDestroyCharacter, false);
}

void UZombiesDeathComponent::ReplicateDeadState_Multicast_Implementation()
{
	UZombiesCharacterAnimInstance* characterAnimInstance = Cast<UZombiesCharacterAnimInstance>(OwnerCharacter->GetMesh()->GetAnimInstance());
	if(characterAnimInstance->IsValidLowLevel())
		characterAnimInstance->Dead = true;

	OwnerCharacter->GetCharStatsComponent()->SetCurrentCharacterState(ECharacterState::Dead);
}

void UZombiesDeathComponent::DestroyCharacter()
{
	GetWorld()->GetTimerManager().ClearTimer(DestroyCharacterTimerHandle);
	OwnerCharacter->Destroy();
}
