#include "ZombiesAttacksComponent.h"

#include "ZombiesCharStatsComponent.h"
#include "ZombiesInteractionComponent.h"
#include "Zombies/Characters/ZombiesCharacter.h"
#include "Zombies/Controllers/ZombiesEnemyBaseController.h"
#include "Zombies/Objects/AttackInfoObject.h"

UZombiesAttacksComponent::UZombiesAttacksComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UZombiesAttacksComponent::BeginPlay()
{
	Super::BeginPlay();
	OwnerCharacter = Cast<AZombiesCharacter>(GetOwner());
}

void UZombiesAttacksComponent::UseRandomAttack_FromServer()
{
	if(OwnerCharacter)
	{
		const int32 randomIndex = FMath::RandRange(0, AttacksMap.Num() - 1);
		const TSubclassOf<UAttackInfoObject> chosenAttack = AttacksMap[randomIndex];
		LastAttack = chosenAttack.GetDefaultObject();

		if(LastAttack)
		{
			SetIsAttacking(true);
			UAnimMontage* montageToPlay = LastAttack->GetAttackMontage();
			PlayAttackAnimation_Multicast(OwnerCharacter, montageToPlay);

			GetWorld()->GetTimerManager().SetTimer(AttackEndedTimerHandle,this, &UZombiesAttacksComponent::EndAttack,
				montageToPlay->GetPlayLength(),false);
		}
	}
}

void UZombiesAttacksComponent::EndAttack()
{
	GetWorld()->GetTimerManager().ClearTimer(AttackEndedTimerHandle);
	AttackedCharacters.Empty();
	SetIsAttacking(false);
	
	if(OwnerCharacter)
		StopAttackAnimation_Multicast(OwnerCharacter, LastAttack->GetAttackMontage());
}

void UZombiesAttacksComponent::OnAttackLanded_Server_Implementation(AZombiesCharacter* targetCharacter)
{
	if(OwnerCharacter && LastAttack && targetCharacter->GetCharStatsComponent()->IsCharacterAlive() && !AttackedCharacters.Contains(targetCharacter))
	{
		AttackedCharacters.Add(targetCharacter);
		OwnerCharacter->GetInteractionComponent()->DamageTarget_FromServer(targetCharacter, LastAttack->GetAttackDamage());
	}
}

void UZombiesAttacksComponent::OnAttackLanded(AZombiesCharacter* targetCharacter)
{
	OnAttackLanded_Server(targetCharacter);
}

void UZombiesAttacksComponent::SetIsAttacking(bool newAttackingStatus)
{
	IsAttacking = newAttackingStatus;
	if(OwnerCharacter)
	{
		AZombiesEnemyBaseController* enemyController = Cast<AZombiesEnemyBaseController>(OwnerCharacter->GetController());
		if(enemyController)
			enemyController->SetIsAttacking(IsAttacking);
	}
}

void UZombiesAttacksComponent::PlayAttackAnimation_Multicast_Implementation(AZombiesCharacter* character, UAnimMontage* attackMontage)
{
	character->PlayAnimMontage(attackMontage);
}

void UZombiesAttacksComponent::StopAttackAnimation_Multicast_Implementation(AZombiesCharacter* character, UAnimMontage* attackMontage)
{
	character->StopAnimMontage(attackMontage);
}