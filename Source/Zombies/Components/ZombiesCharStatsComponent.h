#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ZombiesCharStatsComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnReceivedDamage);
DECLARE_MULTICAST_DELEGATE(FOnMaxHealed);

UENUM(BlueprintType)
enum class ECharacterState : uint8
{
	Alive UMETA(DisplayName = "Alive"),
	Downed UMETA(DisplayName = "Downed"),
	Dead UMETA(DisplayName = "Dead")
};

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesCharStatsComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	/** Fires everytime the character receives damage. */
	FOnReceivedDamage OnReceivedDamageDelegate;

	/** Fires everytime the character heals to Max Health. */
	FOnMaxHealed OnMaxHealedDelegate;	

private:
	///////////////////
	///// COMMON /////
	/////////////////
	UPROPERTY()
	ECharacterState CurrentCharacterState = ECharacterState::Alive;

	UPROPERTY()
	float DefaultMaxHealth = 100.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float MaxHealth = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float CurrentHealth = 100.f;

	///////////////////
	///// ENEMIES/////
	/////////////////
	/** The points that a Player will receive if hits once this enemy. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float PointsGivenPerShoot = 10.f;

	/** The points that a Player will receive if kills this enemy. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float PointsGivenPerDeath = 50.f;

public:	
	UZombiesCharStatsComponent();

	UFUNCTION(BlueprintCallable)
	ECharacterState GetCurrentCharacterState() const { return CurrentCharacterState; }
	UFUNCTION(BlueprintCallable)
	void SetCurrentCharacterState(ECharacterState newState) { CurrentCharacterState = newState; }
	UFUNCTION(BlueprintCallable)
	bool IsCharacterAlive() const { return CurrentCharacterState == ECharacterState::Alive; }
	UFUNCTION(BlueprintCallable)
	bool IsCharacterDowned() const { return CurrentCharacterState == ECharacterState::Downed; }
	UFUNCTION(BlueprintCallable)
	bool IsCharacterDead() const { return CurrentCharacterState == ECharacterState::Dead; }
	
	UFUNCTION(BlueprintCallable)
	float GetDefaultMaxHealth() const { return DefaultMaxHealth; }
	
	UFUNCTION(BlueprintCallable)
	float GetMaxHealth() const { return MaxHealth; }
	UFUNCTION(BlueprintCallable)
	void SetMaxHealth(float newMaxHealth) { MaxHealth = newMaxHealth; }
	UFUNCTION(BlueprintCallable)
	void AddMaxHealth(float amountToAdd);

	UFUNCTION(BlueprintCallable)
	float GetCurrentHealth() const { return CurrentHealth; }
	UFUNCTION(BlueprintCallable)
	float GetCurrentHealthPercentage() const { return (CurrentHealth/MaxHealth); }
	UFUNCTION(BlueprintCallable)
	void SetCurrentHealth(float newCurrentHealth) { CurrentHealth = newCurrentHealth; }
	UFUNCTION(BlueprintCallable)
	void AddCurrentHealth(float amountToAdd);
	UFUNCTION(BlueprintCallable)
	void RestoreCurrentHealthToMax();

	/** Special functions for Juggernog perk, since they can be called from the Client. */
	UFUNCTION()
	void DoubleHealth();
	UFUNCTION()
	void RestoreDefaultHealth();

	/** Special functions for StaminUp perk, since they can be called from the Client. */
	UFUNCTION()
	void AddMovementSpeedAndAcc(float speedToAdd, float accToAdd);

	UFUNCTION(BlueprintCallable)
	float GetPlayerPoints() const;
	UFUNCTION(BlueprintCallable)
	void SetPlayerPoints(float newPoints);
	UFUNCTION(BlueprintCallable)
	void AddPlayerPoints(float pointsToAdd);

	UFUNCTION(BlueprintCallable)
	float GetPointsGivenPerShoot() const { return PointsGivenPerShoot; }

	UFUNCTION(BlueprintCallable)
	float GetPointsGivenPerDeath() const { return PointsGivenPerDeath; }
	
protected:
	virtual void BeginPlay() override;

private:
	//For Juggernog
	UFUNCTION(Server, Reliable)
	void DoubleHealth_Server();
	UFUNCTION()
	void DoubleHealth_Internal();
	
	UFUNCTION(Server, Reliable)
	void RestoreDefaultHealth_Server();
	UFUNCTION()
	void RestoreDefaultHealth_Internal();

	//For StaminUp
	UFUNCTION(Server, Reliable)
	void AddMovementSpeedAndAcc_Server(float speedToAdd, float accToAdd);
	UFUNCTION(NetMulticast, Reliable)
	void AddMovementSpeedAndAcc_Multicast(float speedToAdd, float accToAdd);
};
