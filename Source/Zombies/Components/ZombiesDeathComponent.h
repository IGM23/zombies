#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ZombiesDeathComponent.generated.h"

class AZombiesCharacter;

/**
 * This component executes in the Server.
 * This component manages what happens when a character dies.
 * For enemies, it chooses one random death animation and plays it, and then destroys the character.
 * For players, manages the entering and exiting from the Downed status and its animations. Also it chooses
 * one random death animation and plays it, and then destroys the character.
 */

UCLASS(Abstract, Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesDeathComponent : public UActorComponent
{
	GENERATED_BODY()

protected:
	/** How many seconds we have to wait to destroy this character after they die. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup", meta = (AllowPrivateAccess = "true"))
	float TimeToDestroyCharacter = 10.f;

private:
	UPROPERTY()
	AZombiesCharacter* OwnerCharacter = nullptr;

	UPROPERTY()
	FTimerHandle DestroyCharacterTimerHandle;
	
public:	
	UZombiesDeathComponent();

	/** Called from Server. */
	UFUNCTION()
	virtual void OnCharacterLifeReachesZero() PURE_VIRTUAL(UZombiesDeathComponent::OnCharacterLifeReachesZero, ;)

protected:
	virtual void BeginPlay() override;

	/** True to disable the normal collisions from the ALIVE state. False to enable back the normal collisions again. */
	UFUNCTION(BlueprintImplementableEvent)
	void DisableCollisions(bool disableThem);
	
	UFUNCTION()
	virtual void KillCharacter();

	UFUNCTION(NetMulticast, Reliable)
	void ReplicateDeadState_Multicast();

private:
	UFUNCTION()
	void DestroyCharacter();
};
