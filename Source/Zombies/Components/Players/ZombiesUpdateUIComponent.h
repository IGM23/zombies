#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ZombiesUpdateUIComponent.generated.h"

class AZombiesPlayerCharacter;
class AZombiesPlayerController;

/**
 * This component manages the UI changes of the Players.
*/

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesUpdateUIComponent : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	AZombiesPlayerCharacter* OwnerPlayerCharacter = nullptr;
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	AZombiesPlayerController* OwnerPlayerController = nullptr;

public:	
	UZombiesUpdateUIComponent();

	/** Updates UI for all players. Called from Server.
	 * Only happens when a Player hits an Enemy. */
	UFUNCTION(BlueprintCallable)
	void UpdatePointsUI() const;

	/** Updates UI for all players. Called from Server.
	 * Only happens when an Enemy hits a Player or a player receive damage. */
	UFUNCTION(BlueprintCallable)
	void UpdateHealthUI() const;

	/** Updates UI for the current player. Called from the current player.
	 * Happens when the player shoots, reloads and changes weapon. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void UpdateAmmoUI(FName weaponName, int32 currentReserveAmmo, int32 maxMagazineAmmo, int32 currentMagazineAmmo);

	/** Adds the perk icon to the HUD. Called from the current player.
	 * Happens when the player obtains a perk. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void AddPerkIconToHUD(UTexture2D* perkIcon);

	/** Removes the perk icon to the HUD. Called from the current player.
	 * Happens when the player losses a perk. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void RemovePerkIconFromHUD(UTexture2D* perkIcon);

	UFUNCTION(Client, Reliable)
	void ShowHitmarkerAndSFX_Client(float duration, bool isHeadshoot, USoundBase* hitSound);

	/** Shows and starts the reviving bar. Called from the current player.
	 * Happens when the player is reviving another one. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void StartRevivingUI(float reviveDuration);

	/** Hides the reviving bar. Called from the current player.
	 * Happens when the reviving ends or is interrupted. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void StopRevivingUI();

	/** Shows the message of "You are being revived". Called from the current player.
	 * Happens when the player is being revived by another one. */
	UFUNCTION(Client, Reliable)
	void StartRevivingMessageUI_Client(const FString& reviverName);

	/** Hides the message of "You are being revived". Called from the current player.
	 * Happens when the reviving ends or is interrupted. */
	UFUNCTION(Client, Reliable)
	void StopRevivingMessageUI_Client();
	
	/** Shows and starts the reloading bar. Called from the current player.
	 * Happens when the player reloads. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void StartReloadingUI(float reloadDuration);

	/** Hides the reloading bar. Called from the current player.
	 * Happens when the reload ends or is interrupted. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void StopReloadingUI();

	/** Updates the information text of the HUD. Called from the current player.
	 * Happens when the player enters an Interactable Actor area. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void UpdateInformationText(const FText& newText);

	/** Hides the information text of the HUD. Called from the current player.
	 * Happens when the player exits an Interactable Actor area. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void HideInformationText();

	UFUNCTION(Client, Reliable)
	void SetNewRoundNumber_Client(int32 newRound);

	/** Updates UI and SFX for a specific player. Called from Server.
	 * Happens when the player receives damage. */
	UFUNCTION(Client, Reliable)
	void ActivateDamageScreenAndSound(USoundBase* damageSound);

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void SetupOwnerPlayerController();

	/** Adds the hitmarker icon on screen (different hitmarker if headshot) for some duration.
	 * Called from the current player. Happens when the player's bullet hit an enemy */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ShowHitmarkerAndSFXInHUD(float duration, bool isHeadshoot, USoundBase* hitSound);

	/** Hides all hitmarker icons on screen.
	 * Happens when the hitmarker time finishes. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void HideHitmarkersFromHUD();

	/** Updates the round number in the HUD. Called from the .
	 * Happens when the current round finishes and the next one starts. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void SetNewRoundNumberInHUD(int32 newRound);

	/** Activates the damage screen. It auto-deactivates by itself. Also plays damage SFX. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ActivateDamageScreenInHUDAndSFX(USoundBase* damageSound);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void StartRevivingMessageUI(const FString& reviverName);

	/** Hides the message of "You are being revived". Called from the current player.
	 * Happens when the reviving ends or is interrupted. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void StopRevivingMessageUI();
};
