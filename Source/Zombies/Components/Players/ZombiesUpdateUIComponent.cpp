#include "ZombiesUpdateUIComponent.h"

#include "GameFramework/PlayerState.h"
#include "Zombies/Characters/ZombiesCharacter.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"
#include "Zombies/Controllers/ZombiesPlayerController.h"
#include "Zombies/GameStates/ZombiesGameState.h"

UZombiesUpdateUIComponent::UZombiesUpdateUIComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UZombiesUpdateUIComponent::BeginPlay()
{
	Super::BeginPlay();
	OwnerPlayerCharacter = Cast<AZombiesPlayerCharacter>(GetOwner());

	SetupOwnerPlayerController();
}

void UZombiesUpdateUIComponent::SetupOwnerPlayerController()
{
	if(OwnerPlayerCharacter)
		OwnerPlayerController = Cast<AZombiesPlayerController>(OwnerPlayerCharacter->GetController());
}

void UZombiesUpdateUIComponent::UpdatePointsUI() const
{
	if(OwnerPlayerCharacter)
	{
		AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
		FString playerName = OwnerPlayerCharacter->GetPlayerState()->GetHumanReadableName();
		const float playerScore = OwnerPlayerCharacter->GetPlayerState()->GetScore();
		gameState->UpdatePlayerScoreHUD_FromServer(playerName, playerScore);
	}
}

void UZombiesUpdateUIComponent::UpdateHealthUI() const
{
	if(OwnerPlayerCharacter)
	{
		AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
		FString playerName = OwnerPlayerCharacter->GetPlayerState()->GetHumanReadableName();
		const float lifePercentage = OwnerPlayerCharacter->GetCharStatsComponent()->GetCurrentHealthPercentage();
		gameState->UpdatePlayerLifeHUD_FromServer(playerName, lifePercentage);
	}
}

void UZombiesUpdateUIComponent::StartRevivingMessageUI_Client_Implementation(const FString& reviverName)
{
	StartRevivingMessageUI(reviverName);
}

void UZombiesUpdateUIComponent::StopRevivingMessageUI_Client_Implementation()
{
	StopRevivingMessageUI();
}

void UZombiesUpdateUIComponent::ShowHitmarkerAndSFX_Client_Implementation(float duration, bool isHeadshoot, USoundBase* hitSound)
{
	ShowHitmarkerAndSFXInHUD(duration, isHeadshoot, hitSound);
}

void UZombiesUpdateUIComponent::SetNewRoundNumber_Client_Implementation(int32 newRound)
{
	SetNewRoundNumberInHUD(newRound);
}

void UZombiesUpdateUIComponent::ActivateDamageScreenAndSound_Implementation(USoundBase* damageSound)
{
	ActivateDamageScreenInHUDAndSFX(damageSound);
}