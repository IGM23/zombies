#include "ZombiesPlayerDeathComponent.h"

#include "ZombiesPerksComponent.h"
#include "ZombiesUpdateUIComponent.h"
#include "Components/SphereComponent.h"
#include "Zombies/Animations/AnimInstances/ZombiesCharacterAnimInstance.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"
#include "Zombies/GameStates/ZombiesGameState.h"

void UZombiesPlayerDeathComponent::BeginPlay()
{
	Super::BeginPlay();

	CurrentReviveTime = DefaultReviveTime;
	OwnerPlayerCharacter = Cast<AZombiesPlayerCharacter>(GetOwner());

	if(OwnerPlayerCharacter->GetNetMode() == NM_ListenServer)
	{
		USphereComponent* sphereRevivalAreaComponent = OwnerPlayerCharacter->GetRevivalAreaComponent();
		sphereRevivalAreaComponent->OnComponentBeginOverlap.AddDynamic(this, &UZombiesPlayerDeathComponent::OnPlayerEntersRevivalArea);
		sphereRevivalAreaComponent->OnComponentEndOverlap.AddDynamic(this, &UZombiesPlayerDeathComponent::OnPlayerLeavesRevivalArea);
	}
}

void UZombiesPlayerDeathComponent::OnPlayerEntersRevivalArea(UPrimitiveComponent* overlappedComponent,
	AActor* otherActor, UPrimitiveComponent* otherComponent, int32 otherBodyIndex, bool bFromSweep, const FHitResult& hit)
{
	if(CurrentlyRevivingPlayer)
		return;
	
	AZombiesPlayerCharacter* otherPlayer = Cast<AZombiesPlayerCharacter>(otherActor);
	if(otherPlayer && !otherPlayer->GetCharStatsComponent()->IsCharacterAlive())
	{
		CurrentlyRevivingPlayer = otherPlayer;
		StartReviveProcess_Client(true, otherPlayer);
		GetWorld()->GetTimerManager().SetTimer(ReviveTimerHandle,this, &UZombiesPlayerDeathComponent::OnReviveBarCompleted,
			CurrentReviveTime,false);

		UpdateBeingRevivedMessage(true);
	}
}

void UZombiesPlayerDeathComponent::OnPlayerLeavesRevivalArea(UPrimitiveComponent* overlappedComp, AActor* otherActor,
	UPrimitiveComponent* otherComp, int32 otherBodyIndex)
{
	if(!CurrentlyRevivingPlayer)
		return;

	AZombiesPlayerCharacter* otherPlayer = Cast<AZombiesPlayerCharacter>(otherActor);
	if(otherPlayer && !otherPlayer->GetCharStatsComponent()->IsCharacterAlive() && otherPlayer == CurrentlyRevivingPlayer)
		StopReviving();
}

void UZombiesPlayerDeathComponent::OnReviveBarCompleted()
{
	CurrentlyRevivingPlayer->GetPlayerDeathComponent()->OnPlayerRevived();
	StopReviving();
}

void UZombiesPlayerDeathComponent::StopReviving()
{
	UpdateBeingRevivedMessage(false);
	CurrentlyRevivingPlayer = nullptr;
	StartReviveProcess_Client(false, nullptr);
	GetWorld()->GetTimerManager().ClearTimer(ReviveTimerHandle);
}

void UZombiesPlayerDeathComponent::StartReviveProcess_Client_Implementation(bool isStarting, AZombiesPlayerCharacter* revivingPlayer)
{
	CurrentlyRevivingPlayer = revivingPlayer;
	
	if(isStarting)
		OwnerPlayerCharacter->GetUpdateUIComponent()->StartRevivingUI(CurrentReviveTime);
	else
		OwnerPlayerCharacter->GetUpdateUIComponent()->StopRevivingUI();
}

void UZombiesPlayerDeathComponent::SetAndSynchroNewReviveTime(float newTime)
{
	SetNewReviveTime(newTime);
	SetNewReviveTimeOnServer(newTime);
}

void UZombiesPlayerDeathComponent::SetNewReviveTimeOnServer_Implementation(float newTime)
{
	SetNewReviveTime(newTime);
}

void UZombiesPlayerDeathComponent::OnCharacterLifeReachesZero()
{
	ReplicateDownState_Multicast(true);

	if(IsRevivingAnotherPlayer())
		StopReviving();

	OwnerPlayerCharacter->GetPerksComponent()->RemoveAndDeactivateAllPerks_FromServer();

	AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
	if(gameState)
		gameState->AddDownedPlayerAndCheckEndGame_FromServer(OwnerPlayerCharacter);
}

void UZombiesPlayerDeathComponent::OnPlayerRevived()
{
	ReplicateDownState_Multicast(false);

	OwnerPlayerCharacter->GetCharStatsComponent()->RestoreCurrentHealthToMax();
	OwnerPlayerCharacter->GetUpdateUIComponent()->UpdateHealthUI();
	
	AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
	if(gameState)
		gameState->RemoveDownedPlayer_FromServer(OwnerPlayerCharacter);
}

void UZombiesPlayerDeathComponent::ReplicateDownState_Multicast_Implementation(bool isDowned)
{
	DisableCollisions(isDowned);

	if(isDowned)
		OwnerPlayerCharacter->GetCharStatsComponent()->SetCurrentCharacterState(ECharacterState::Downed);
	else
		OwnerPlayerCharacter->GetCharStatsComponent()->SetCurrentCharacterState(ECharacterState::Alive);
	
	UZombiesCharacterAnimInstance* characterAnimInstance = Cast<UZombiesCharacterAnimInstance>(OwnerPlayerCharacter->GetMesh()->GetAnimInstance());
	if(characterAnimInstance->IsValidLowLevel())
		characterAnimInstance->Downed = isDowned;
}

bool UZombiesPlayerDeathComponent::IsRevivingAnotherPlayer() const
{
	if(CurrentlyRevivingPlayer)
		return true;

	return false;
}

void UZombiesPlayerDeathComponent::UpdateBeingRevivedMessage(bool bShowMessage)
{
	if(!CurrentlyRevivingPlayer)
		return;
	
	if(bShowMessage)
	{
		APlayerController* reviverPlayerController = Cast<APlayerController>(OwnerPlayerCharacter->GetController());
		if(reviverPlayerController)
		{
			const FString reviverName = reviverPlayerController->GetHumanReadableName();
			CurrentlyRevivingPlayer->GetUpdateUIComponent()->StartRevivingMessageUI_Client(reviverName);
		}
	}
	else
	{
		CurrentlyRevivingPlayer->GetUpdateUIComponent()->StopRevivingMessageUI_Client();
	}
}