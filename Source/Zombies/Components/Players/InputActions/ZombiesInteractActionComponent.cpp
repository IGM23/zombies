#include "ZombiesInteractActionComponent.h"
#include "EnhancedInputComponent.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesInteractionComponent.h"

void UZombiesInteractActionComponent::BindInputAction(UEnhancedInputComponent* enhancedInputComponent)
{
	FEnhancedInputActionEventBinding* inputActionEventBindingPtr = &enhancedInputComponent->BindAction(InputAction,
		ETriggerEvent::Triggered, this, &UZombiesInteractActionComponent::Interact);

	if(inputActionEventBindingPtr)
		InputHandles.AddUnique(inputActionEventBindingPtr->GetHandle());
}

void UZombiesInteractActionComponent::Interact()
{
	if(CheckIsPlayerAlive())
	{
		if(!OwnerPlayer)
			OwnerPlayer = Cast<AZombiesPlayerCharacter>(GetOwner());

		if(OwnerPlayer)
		{
			if(OwnerPlayer->GetInteractionComponent())
				OwnerPlayer->GetInteractionComponent()->InteractWithInteractableActor();
		}
	}
}
