#include "ZombiesAimZoomActionComponent.h"
#include "EnhancedInputComponent.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/Players/ZombiesZoomAimingComponent.h"

void UZombiesAimZoomActionComponent::BindInputAction(UEnhancedInputComponent* enhancedInputComponent)
{
	FEnhancedInputActionEventBinding* inputActionEventBindingPtr = &enhancedInputComponent->BindAction(InputAction,
		ETriggerEvent::Triggered, this, &UZombiesAimZoomActionComponent::DoZoom);
	if(inputActionEventBindingPtr)
		InputHandles.AddUnique(inputActionEventBindingPtr->GetHandle());

	inputActionEventBindingPtr = &enhancedInputComponent->BindAction(InputAction,
			ETriggerEvent::Completed, this, &UZombiesAimZoomActionComponent::StopZoom);
	if(inputActionEventBindingPtr)
		InputHandles.AddUnique(inputActionEventBindingPtr->GetHandle());
}

void UZombiesAimZoomActionComponent::DoZoom()
{
	if(!OwnerPlayer)
		OwnerPlayer = Cast<AZombiesPlayerCharacter>(GetOwner());

	if(OwnerPlayer)
	{
		if(OwnerPlayer->GetZoomAimingComponent())
			OwnerPlayer->GetZoomAimingComponent()->StartZoom();
	}
}

void UZombiesAimZoomActionComponent::StopZoom()
{
	if(!OwnerPlayer)
		OwnerPlayer = Cast<AZombiesPlayerCharacter>(GetOwner());

	if(OwnerPlayer)
	{
		if(OwnerPlayer->GetZoomAimingComponent())
			OwnerPlayer->GetZoomAimingComponent()->StopZoom();
	}
}
