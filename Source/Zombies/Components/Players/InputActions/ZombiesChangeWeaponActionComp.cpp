#include "ZombiesChangeWeaponActionComp.h"
#include "EnhancedInputComponent.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/Players/ZombiesWeaponsManagerCmp.h"

void UZombiesChangeWeaponActionComp::BindInputAction(UEnhancedInputComponent* enhancedInputComponent)
{
	FEnhancedInputActionEventBinding* inputActionEventBindingPtr = &enhancedInputComponent->BindAction(InputAction,
		ETriggerEvent::Triggered, this, &UZombiesChangeWeaponActionComp::ChangeWeapon);

	if(inputActionEventBindingPtr)
		InputHandles.AddUnique(inputActionEventBindingPtr->GetHandle());
}

void UZombiesChangeWeaponActionComp::ChangeWeapon()
{
	if(!OwnerPlayer)
		OwnerPlayer = Cast<AZombiesPlayerCharacter>(GetOwner());
	
	if(OwnerPlayer)
	{
		if(OwnerPlayer->GetWeaponsManagerComponent())
			OwnerPlayer->GetWeaponsManagerComponent()->ChangeWeapon();
	}
}
