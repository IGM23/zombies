#pragma once

#include "CoreMinimal.h"
#include "Zombies/Components/Players/ZombiesInputActionComponent.h"
#include "ZombiesAimZoomActionComponent.generated.h"

class UEnhancedInputComponent;
class AZombiesPlayerCharacter;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesAimZoomActionComponent : public UZombiesInputActionComponent
{
	GENERATED_BODY()

	UPROPERTY()
	AZombiesPlayerCharacter* OwnerPlayer = nullptr;

public:
	virtual void BindInputAction(UEnhancedInputComponent* enhancedInputComponent) override;
	
	UFUNCTION()
	void DoZoom();

	UFUNCTION()
	void StopZoom();
};
