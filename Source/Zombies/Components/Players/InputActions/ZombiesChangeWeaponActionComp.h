#pragma once

#include "CoreMinimal.h"
#include "Zombies/Components/Players/ZombiesInputActionComponent.h"
#include "ZombiesChangeWeaponActionComp.generated.h"

class UEnhancedInputComponent;
class AZombiesPlayerCharacter;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesChangeWeaponActionComp : public UZombiesInputActionComponent
{
	GENERATED_BODY()

	UPROPERTY()
	AZombiesPlayerCharacter* OwnerPlayer = nullptr;

public:
	virtual void BindInputAction(UEnhancedInputComponent* enhancedInputComponent) override;
	
	UFUNCTION()
	void ChangeWeapon();
};
