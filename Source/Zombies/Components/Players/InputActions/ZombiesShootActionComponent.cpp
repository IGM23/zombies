#include "ZombiesShootActionComponent.h"

#include "EnhancedInputComponent.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/Players/ZombiesWeaponsManagerCmp.h"

void UZombiesShootActionComponent::BindInputAction(UEnhancedInputComponent* enhancedInputComponent)
{
	FEnhancedInputActionEventBinding* inputActionEventBindingPtr = &enhancedInputComponent->BindAction(InputAction,
		ETriggerEvent::Triggered, this, &UZombiesShootActionComponent::Shoot);

	if(inputActionEventBindingPtr)
		InputHandles.AddUnique(inputActionEventBindingPtr->GetHandle());
}

void UZombiesShootActionComponent::Shoot()
{
	if(CheckIsPlayerAlive() && !CheckIsRevivingAnotherPlayer())
	{
		if(!OwnerPlayer)
			OwnerPlayer = Cast<AZombiesPlayerCharacter>(GetOwner());
	
		if(OwnerPlayer)
		{
			if(OwnerPlayer->GetWeaponsManagerComponent())
				OwnerPlayer->GetWeaponsManagerComponent()->ShootCurrentWeapon();
		}
	}
}
