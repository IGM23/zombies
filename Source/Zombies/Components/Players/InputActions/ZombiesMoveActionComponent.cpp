#include "ZombiesMoveActionComponent.h"

#include "EnhancedInputComponent.h"
#include "Zombies/Controllers/ZombiesPlayerController.h"

void UZombiesMoveActionComponent::BindInputAction(UEnhancedInputComponent* enhancedInputComponent)
{
	FEnhancedInputActionEventBinding* inputActionEventBindingPtr = &enhancedInputComponent->BindAction(InputAction,
		ETriggerEvent::Triggered, this, &UZombiesMoveActionComponent::Move);
	
	if(inputActionEventBindingPtr)
		InputHandles.AddUnique(inputActionEventBindingPtr->GetHandle());
}

void UZombiesMoveActionComponent::Move(const FInputActionValue& Value)
{
	if(CheckIsPlayerAlive())
	{
		// input is a Vector2D
		FVector2D MovementVector = Value.Get<FVector2D>();

		// find out which way is forward
		const FRotator Rotation = PlayerController->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement 
		PlayerController->GetPawn()->AddMovementInput(ForwardDirection, MovementVector.Y);
		PlayerController->GetPawn()->AddMovementInput(RightDirection, MovementVector.X);
	}
}
