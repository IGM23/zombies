#pragma once

#include "CoreMinimal.h"
#include "Zombies/Components/Players/ZombiesInputActionComponent.h"
#include "ZombiesInteractActionComponent.generated.h"

class AZombiesPlayerCharacter;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesInteractActionComponent : public UZombiesInputActionComponent
{
	GENERATED_BODY()

	UPROPERTY()
	AZombiesPlayerCharacter* OwnerPlayer = nullptr;

public:
	virtual void BindInputAction(UEnhancedInputComponent* enhancedInputComponent) override;
	
	UFUNCTION()
	void Interact();
};
