#pragma once

#include "CoreMinimal.h"
#include "Zombies/Components/Players/ZombiesInputActionComponent.h"
#include "ZombiesJumpActionComponent.generated.h"

class UEnhancedInputComponent;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesJumpActionComponent : public UZombiesInputActionComponent
{
	GENERATED_BODY()

public:	
	UZombiesJumpActionComponent(){}
	
	virtual void BindInputAction(UEnhancedInputComponent* enhancedInputComponent) override;

protected:
	UFUNCTION()
	void Jump();

	UFUNCTION()
	void StopJumping();
};
