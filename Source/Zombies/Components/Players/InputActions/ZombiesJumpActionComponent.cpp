#include "ZombiesJumpActionComponent.h"

#include "EnhancedInputComponent.h"
#include "GameFramework/Character.h"
#include "Zombies/Controllers/ZombiesPlayerController.h"

void UZombiesJumpActionComponent::BindInputAction(UEnhancedInputComponent* enhancedInputComponent)
{
	FEnhancedInputActionEventBinding* inputActionEventBindingPtr = &enhancedInputComponent->BindAction(InputAction,
		ETriggerEvent::Triggered, this, &UZombiesJumpActionComponent::Jump);
	if(inputActionEventBindingPtr)
		InputHandles.AddUnique(inputActionEventBindingPtr->GetHandle());
	
	inputActionEventBindingPtr = &enhancedInputComponent->BindAction(InputAction,
		ETriggerEvent::Completed, this, &UZombiesJumpActionComponent::StopJumping);
	if(inputActionEventBindingPtr)
		InputHandles.AddUnique(inputActionEventBindingPtr->GetHandle());
}

void UZombiesJumpActionComponent::Jump()
{
	if(CheckIsPlayerAlive())
	{
		ACharacter* controlledCharacter = Cast<ACharacter>(PlayerController->GetPawn());
		if(controlledCharacter)
			controlledCharacter->Jump();
	}
}

void UZombiesJumpActionComponent::StopJumping()
{
	if(CheckIsPlayerAlive())
	{
		ACharacter* controlledCharacter = Cast<ACharacter>(PlayerController->GetPawn());
		if(controlledCharacter)
			controlledCharacter->StopJumping();
	}
}
