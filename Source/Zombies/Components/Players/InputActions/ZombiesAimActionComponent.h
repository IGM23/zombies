#pragma once

#include "CoreMinimal.h"
#include "Zombies/Components/Players/ZombiesInputActionComponent.h"
#include "ZombiesAimActionComponent.generated.h"

class AZombiesPlayerCharacter;
class UEnhancedInputComponent;
struct FInputActionValue;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesAimActionComponent : public UZombiesInputActionComponent
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, meta = ( AllowPrivateAccess = "true" ))
	AZombiesPlayerCharacter* OwnerPlayerCharacter = nullptr;

public:
	/** Special case. Aim Input Action when using Controller */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputAction* ControllerAimInputAction = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	float ControllerYawPerSecond = 45.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	float ControllerPitchPerSecond = 25.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	float MouseYawPerSecond = 60.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	float MousePitchPerSecond = 40.f;
	
public:	
	UZombiesAimActionComponent(){}
	
	virtual void BindInputAction(UEnhancedInputComponent* enhancedInputComponent) override;
	
protected:
	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateCrosshairWidget_Blueprint(bool isVisible);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateCharactersUpperBodyPitch(const FRotator& deltaRotator);
	
	UFUNCTION()
	void SetAimMouseValues(const FInputActionValue& Value);

	UFUNCTION()
	void SetAimControllerValues(const FInputActionValue& Value);

private:
	UFUNCTION()
	void UpdateAimingRotation(float yawToAdd, float pitchToAdd);
	
	// We actually want a copy of the Vectors in the parameters, not the original ones.
	UFUNCTION()
	float CalculateAngleBetweenVectors(FVector vec1, FVector vec2) const;
};
