#include "ZombiesReloadWeaponActionComp.h"
#include "EnhancedInputComponent.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/Players/ZombiesWeaponsManagerCmp.h"

void UZombiesReloadWeaponActionComp::BindInputAction(UEnhancedInputComponent* enhancedInputComponent)
{
	FEnhancedInputActionEventBinding* inputActionEventBindingPtr = &enhancedInputComponent->BindAction(InputAction,
		ETriggerEvent::Triggered, this, &UZombiesReloadWeaponActionComp::ReloadWeapon);

	if(inputActionEventBindingPtr)
		InputHandles.AddUnique(inputActionEventBindingPtr->GetHandle());
}

void UZombiesReloadWeaponActionComp::ReloadWeapon()
{
	if(!OwnerPlayer)
		OwnerPlayer = Cast<AZombiesPlayerCharacter>(GetOwner());
	
	if(OwnerPlayer)
	{
		if(OwnerPlayer->GetWeaponsManagerComponent())
			OwnerPlayer->GetWeaponsManagerComponent()->ReloadCurrentWeapon();
	}
}
