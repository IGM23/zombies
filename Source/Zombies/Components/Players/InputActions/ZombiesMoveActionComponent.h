#pragma once

#include "CoreMinimal.h"
#include "Zombies/Components/Players/ZombiesInputActionComponent.h"
#include "ZombiesMoveActionComponent.generated.h"

class UEnhancedInputComponent;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesMoveActionComponent : public UZombiesInputActionComponent
{
	GENERATED_BODY()

public:	
	UZombiesMoveActionComponent(){}
	
	virtual void BindInputAction(UEnhancedInputComponent* enhancedInputComponent) override;

protected:
	UFUNCTION()
	void Move(const FInputActionValue& Value);
};
