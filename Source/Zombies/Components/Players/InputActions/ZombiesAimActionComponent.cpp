#include "ZombiesAimActionComponent.h"

#include "EnhancedInputComponent.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"

void UZombiesAimActionComponent::BeginPlay()
{
	Super::BeginPlay();
	OwnerPlayerCharacter = Cast<AZombiesPlayerCharacter>(GetOwner());
}

void UZombiesAimActionComponent::BindInputAction(UEnhancedInputComponent* enhancedInputComponent)
{
	FEnhancedInputActionEventBinding* inputActionEventBindingPtr = &enhancedInputComponent->BindAction(InputAction,
		ETriggerEvent::Triggered, this, &UZombiesAimActionComponent::SetAimMouseValues);
	if(inputActionEventBindingPtr)
		InputHandles.AddUnique(inputActionEventBindingPtr->GetHandle());

	inputActionEventBindingPtr = &enhancedInputComponent->BindAction(ControllerAimInputAction, ETriggerEvent::Triggered, this,
		&UZombiesAimActionComponent::SetAimControllerValues);
	if(inputActionEventBindingPtr)
		InputHandles.AddUnique(inputActionEventBindingPtr->GetHandle());
}

void UZombiesAimActionComponent::SetAimMouseValues(const FInputActionValue& Value)
{
	// input is a Vector2D.
	const FVector2D lookAxisVector = Value.Get<FVector2D>();
	const float yawToAdd = lookAxisVector.X * MouseYawPerSecond * GetWorld()->GetDeltaSeconds();
	const float pitchToAdd = lookAxisVector.Y * MousePitchPerSecond * GetWorld()->GetDeltaSeconds();

	UpdateAimingRotation(yawToAdd, pitchToAdd);
}

void UZombiesAimActionComponent::SetAimControllerValues(const FInputActionValue& Value)
{
	// input is a Vector2D.
	const FVector2D lookAxisVector = Value.Get<FVector2D>();
	const float yawToAdd = lookAxisVector.X * ControllerYawPerSecond * GetWorld()->GetDeltaSeconds();
	const float pitchToAdd = lookAxisVector.Y * ControllerPitchPerSecond * GetWorld()->GetDeltaSeconds();

	UpdateAimingRotation(yawToAdd, pitchToAdd);
}

void UZombiesAimActionComponent::UpdateAimingRotation(float yawToAdd, float pitchToAdd)
{
	OwnerPlayerCharacter->AddControllerYawInput(yawToAdd);
	OwnerPlayerCharacter->AddControllerPitchInput(pitchToAdd);
	
	//UpdateCharactersUpperBodyPitch(deltaRotation);
}

float UZombiesAimActionComponent::CalculateAngleBetweenVectors(FVector vec1, FVector vec2) const
{
	vec1.Normalize();
	vec2.Normalize();

	const double dotProduct = FVector::DotProduct(vec1, vec2);
	const float angle = FMath::RadiansToDegrees(FMath::Acos(dotProduct));

	return angle;
}
