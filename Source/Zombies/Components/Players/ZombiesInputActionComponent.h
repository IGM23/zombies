#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ZombiesInputActionComponent.generated.h"

/**
 * This component has a unique Input Action (one component to jump, other to shoot, etc.)
 * and it manages the functionality of that Input Action and its setup as well.
*/

class AZombiesPlayerController;
class UInputAction;
struct FEnhancedInputActionEventBinding;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Abstract )
class ZOMBIES_API UZombiesInputActionComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	/** This component's Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputAction* InputAction = nullptr;

protected:
	UPROPERTY(BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	AZombiesPlayerController* PlayerController = nullptr;
	
	UPROPERTY()
	TArray<uint32> InputHandles;
	
public:	
	UZombiesInputActionComponent();
	virtual ~UZombiesInputActionComponent() override;

	UFUNCTION()
	void SetPlayerController(AZombiesPlayerController* pc);

	UFUNCTION()
	virtual void BindInputAction(UEnhancedInputComponent* enhancedInputComponent) PURE_VIRTUAL(UZombiesInputActionComponent::BindInputAction, ;)
	
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void RemoveInputActionBinding();

	UFUNCTION()
	bool CheckIsPlayerAlive() const;

	UFUNCTION()
	bool CheckIsRevivingAnotherPlayer() const;
};
