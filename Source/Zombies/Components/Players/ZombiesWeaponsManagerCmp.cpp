#include "ZombiesWeaponsManagerCmp.h"

#include "ZombiesUpdateUIComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Zombies/Actors/Weapons/ZombiesWeapon.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"
#include "Zombies/Components/ZombiesInteractionComponent.h"
#include "Zombies/Controllers/ZombiesPlayerController.h"

UZombiesWeaponsManagerCmp::UZombiesWeaponsManagerCmp()
{
	PrimaryComponentTick.bCanEverTick = false;
	SetIsReplicatedByDefault(true);
}

void UZombiesWeaponsManagerCmp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	//Testing_PrintLineTraceOfBullet();
}

void UZombiesWeaponsManagerCmp::BeginPlay()
{
	Super::BeginPlay();

	IsReloading = false;
	OwnerPlayerCharacter = Cast<AZombiesPlayerCharacter>(GetOwner());
	if(OwnerPlayerCharacter)
	{
		SpawnInitialWeaponsForReplication();
		
		if(OwnerPlayerCharacter->GetHasFinishedSetup())
			SetupInitialWeaponsAndAmmoUI();
		else
			OwnerPlayerCharacter->OnPlayerFinishedSetupDelegate.AddDynamic(this, &UZombiesWeaponsManagerCmp::SetupInitialWeaponsAndAmmoUI);
	}
}

void UZombiesWeaponsManagerCmp::UpdateCurrentWeaponAmmoUI() const
{
	// Only a local player can have the UpdateUIComponent.
	if(UpdateUIComponent && CurrentWeapon)
	{
		UpdateUIComponent->UpdateAmmoUI(CurrentWeapon->GetWeaponName(), CurrentWeapon->GetCurrentReserveAmmo(),
			CurrentWeapon->GetMaxMagazineAmmo(), CurrentWeapon->GetCurrentMagazineAmmo());
	}
}

void UZombiesWeaponsManagerCmp::ShootCurrentWeapon()
{
	if(CurrentWeapon && !IsReloading && CurrentWeapon->Shoot_CheckAndUseAmmo())
	{
		NextBulletDirection = ObtainCrosshairShootingDirection(CurrentWeapon->GetNextTraceSpawnLocation());
		SpawnShootFX_Internal();
		
		if(GetOwner()->GetLocalRole() == ROLE_Authority)
		{
			CurrentWeapon->Shoot_FromServer(NextBulletDirection);
			Multicast_SpawnShootFX();
		}
		else
			SpawnTraceFromWeapon_Server(NextBulletDirection);
		
		UpdateCurrentWeaponAmmoUI();
	}
}

void UZombiesWeaponsManagerCmp::SpawnTraceFromWeapon_Server_Implementation(FVector bulletDirection) const
{
	if(CurrentWeapon)
	{
		CurrentWeapon->Shoot_FromServer(bulletDirection);
		Multicast_SpawnShootFX();
	}
}

void UZombiesWeaponsManagerCmp::Multicast_SpawnShootFX_Implementation() const
{
	//The Instigator Player already has spawned the FX, so don't do it again.
	if(!OwnerPlayerController || (OwnerPlayerController && !OwnerPlayerController->IsLocalPlayerController()))
		SpawnShootFX_Internal();
}

void UZombiesWeaponsManagerCmp::SpawnShootFX_Internal() const
{
	if(CurrentWeapon && OwnerPlayerCharacter && ShootWeaponMontage)
	{
		CurrentWeapon->SpawnShootFX();
		OwnerPlayerCharacter->PlayAnimMontage(ShootWeaponMontage);
	}
}

float UZombiesWeaponsManagerCmp::GetReloadAnimPlayRate(float weaponFinalReloadTime) const
{
	const float reloadAnimLength = ReloadWeaponMontage->GetPlayLength();
	const float animPlayRate = reloadAnimLength / weaponFinalReloadTime;

	return animPlayRate;
}

void UZombiesWeaponsManagerCmp::ReloadCurrentWeapon()
{
	if(!IsReloading && CurrentWeapon && CurrentWeapon->CheckCanReloadWeapon() && UpdateUIComponent)
	{
		IsReloading = true;
		const float reloadTime = CurrentWeapon->GetReloadTime() * ReloadTimeModifier;
		UpdateUIComponent->StartReloadingUI(reloadTime);

		const float reloadAnimPlayRate = GetReloadAnimPlayRate(reloadTime);
		if(GetOwner()->GetLocalRole() == ROLE_Authority)
			Multicast_ReloadAnimation(reloadAnimPlayRate);
		else
			Server_ReloadAnimation(reloadAnimPlayRate);

		FTimerDelegate reloadStartDelegate;
		reloadStartDelegate.BindUFunction(this, "FinishReload", true);
		GetWorld()->GetTimerManager().SetTimer(ReloadingTimerHandle, reloadStartDelegate, reloadTime, false);
	}
}

void UZombiesWeaponsManagerCmp::Server_ReloadAnimation_Implementation(float reloadAnimPlayRate) const
{
	Multicast_ReloadAnimation(reloadAnimPlayRate);
}

void UZombiesWeaponsManagerCmp::Multicast_ReloadAnimation_Implementation(float reloadAnimPlayRate) const
{
	OwnerPlayerCharacter->PlayAnimMontage(ReloadWeaponMontage, reloadAnimPlayRate);
}

void UZombiesWeaponsManagerCmp::FinishReload(bool successfullyFinished)
{
	if(successfullyFinished)
	{
		CurrentWeapon->ReloadWeapon();
		UpdateCurrentWeaponAmmoUI();
	}
	
	UpdateUIComponent->StopReloadingUI();
	GetWorld()->GetTimerManager().ClearTimer(ReloadingTimerHandle);
	IsReloading = false;
}

void UZombiesWeaponsManagerCmp::ReplenishEquippedWeaponsAmmo()
{
	for(AZombiesWeapon* weapon : WeaponsEquipped)
		weapon->ReplenishAmmo();
	
	UpdateCurrentWeaponAmmoUI();
}

void UZombiesWeaponsManagerCmp::ReplenishEquippedWeaponAmmo(AZombiesWeapon* weaponToReplenish)
{
	for(AZombiesWeapon* weapon : WeaponsEquipped)
	{
		if(weapon == weaponToReplenish)
		{
			weapon->ReplenishAmmo();
			UpdateCurrentWeaponAmmoUI();
			return;
		}
	}
}

void UZombiesWeaponsManagerCmp::ChangeWeapon()
{
	if(WeaponsEquipped.Num() <= 1 || !CanChangeWeapon)
		return;

	if(IsReloading)
		FinishReload(false);

	const int32 oldIndex = WeaponsEquippedIndex;
	++WeaponsEquippedIndex;
	if(WeaponsEquippedIndex >= WeaponsEquipped.Num())
		WeaponsEquippedIndex = 0;

	UpdateWeaponMeshesAndAmmoUI(oldIndex, WeaponsEquippedIndex);
	
	CanChangeWeapon = false;
	GetWorld()->GetTimerManager().SetTimer(ResetCanChangeWeaponTimerHandle, this,
		&UZombiesWeaponsManagerCmp::ResetCanChangeWeapon,ChangeWeaponAgainDelay,false);
}

void UZombiesWeaponsManagerCmp::UpdateWeapon_Server_Implementation(int32 oldWeaponIndex, int32 newWeaponIndex)
{
	UpdateWeapon_Multicast(oldWeaponIndex, newWeaponIndex);
}

void UZombiesWeaponsManagerCmp::UpdateWeapon_Multicast_Implementation(int32 oldWeaponIndex, int32 newWeaponIndex)
{
	//The Instigator Player already has updated the Current Weapon and Meshes, so don't do it again.
	if(!OwnerPlayerController || (OwnerPlayerController && !OwnerPlayerController->IsLocalPlayerController()))
		UpdateCurrentWeaponAndMeshesAndPlayAnim(oldWeaponIndex, newWeaponIndex);
}

void UZombiesWeaponsManagerCmp::UpdateCurrentWeaponAndMeshesAndPlayAnim(int32 oldWeaponIndex, int32 newWeaponIndex)
{
	const float changeWeaponAnimPlayRate = ChangeWeaponMontage->GetPlayLength() / ChangeWeaponAgainDelay;
	OwnerPlayerCharacter->PlayAnimMontage(ChangeWeaponMontage, changeWeaponAnimPlayRate);

	const float changeWeaponMeshesTime = ChangeWeaponMontage->GetPlayLength() / changeWeaponAnimPlayRate * 0.5f;
	FTimerDelegate changeWeaponMeshesDelegate;
	changeWeaponMeshesDelegate.BindUFunction(this, "UpdateCurrentWeaponAndMeshes_Internal",
		oldWeaponIndex, newWeaponIndex);
	
	GetWorld()->GetTimerManager().SetTimer(ChangeWeaponMeshesTimerHandle, changeWeaponMeshesDelegate,
	changeWeaponMeshesTime, false);
}

void UZombiesWeaponsManagerCmp::UpdateCurrentWeaponAndMeshes_Internal(int32 oldWeaponIndex, int32 newWeaponIndex)
{
	GetWorld()->GetTimerManager().ClearTimer(ChangeWeaponMeshesTimerHandle);
	
	AZombiesWeapon* oldWeapon = WeaponsEquipped[oldWeaponIndex];
	CurrentWeapon = WeaponsEquipped[newWeaponIndex];
	
	oldWeapon->SetWeaponHidden(true);
	CurrentWeapon->SetWeaponHidden(false);

	if(OwnerPlayerController && OwnerPlayerController->IsLocalPlayerController())
		UpdateCurrentWeaponAmmoUI();
}

void UZombiesWeaponsManagerCmp::ResetCanChangeWeapon()
{
	GetWorld()->GetTimerManager().ClearTimer(ResetCanChangeWeaponTimerHandle);
	CanChangeWeapon = true;
}

void UZombiesWeaponsManagerCmp::UpdateWeaponMeshesAndAmmoUI(int32 oldWeaponIndex, int32 newWeaponIndex) 
{
	UpdateCurrentWeaponAndMeshesAndPlayAnim(oldWeaponIndex, newWeaponIndex);

	if(GetOwner()->GetLocalRole() == ROLE_Authority)
		UpdateWeapon_Multicast(oldWeaponIndex, newWeaponIndex);
	else
		UpdateWeapon_Server(oldWeaponIndex, newWeaponIndex);
}

void UZombiesWeaponsManagerCmp::PickupWeapon(TSubclassOf<AZombiesWeapon> newWeaponClass)
{
	if(IsReloading)
		FinishReload(false);
	
	const int32 oldIndex = WeaponsEquippedIndex;
	if(WeaponsEquipped.Num() < MaxWeaponCapacity)
		WeaponsEquippedIndex = WeaponsEquipped.Num();

	//First, create and equip the new weapon correctly.
	EquipPickedWeapon(newWeaponClass, WeaponsEquippedIndex);
	if(GetOwner()->GetLocalRole() == ROLE_Authority)
		EquipPickedWeapon_Multicast(newWeaponClass, WeaponsEquippedIndex);
	else
		EquipPickedWeapon_Server(newWeaponClass, WeaponsEquippedIndex);

	//Second, hide/show the weapons correctly and update the screen UI.
	UpdateWeaponMeshesAndAmmoUI(oldIndex, WeaponsEquippedIndex);
}

void UZombiesWeaponsManagerCmp::EquipPickedWeapon_Server_Implementation(TSubclassOf<AZombiesWeapon> weaponClass, int32 weaponIndex)
{
	EquipPickedWeapon_Multicast(weaponClass, weaponIndex);
}

void UZombiesWeaponsManagerCmp::EquipPickedWeapon_Multicast_Implementation(TSubclassOf<AZombiesWeapon> weaponClass, int32 weaponIndex)
{
	//The Instigator Player already has equiped the new weapon, so don't do it again.
	if(!OwnerPlayerController || (OwnerPlayerController && !OwnerPlayerController->IsLocalPlayerController()))
		EquipPickedWeapon(weaponClass, weaponIndex);
}

void UZombiesWeaponsManagerCmp::EquipPickedWeapon(TSubclassOf<AZombiesWeapon> weaponClass, int32 weaponIndex)
{
	AZombiesWeapon* spawnedWeapon = SpawnAndAttachWeapon(weaponClass);
	spawnedWeapon->SetOwner(OwnerPlayerController);
	spawnedWeapon->SetOwnerPlayerController(OwnerPlayerController);
	spawnedWeapon->SetWeaponHidden(true);
	
	if(WeaponsEquipped.Num() < MaxWeaponCapacity)
	{
		WeaponsEquipped.Add(spawnedWeapon);
	}
	else
	{
		WeaponsEquipped[weaponIndex]->Destroy();
		WeaponsEquipped[weaponIndex] = spawnedWeapon;
	}
}

AZombiesWeapon* UZombiesWeaponsManagerCmp::ReturnWeaponEquipped(TSubclassOf<AZombiesWeapon> weaponClassToCheck) const
{
	for(AZombiesWeapon* weapon : WeaponsEquipped)
	{
		if(weapon->GetClass() == weaponClassToCheck)
			return weapon;
	}

	return nullptr;
}

void UZombiesWeaponsManagerCmp::SpawnInitialWeaponsForReplication()
{
	WeaponsEquippedIndex = 0;
	
	for(TSubclassOf<AZombiesWeapon> weaponClass : StartingWeaponsEquippedClasses)
	{
		AZombiesWeapon* spawnedWeapon = SpawnAndAttachWeapon(weaponClass);

		if(spawnedWeapon)
		{
			spawnedWeapon->SetWeaponHidden(true);
			WeaponsEquipped.Add(spawnedWeapon);
		}
	}

	if(WeaponsEquipped.Num() > 0)
	{
		CurrentWeapon = WeaponsEquipped[WeaponsEquippedIndex];
		CurrentWeapon->SetWeaponHidden(false);
	}
}

void UZombiesWeaponsManagerCmp::SetupInitialWeaponsAndAmmoUI()
{
	OwnerPlayerCharacter->OnPlayerFinishedSetupDelegate.RemoveDynamic(this, &UZombiesWeaponsManagerCmp::SetupInitialWeaponsAndAmmoUI);
	
	OwnerPlayerController = Cast<AZombiesPlayerController>(OwnerPlayerCharacter->GetController());
	for(AZombiesWeapon* weapon : WeaponsEquipped)
	{
		weapon->SetOwner(OwnerPlayerController);
		weapon->SetOwnerPlayerController(OwnerPlayerController);
	}

	UpdateUIComponent = OwnerPlayerCharacter->GetUpdateUIComponent();
	UpdateCurrentWeaponAmmoUI();
}

AZombiesWeapon* UZombiesWeaponsManagerCmp::SpawnAndAttachWeapon(TSubclassOf<AZombiesWeapon> weaponClass)
{
	if(OwnerPlayerCharacter)
	{
		AZombiesWeapon* spawnedWeapon = GetWorld()->SpawnActorDeferred<AZombiesWeapon>(weaponClass,
			OwnerPlayerCharacter->GetActorTransform(), OwnerPlayerController, OwnerPlayerCharacter);

		spawnedWeapon->SetOwnerPlayerController(OwnerPlayerController);
		spawnedWeapon->SetOwnerPlayer(OwnerPlayerCharacter);
		spawnedWeapon->SetWeaponsManagerCmp(this);

		UGameplayStatics::FinishSpawningActor(spawnedWeapon, OwnerPlayerCharacter->GetActorTransform());

		if(spawnedWeapon)
		{
			if(spawnedWeapon->GetIsYForward())
				spawnedWeapon->AttachToComponent(OwnerPlayerCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, WeaponAttachmentSocketNameYForward);
			else
				spawnedWeapon->AttachToComponent(OwnerPlayerCharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, WeaponAttachmentSocketNameXForward);
			
			return spawnedWeapon;
		}
	}

	return nullptr;
}

void UZombiesWeaponsManagerCmp::BulletHitHandle(AZombiesCharacter* target, float damage, bool isHeadShoot)
{
	if(GetOwnerRole() == ROLE_Authority)
		BulletHitHandle_Internal_FromServer(target, damage, isHeadShoot);
	else
		BulletHitHandle_Server(target, damage, isHeadShoot);
}

void UZombiesWeaponsManagerCmp::BulletHitHandle_Server_Implementation(AZombiesCharacter* target, float damage, bool isHeadShoot)
{
	BulletHitHandle_Internal_FromServer(target, damage, isHeadShoot);
}

void UZombiesWeaponsManagerCmp::BulletHitHandle_Internal_FromServer(AZombiesCharacter* target, float damage, bool isHeadShoot)
{
	if(target && target->GetCharStatsComponent()->IsCharacterAlive() && OwnerPlayerCharacter)
	{
		OwnerPlayerCharacter->GetInteractionComponent()->DamageTarget_FromServer(target, damage);
		OwnerPlayerCharacter->GetUpdateUIComponent()->ShowHitmarkerAndSFX_Client(HitmarkerDuration, isHeadShoot, HitMarkerSound);
	}
}

void UZombiesWeaponsManagerCmp::Testing_PrintLineTraceOfBullet()
{
	NextBulletDirection = ObtainCrosshairShootingDirection(CurrentWeapon->GetNextTraceSpawnLocation());
	const FVector traceStartingPoint = CurrentWeapon->GetNextTraceSpawnLocation();
	const FVector traceEndingPoint = traceStartingPoint + NextBulletDirection * 10000.f;

	Testing_PrintBulletTrace(traceStartingPoint, traceEndingPoint);
}
