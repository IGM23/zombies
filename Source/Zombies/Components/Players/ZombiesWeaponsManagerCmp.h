#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ZombiesWeaponsManagerCmp.generated.h"

class AZombiesPlayerCharacter;
class AZombiesPlayerController;
class AZombiesWeapon;
class AZombiesCharacter;
class AZombiesBullet;
class UZombiesUpdateUIComponent;
class USoundBase;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesWeaponsManagerCmp : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	AZombiesPlayerCharacter* OwnerPlayerCharacter = nullptr;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	AZombiesPlayerController* OwnerPlayerController = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<AZombiesWeapon>> StartingWeaponsEquippedClasses;

	UPROPERTY(BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	TArray<AZombiesWeapon*> WeaponsEquipped;

	UPROPERTY()
	int32 WeaponsEquippedIndex = 0;

	/** The direction that the next spawned bullet will move towards. Variable stored for replication. */
	UPROPERTY(BlueprintReadOnly, meta=( AllowPrivateAccess = "true" ))
	FVector NextBulletDirection = FVector::ZeroVector;

	/** Socket used for Meshes which have the X as forward Vector. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	FName WeaponAttachmentSocketNameXForward = "";

	/** Socket used for Meshes which have the Y as forward Vector. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	FName WeaponAttachmentSocketNameYForward = "";

	/** How many weapons can the player carry. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	int32 MaxWeaponCapacity = 2;

	UPROPERTY(BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	AZombiesWeapon* CurrentWeapon = nullptr;

	/** Time it takes to switch weapons. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	float ChangeWeaponTime = 2.0f;
	
	/** Small time delay to switch weapons again after doing so. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	float ChangeWeaponAgainDelay = 0.5f;

	/** How many seconds the hitmarker stays on screen after hitting an enemy with a bullet. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	float HitmarkerDuration = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ChangeWeaponMontage = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ShootWeaponMontage = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ReloadWeaponMontage = nullptr;

	/** When the owner of this component (Player) hits a target, that player will hear this sound. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	USoundBase* HitMarkerSound = nullptr;
	
	UPROPERTY()
	bool IsReloading = false;

	/** Affects the final reloading time of the current weapon. */
	UPROPERTY()
	float ReloadTimeModifier = 1.f;
	
	UPROPERTY()
	bool CanChangeWeapon = true;

	UPROPERTY()
	FTimerHandle ResetCanChangeWeaponTimerHandle;

	UPROPERTY()
	FTimerHandle ChangeWeaponMeshesTimerHandle;

	UPROPERTY()
	FTimerHandle ReloadingTimerHandle;
	
	UPROPERTY()
	UZombiesUpdateUIComponent* UpdateUIComponent = nullptr;
	
public:	
	UZombiesWeaponsManagerCmp();
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
	void ShootCurrentWeapon();
	
	UFUNCTION()
	void ReloadCurrentWeapon();
	
	UFUNCTION()
	void ReplenishEquippedWeaponsAmmo();

	UFUNCTION()
	void ReplenishEquippedWeaponAmmo(AZombiesWeapon* weaponToReplenish);

	UFUNCTION()
	void ChangeWeapon();

	/** Returns the weapon equipped of the 'weaponClassToCheck' class. If it is not equipped, returns nullptr. */
	UFUNCTION()
	AZombiesWeapon* ReturnWeaponEquipped(TSubclassOf<AZombiesWeapon> weaponClassToCheck) const;
	
	UFUNCTION()
	void PickupWeapon(TSubclassOf<AZombiesWeapon> newWeaponClass);
	
	UFUNCTION()
	void BulletHitHandle(AZombiesCharacter* target, float damage, bool isHeadShoot);

	UFUNCTION()
	float GetReloadTimeModifier() const { return ReloadTimeModifier; }
	UFUNCTION()
	void SetReloadTimeModifier(float newModifier) { ReloadTimeModifier = newModifier; }
	
	/** Obtains the direction from the Bullet spawn point to the point that the player is aiming to. */
	UFUNCTION(BlueprintImplementableEvent)
	FVector ObtainCrosshairShootingDirection(FVector BulletSpawnPoint);
	
protected:
	virtual void BeginPlay() override;

	UFUNCTION(NetMulticast, Unreliable)
	void Multicast_SpawnShootFX() const;

	UFUNCTION()
	void SpawnShootFX_Internal() const;

	UFUNCTION(Server, Unreliable)
	void Server_ReloadAnimation(float reloadAnimPlayRate) const;
	
	UFUNCTION(NetMulticast, Unreliable)
	void Multicast_ReloadAnimation(float reloadAnimPlayRate) const;

	UFUNCTION()
	float GetReloadAnimPlayRate(float weaponFinalReloadTime) const;
	
	/** Calls a Multicast function for the showing/hiding of the weapons when the player changes them, so all
	 * players are updated correctly about the weapons. */
	UFUNCTION(Server, Reliable)
	void UpdateWeapon_Server(int32 oldWeaponIndex, int32 newWeaponIndex);

	UFUNCTION(NetMulticast, Reliable)
	void UpdateWeapon_Multicast(int32 oldWeaponIndex, int32 newWeaponIndex);

	/** Hides the old weapon and shows the new one. Also plays ChangeWeaponMontage. Used when changing weapons. */
	UFUNCTION()
	void UpdateCurrentWeaponAndMeshesAndPlayAnim(int32 oldWeaponIndex, int32 newWeaponIndex);

	UFUNCTION()
	void UpdateCurrentWeaponAndMeshes_Internal(int32 oldWeaponIndex, int32 newWeaponIndex);
	
	UFUNCTION()
	void ResetCanChangeWeapon();

	/** Spawns the initial weapons for the rest of the remote players in this PC. */
	UFUNCTION()
	void SpawnInitialWeaponsForReplication();

	/** Finishes setting up the spawned weapons and puts their UI in the screen.
	 * This function is only called for the local player of this PC. */
	UFUNCTION()
	void SetupInitialWeaponsAndAmmoUI();

	UFUNCTION(Server, Reliable)
	void EquipPickedWeapon_Server(TSubclassOf<AZombiesWeapon> weaponClass, int32 weaponIndex);
	
	UFUNCTION(NetMulticast, Reliable)
	void EquipPickedWeapon_Multicast(TSubclassOf<AZombiesWeapon> weaponClass, int32 weaponIndex);

	/** Creates a new weapon and adds it or replaces it in the equipment, depending if there is space. */
	void EquipPickedWeapon(TSubclassOf<AZombiesWeapon> weaponClass, int32 weaponIndex);

	UFUNCTION()
	void FinishReload(bool successfullyFinished);
	
	UFUNCTION()
	AZombiesWeapon* SpawnAndAttachWeapon(TSubclassOf<AZombiesWeapon> weaponClass);

	UFUNCTION()
	void UpdateCurrentWeaponAmmoUI() const;

	// Hide/show the weapons correctly and update the screen UI. Used when changing or picking up a weapon.
	UFUNCTION()
	void UpdateWeaponMeshesAndAmmoUI(int32 oldWeaponIndex, int32 newWeaponIndex);

	UFUNCTION(Server, Reliable)
	void SpawnTraceFromWeapon_Server(FVector bulletDirection) const;

	UFUNCTION(Server, Reliable)
	void BulletHitHandle_Server(AZombiesCharacter* target, float damage, bool isHeadShoot);

	UFUNCTION()
	void BulletHitHandle_Internal_FromServer(AZombiesCharacter* target, float damage, bool isHeadShoot);

	// Testing purposes only. Prints constantly the direction of the next bullet being shot. Call it on Tick function.
	UFUNCTION()
	void Testing_PrintLineTraceOfBullet();
	UFUNCTION(BlueprintImplementableEvent)
	void Testing_PrintBulletTrace(FVector startLocation, FVector endLocation);
};
