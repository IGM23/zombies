#include "ZombiesAutoHealComponent.h"

#include "ZombiesUpdateUIComponent.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"
#include "Zombies/GameStates/ZombiesGameState.h"

UZombiesAutoHealComponent::UZombiesAutoHealComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UZombiesAutoHealComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(IsAutoHealing && UpdateUIComponent)
	{
		OwnerPlayerCharacter->GetCharStatsComponent()->AddCurrentHealth(AutoHealingAmountPerSecond * DeltaTime);
		UpdateUIComponent->UpdateHealthUI();
	}
}

void UZombiesAutoHealComponent::BeginPlay()
{
	Super::BeginPlay();

	OwnerPlayerCharacter = Cast<AZombiesPlayerCharacter>(GetOwner());
	if(OwnerPlayerCharacter)
	{
		UpdateUIComponent = OwnerPlayerCharacter->GetUpdateUIComponent();
		AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
		if(!gameState)
			return;
		
		// Delegates are only bound on the Server, and new life values will be replicated.
		if(gameState->CheckIsThisServer())
		{
			OwnerPlayerCharacter->GetCharStatsComponent()->OnReceivedDamageDelegate.AddUObject(this, &UZombiesAutoHealComponent::RestartIsAutoHealing);
			OwnerPlayerCharacter->GetCharStatsComponent()->OnMaxHealedDelegate.AddUObject(this, &UZombiesAutoHealComponent::OnAutoHealingCompleted);
		}
	}
}

void UZombiesAutoHealComponent::RestartIsAutoHealing()
{
	GetWorld()->GetTimerManager().ClearTimer(AutoHealingStartTimerHandle);
	SetIsAutoHealing(false);

	if(OwnerPlayerCharacter->GetCharStatsComponent()->GetCurrentHealth() <= 0.f)
		return;

	FTimerDelegate autoHealingStartDelegate;
	autoHealingStartDelegate.BindUFunction(this, "SetIsAutoHealing", true);
	GetWorld()->GetTimerManager().SetTimer(AutoHealingStartTimerHandle, autoHealingStartDelegate, AutoHealingCooldown, false);
}

void UZombiesAutoHealComponent::OnAutoHealingCompleted()
{
	GetWorld()->GetTimerManager().ClearTimer(AutoHealingStartTimerHandle);
	SetIsAutoHealing(false);
}
