#include "ZombiesZoomAimingComponent.h"

#include "Camera/CameraComponent.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"

UZombiesZoomAimingComponent::UZombiesZoomAimingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UZombiesZoomAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(IsZooming && PlayerCamera->FieldOfView > CameraMaxZoomFOV)
	{
		const float FOVtoAdd = DeltaFOV * ZoomTransitionTime * DeltaTime * -1.f;
		SetCurrentFOV(FOVtoAdd);
	}
	else if(!IsZooming && PlayerCamera->FieldOfView < CameraOriginalFOV)
	{
		const float FOVtoAdd = DeltaFOV * ZoomTransitionTime * DeltaTime;
		SetCurrentFOV(FOVtoAdd);
	}
}

void UZombiesZoomAimingComponent::SetCurrentFOV(const float FOVToAdd) const
{
	const float newFOV = PlayerCamera->FieldOfView + FOVToAdd;
	const float clampedFOV = FMath::Clamp(newFOV, CameraMaxZoomFOV, CameraOriginalFOV);
	PlayerCamera->SetFieldOfView(clampedFOV);
}

void UZombiesZoomAimingComponent::BeginPlay()
{
	Super::BeginPlay();

	OwnerPlayerCharacter = Cast<AZombiesPlayerCharacter>(GetOwner());
	if(OwnerPlayerCharacter)
	{
		PlayerCamera = OwnerPlayerCharacter->GetPlayerCameraComponent();
		CameraOriginalFOV = PlayerCamera->FieldOfView;
		DeltaFOV = CameraOriginalFOV - CameraMaxZoomFOV;
		if(ZoomTransitionTime < 1.f)
			ZoomTransitionTime = 1.f / ZoomTransitionTime;
	}
}

void UZombiesZoomAimingComponent::StartZoom()
{
	IsZooming = true;
}

void UZombiesZoomAimingComponent::StopZoom()
{
	IsZooming = false;
}
