#include "ZombiesInputActionComponent.h"

#include "EnhancedInputComponent.h"
#include "ZombiesPlayerDeathComponent.h"
#include "Zombies/Characters/ZombiesCharacter.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"
#include "Zombies/Controllers/ZombiesPlayerController.h"

UZombiesInputActionComponent::UZombiesInputActionComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

UZombiesInputActionComponent::~UZombiesInputActionComponent()
{
	UZombiesInputActionComponent::RemoveInputActionBinding();
}

void UZombiesInputActionComponent::SetPlayerController(AZombiesPlayerController* pc)
{
	PlayerController = pc;
}

void UZombiesInputActionComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UZombiesInputActionComponent::RemoveInputActionBinding()
{
	if(PlayerController->IsValidLowLevel() && PlayerController->GetEnhancedInputComponent())
	{
		for(uint32 currentHandle : InputHandles)
			PlayerController->GetEnhancedInputComponent()->RemoveActionEventBinding(currentHandle);
	}
}

bool UZombiesInputActionComponent::CheckIsPlayerAlive() const
{
	if(PlayerController)
	{
		AZombiesCharacter* ownerCharacter = Cast<AZombiesCharacter>(PlayerController->GetPawn());
		if(ownerCharacter)
			return ownerCharacter->GetCharStatsComponent()->IsCharacterAlive();
	}

	return false;
}

bool UZombiesInputActionComponent::CheckIsRevivingAnotherPlayer() const
{
	if(PlayerController)
	{
		AZombiesPlayerCharacter* playerCharacter = Cast<AZombiesPlayerCharacter>(PlayerController->GetPawn());
		if(playerCharacter)
			return playerCharacter->GetPlayerDeathComponent()->IsRevivingAnotherPlayer();
	}

	return false;
}
