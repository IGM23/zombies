#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ZombiesAutoHealComponent.generated.h"

class AZombiesPlayerCharacter;
class UZombiesUpdateUIComponent;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesAutoHealComponent : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY()
	AZombiesPlayerCharacter* OwnerPlayerCharacter = nullptr;

	UPROPERTY()
	UZombiesUpdateUIComponent* UpdateUIComponent = nullptr;
	
	UPROPERTY()
	FTimerHandle AutoHealingStartTimerHandle;

	UPROPERTY()
	bool IsAutoHealing = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AutoHeal, meta = (AllowPrivateAccess = "true"))
	float AutoHealingCooldown = 4.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AutoHeal, meta = (AllowPrivateAccess = "true"))
	float AutoHealingAmountPerSecond = 10.f;

public:	
	UZombiesAutoHealComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	virtual void BeginPlay() override;

private:
	/** Everytime a Player receives a damage, after 'AutoHealingCooldown' seconds it starts the auto-healing.
	 * Everytime it receives a hit, 'AutoHealingCooldown' resets too. */
	UFUNCTION()
	void RestartIsAutoHealing();

	UFUNCTION()
	void OnAutoHealingCompleted();

	UFUNCTION()
	void SetIsAutoHealing(bool isHealing) { IsAutoHealing = isHealing; }
};
