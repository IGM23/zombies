#include "ZombiesPerksComponent.h"

#include "ZombiesUpdateUIComponent.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Objects/ZombiesPerkObject.h"

UZombiesPerksComponent::UZombiesPerksComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UZombiesPerksComponent::BeginPlay()
{
	Super::BeginPlay();

	OwnerPlayerCharacter = Cast<AZombiesPlayerCharacter>(GetOwner());
}

void UZombiesPerksComponent::AddAndActivatePerk(UZombiesPerkObject* perkToActivate)
{
	PerksEquipped.Add(perkToActivate);
	OwnerPlayerCharacter->GetUpdateUIComponent()->AddPerkIconToHUD(perkToActivate->GetPerkIcon());
	perkToActivate->ApplyPerkToPlayer(OwnerPlayerCharacter);
}

void UZombiesPerksComponent::RemoveAndDeactivatePerk(UZombiesPerkObject* perkToDeactivate)
{
	perkToDeactivate->RemovePerkFromPlayer(OwnerPlayerCharacter);
	OwnerPlayerCharacter->GetUpdateUIComponent()->RemovePerkIconFromHUD(perkToDeactivate->GetPerkIcon());
	PerksEquipped.Remove(perkToDeactivate);
}

void UZombiesPerksComponent::RemoveAndDeactivateAllPerks_FromServer()
{
	RemoveAndDeactivateAllPerks_Client();
}

void UZombiesPerksComponent::RemoveAndDeactivateAllPerks_Client_Implementation()
{
	for(UZombiesPerkObject* currentPerk : PerksEquipped)
	{
		currentPerk->RemovePerkFromPlayer(OwnerPlayerCharacter);
		OwnerPlayerCharacter->GetUpdateUIComponent()->RemovePerkIconFromHUD(currentPerk->GetPerkIcon());
	}
	PerksEquipped.Empty();
}
