#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ZombiesPerksComponent.generated.h"

class AZombiesPlayerCharacter;
class UZombiesPerkObject;

/** This component stores the perks that the player buys and apply their effects. Also, when the player is downed
 * or dead, this component will remove the perks and their effects. */

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesPerksComponent : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY()
	AZombiesPlayerCharacter* OwnerPlayerCharacter = nullptr;

	/** How many perks at the same time can have the player. */
	UPROPERTY()
	int32 MaxPerks = 4;
	
	UPROPERTY()
	TArray<UZombiesPerkObject*> PerksEquipped;

public:	
	UZombiesPerksComponent();

	UFUNCTION()
	void AddAndActivatePerk(UZombiesPerkObject* perkToActivate);
	UFUNCTION()
	void RemoveAndDeactivatePerk(UZombiesPerkObject* perkToDeactivate);
	UFUNCTION()
	void RemoveAndDeactivateAllPerks_FromServer();
	
	UFUNCTION()
	int32 GetMaxPerks() const { return MaxPerks; }
	UFUNCTION()
	int32 GetNumPerksEquipped() const { return PerksEquipped.Num(); }
	UFUNCTION()
	bool HasPerkEquipped(UZombiesPerkObject* perkToCheck) const { return PerksEquipped.Contains(perkToCheck); }
	UFUNCTION()
	bool HasMaxPerksEquipped() const { return PerksEquipped.Num() >= MaxPerks; }

protected:
	virtual void BeginPlay() override;

	UFUNCTION(Client, Reliable)
	void RemoveAndDeactivateAllPerks_Client();
};
