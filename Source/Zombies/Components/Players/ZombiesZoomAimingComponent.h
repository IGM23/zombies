#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ZombiesZoomAimingComponent.generated.h"

class AZombiesPlayerCharacter;
class UCameraComponent;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesZoomAimingComponent : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY()
	AZombiesPlayerCharacter* OwnerPlayerCharacter = nullptr;
	UPROPERTY()
	UCameraComponent* PlayerCamera = nullptr;

	UPROPERTY()
	bool IsZooming = false;

	UPROPERTY()
	float CameraOriginalFOV = 90.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ZoomCustomization", meta = (AllowPrivateAccess = "true"))
	float CameraMaxZoomFOV = 60.f;

	UPROPERTY()
	float DeltaFOV = 0.f;
	
	/** How many seconds it takes to go from normal to Zoom and viceversa. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ZoomCustomization", meta = (AllowPrivateAccess = "true"))
	float ZoomTransitionTime = 0.25f;

public:	
	UZombiesZoomAimingComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
	void StartZoom();
	
	UFUNCTION()
	void StopZoom();
	
protected:
	virtual void BeginPlay() override;

private:
	void SetCurrentFOV(const float FOVToAdd) const;
};
