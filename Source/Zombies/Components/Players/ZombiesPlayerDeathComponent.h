#pragma once

#include "CoreMinimal.h"
#include "Zombies/Components/ZombiesDeathComponent.h"
#include "ZombiesPlayerDeathComponent.generated.h"

class AZombiesPlayerCharacter;

/**
 * As a reminder, this component executes on Server.
 * A Downed Player can be revived being inside of the RevivalArea of an Alive Player for some seconds.
 * Players can not shoot while reviving.
 */

UCLASS(Abstract, Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesPlayerDeathComponent : public UZombiesDeathComponent
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	AZombiesPlayerCharacter* OwnerPlayerCharacter = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float DefaultReviveTime = 6.f;

	UPROPERTY()
	float CurrentReviveTime = 1.f;
	
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	AZombiesPlayerCharacter* CurrentlyRevivingPlayer = nullptr;

	UPROPERTY()
	FTimerHandle ReviveTimerHandle;

public:
	UZombiesPlayerDeathComponent() {}

	UFUNCTION(BlueprintCallable)
	void OnReviveBarCompleted();
	
	virtual void OnCharacterLifeReachesZero() override;
	
	/** Passes from DOWN state to ALIVE state again. */
	UFUNCTION(BlueprintCallable)
	void OnPlayerRevived();

	UFUNCTION(BlueprintCallable)
	bool IsRevivingAnotherPlayer() const;

	UFUNCTION()
	float GetDefaultReviveTime() const { return DefaultReviveTime; };
	
	UFUNCTION()
	void SetAndSynchroNewReviveTime(float newTime);

protected:
	virtual void BeginPlay() override;

	UFUNCTION(NetMulticast, Reliable)
	void ReplicateDownState_Multicast(bool isDowned);

	UFUNCTION()
	virtual void OnPlayerEntersRevivalArea(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
		UPrimitiveComponent* otherComponent, int32 otherBodyIndex, bool bFromSweep, const FHitResult& hit);
	
	UFUNCTION()
	virtual void OnPlayerLeavesRevivalArea(UPrimitiveComponent* overlappedComp, AActor* otherActor,
	UPrimitiveComponent* otherComp, int32 otherBodyIndex);

	UFUNCTION()
	void StopReviving();

	UFUNCTION(Client, Reliable)
	void StartReviveProcess_Client(bool isStarting, AZombiesPlayerCharacter* revivingPlayer);

	UFUNCTION(Server, Reliable)
	void SetNewReviveTimeOnServer(float newTime);
	UFUNCTION()
	void SetNewReviveTime(float newTime) { CurrentReviveTime = newTime; }

	/** bShowMessage == true shows the message, otherwise hides it. */
	UFUNCTION()
	void UpdateBeingRevivedMessage(bool bShowMessage);
};
