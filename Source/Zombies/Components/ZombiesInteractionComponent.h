#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ZombiesInteractionComponent.generated.h"

/**
 * This component manages interactions between the owner of this component and another Actor.
 * For example, when this Actor hits another one a health decrease happens, and if it is a Player, it also gains points.
 * It is also used to interact with the InteractableActors of the map.
*/

class AZombiesInteractableActor;
class AZombiesCharacter;
class AZombiesPlayerCharacter;
class USoundBase;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIES_API UZombiesInteractionComponent : public UActorComponent
{
	GENERATED_BODY()

	/** Any character has this component, but with this variable we can know if the owner is a Player or not. */
	UPROPERTY()
	AZombiesPlayerCharacter* OwnerPlayerCharacter = nullptr;
	
	UPROPERTY()
	AZombiesInteractableActor* InteractableActorAvailable = nullptr;

	/** When the owner of this component hits a Player, that player will hear this sound. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USoundBase* DamageDealtToPlayerSound = nullptr;

public:	
	UZombiesInteractionComponent();

	/** 2 Options. A Player hit an Enemy or an Enemy hit a Player. */
	UFUNCTION()
	void DamageTarget_FromServer(AZombiesCharacter* targetToDamage, float damageAmount) const;

	UFUNCTION()
	void InteractWithInteractableActor() const;

	/** Some Interactable Actors need to replicate their Interaction, and when they have to,
	 * they call back this function from their "Interact" function.
	 * If isMulticast == true, we use a Multicast RPC. If not, we execute the functionality only on the Server. */
	UFUNCTION()
	void ReplicateInteractableActorInteraction(bool isMulticast);
	
	UFUNCTION()
	void AddPointsAndUpdateUI(float pointsToAdd) const;
	UFUNCTION(Server, Reliable)
	void AddPointsAndUpdateUI_Server(float pointsToAdd) const;
	
	UFUNCTION()
	AZombiesInteractableActor* GetInteractableActorAvailable() const { return InteractableActorAvailable; }
	UFUNCTION()
	void SetInteractableActorAvailable(AZombiesInteractableActor* interactable);

protected:
	virtual void BeginPlay() override;
	
private:
	UFUNCTION()
	void AddPointsAndUpdateUI_Internal(float pointsToAdd) const;
	
	/** Only happens when an Enemy hits a Player or a player receive damage. */
	UFUNCTION()
	void UpdateTargetHealthUIAndDamageScreenAndSound(AZombiesCharacter* characterToUpdate) const;
	
	UFUNCTION()
	bool CheckIfPlayerHasHitAnEnemy(AZombiesCharacter* targetToCheck) const;

	UFUNCTION(Server, Reliable)
	void ReplicateInteractableActorInteraction_Server(bool isMulticast, AZombiesInteractableActor* interactableActor);
	UFUNCTION(NetMulticast, Reliable)
	void ReplicateInteractableActorInteraction_Multicast(AZombiesInteractableActor* interactableActor);
	UFUNCTION()
	void ReplicateInteractableActorInteraction_Internal(AZombiesInteractableActor* interactableActor);
	
};
 