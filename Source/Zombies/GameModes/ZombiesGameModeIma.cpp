#include "ZombiesGameModeIma.h"
#include "Zombies/Controllers/ZombiesPlayerController.h"

AZombiesGameModeIma::AZombiesGameModeIma()
{
	PlayerControllerClass = AZombiesPlayerController::StaticClass();

	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Characters/Players/BP_PlayerBase"));
	if (PlayerPawnBPClass.Class != nullptr)
		DefaultPawnClass = PlayerPawnBPClass.Class;
	
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Controllers/BP_ZombiesPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
		PlayerControllerClass = PlayerControllerBPClass.Class;
}

void AZombiesGameModeIma::StartPlay()
{
	Super::StartPlay();
}

