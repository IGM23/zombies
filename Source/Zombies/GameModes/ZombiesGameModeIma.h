#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "ZombiesGameModeIma.generated.h"

UCLASS()
class ZOMBIES_API AZombiesGameModeIma : public AGameMode
{
	GENERATED_BODY()

public:
	AZombiesGameModeIma();

protected:
	virtual void StartPlay() override;
	
};
