#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "Z_EndAttackBeforeNotify.generated.h"

/**
 * Used to end the attacking sate before. Normally this is done in the AttacksComponent when the animation finishes
 * playing, but it can be done here if we want the attack to be finished some frames before and not wait until the end.
 */

UCLASS()
class ZOMBIES_API UZ_EndAttackBeforeNotify : public UAnimNotify
{
	GENERATED_BODY()

public:
	UZ_EndAttackBeforeNotify() {}

private:
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference) override;
};
