#include "Z_AttackHitboxNotifyState.h"

#include "Zombies/Characters/ZombiesCharacter.h"
#include "Zombies/Characters/ZombiesEnemyCharacter.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Components/ZombiesAttacksComponent.h"
#include "Zombies/Controllers/ZombiesEnemyBaseController.h"
#include "Zombies/GameStates/ZombiesGameState.h"

void UZ_AttackHitboxNotifyState::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
	float FrameDeltaTime, const FAnimNotifyEventReference& EventReference)
{
    // Check that we are on the Server
    if(MeshComp->GetOwner()->GetNetMode() == ENetMode::NM_Client)
        return;
    
    // 1- Create the attack Hitbox shape.
    FCollisionShape collisionShape;
    const FVector socketLocation = MeshComp->GetSocketTransform(SocketName).GetLocation();
    const FQuat socketRotation = MeshComp->GetSocketTransform(SocketName).GetRotation();
    CreateCollisionShape(collisionShape, socketLocation, socketRotation, MeshComp);

    AZombiesCharacter* instigatorCharacter = Cast<AZombiesCharacter>(MeshComp->GetOwner());

    if (instigatorCharacter->IsValidLowLevel())
    {
        // 2- If there has been a Hit, apply damage to all affected Characters.
        TArray<FHitResult> outHits;
        FCollisionQueryParams TraceParams(FName(TEXT("InteractTrace")), true, NULL);
        TraceParams.AddIgnoredActor(instigatorCharacter); //Ignore himself
        const bool hit = MeshComp->GetWorld()->SweepMultiByChannel(outHits,socketLocation,socketLocation, socketRotation,
            ECC_Pawn, collisionShape, TraceParams);
        
        if (hit)
        {
            TArray<AZombiesCharacter*> alreadyHitCharacters;
            for (FHitResult& hitResult : outHits)
            {
                AZombiesCharacter* hitReceivedCharacter = Cast<AZombiesCharacter>(hitResult.GetActor());

                // CreateCollisionShape creates the collision in the client as well, so we only want to take the character from the Server.
                if (hitReceivedCharacter && hitReceivedCharacter->IsValidLowLevel() &&
                    hitReceivedCharacter->GetNetMode() != ENetMode::NM_Client && !alreadyHitCharacters.Contains(hitReceivedCharacter))
                {
                    if((CharactersAffected == ECharactersAffected::Players && Cast<AZombiesPlayerCharacter>(hitResult.GetActor())) ||
                        (CharactersAffected == ECharactersAffected::Enemies && Cast<AZombiesEnemyCharacter>(hitResult.GetActor())) ||
                        (CharactersAffected == ECharactersAffected::All && Cast<AZombiesCharacter>(hitResult.GetActor())))
                    {
                        instigatorCharacter->GetAttacksComponent()->OnAttackLanded(hitReceivedCharacter);
                        alreadyHitCharacters.Add(hitReceivedCharacter);
                    }
                }
            }
        }
    }
}

void UZ_AttackHitboxNotifyState::CreateCollisionShape(FCollisionShape& collisionShape, const FVector& socketLocation, const FQuat& socketRotation, const USkeletalMeshComponent* meshComp) const
{
    if (CurrentHitboxShape == EHitboxShape::Sphere)
    {
        collisionShape = FCollisionShape::MakeSphere(SphereRadius);

        if (PrintDebug)
            DrawDebugSphere(meshComp->GetWorld(), socketLocation, SphereRadius, 32, FColor::Green, false, -1, 0, CurrentHitboxLineThickness);
    }
    else if (CurrentHitboxShape == EHitboxShape::Box)
    {
        FVector BoxDimensions(BoxWidth * 0.5f, BoxHeight * 0.5f, BoxDepth * 0.5f);
        collisionShape = FCollisionShape::MakeBox(BoxDimensions);

        if (PrintDebug)
            DrawDebugBox(meshComp->GetWorld(), socketLocation, BoxDimensions, socketRotation, FColor::Blue, false, -1, 0, CurrentHitboxLineThickness);
    }
    else if (CurrentHitboxShape == EHitboxShape::Capsule)
    {
        collisionShape = FCollisionShape::MakeCapsule(CapsuleRadius, CapsuleHeight * 0.5f);

        if (PrintDebug)
            DrawDebugCapsule(meshComp->GetWorld(), socketLocation, CapsuleHeight * 0.5f, CapsuleRadius, socketRotation, FColor::Green, false, -1, 0, CurrentHitboxLineThickness);
    }
}