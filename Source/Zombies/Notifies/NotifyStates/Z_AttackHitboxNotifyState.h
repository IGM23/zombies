#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "Z_AttackHitboxNotifyState.generated.h"

class AZombiesCharacter;

UENUM(BlueprintType)
enum class EHitboxShape : uint8
{
	Box UMETA(DisplayName = "Box"),
	Sphere UMETA(DisplayName = "Sphere"),
	Capsule UMETA(DisplayName = "Capsule")
};

UENUM(BlueprintType)
enum class ECharactersAffected : uint8
{
	All UMETA(DisplayName = "All"),
	Players UMETA(DisplayName = "Players"),
	Enemies UMETA(DisplayName = "Enemies")
};

UCLASS()
class ZOMBIES_API UZ_AttackHitboxNotifyState : public UAnimNotifyState
{
	GENERATED_BODY()

protected:
	/** Who is affected by this attack. */
	UPROPERTY(EditAnywhere, Category = "Hitbox", meta = (DisplayName = "Characters Affected"))
	ECharactersAffected CharactersAffected = ECharactersAffected::Players;
	
	UPROPERTY(EditAnywhere, Category = "Hitbox Shape", meta = (DisplayName = "Hitbox shape"))
	EHitboxShape CurrentHitboxShape = EHitboxShape::Box;

	UPROPERTY(EditAnywhere, Category = "Hitbox Shape", meta = (DisplayName = "Hitbox thickness"))
	float CurrentHitboxLineThickness = 0.1f;

	UPROPERTY(EditAnywhere, Category = "Hitbox Box Shape", meta = (DisplayName = "Box Width", EditCondition = "CurrentHitboxShape == EHitboxShape::Box"))
	float BoxWidth = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Hitbox Box Shape", meta = (DisplayName = "Box Height", EditCondition = "CurrentHitboxShape == EHitboxShape::Box"))
	float BoxHeight = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Hitbox Box Shape", meta = (DisplayName = "Box Depth", EditCondition = "CurrentHitboxShape == EHitboxShape::Box"))
	float BoxDepth = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Hitbox Sphere Shape", meta = (DisplayName = "Sphere Radius", EditCondition = "CurrentHitboxShape == EHitboxShape::Sphere"))
	float SphereRadius = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Hitbox Capsule Shape", meta = (DisplayName = "Capsule Height", EditCondition = "CurrentHitboxShape == EHitboxShape::Capsule"))
	float CapsuleHeight = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Hitbox Capsule Shape", meta = (DisplayName = "Capsule Radius", EditCondition = "CurrentHitboxShape == EHitboxShape::Capsule"))
	float CapsuleRadius = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Hitbox", meta = (DisplayName = "Socket Name"))
	FName SocketName = "";

	UPROPERTY(EditAnywhere, Category = "Hitbox", meta = (DisplayName = "Print debug"))
	bool PrintDebug = false;

public:
	UZ_AttackHitboxNotifyState() {}

private:
	virtual void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration, const FAnimNotifyEventReference& EventReference) override {}
	virtual void NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime, const FAnimNotifyEventReference& EventReference) override;
	virtual void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference) override {}
	
	void CreateCollisionShape(FCollisionShape& collisionShape, const FVector& socketLocation, const FQuat& socketRotation, const USkeletalMeshComponent* meshComp) const;
};
