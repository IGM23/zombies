#include "Z_EndAttackBeforeNotify.h"

#include "Zombies/Characters/ZombiesCharacter.h"
#include "Zombies/Components/ZombiesAttacksComponent.h"

void UZ_EndAttackBeforeNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
	const FAnimNotifyEventReference& EventReference)
{
	AZombiesCharacter* instigatorCharacter = Cast<AZombiesCharacter>(MeshComp->GetOwner());
	
	if(instigatorCharacter && instigatorCharacter->GetNetMode() != ENetMode::NM_Client)
		instigatorCharacter->GetAttacksComponent()->EndAttack();
}
