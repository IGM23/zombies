#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "Z_PlayLocal2dSoundNotify.generated.h"

class USoundBase;

UCLASS()
class ZOMBIES_API UZ_PlayLocal2dSoundNotify : public UAnimNotify
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, Category = "SFX")
	USoundBase* SoundToPlay = nullptr;

	UPROPERTY(EditAnywhere, Category = "SFX")
	float SoundMultiplier = 1.f;
	
public:
	UZ_PlayLocal2dSoundNotify() {}

private:
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference) override;
	
};
