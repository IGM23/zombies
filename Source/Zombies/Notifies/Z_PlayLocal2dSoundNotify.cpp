#include "Z_PlayLocal2dSoundNotify.h"

#include "Kismet/GameplayStatics.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/Controllers/ZombiesPlayerController.h"

void UZ_PlayLocal2dSoundNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
                                       const FAnimNotifyEventReference& EventReference)
{
	if(!SoundToPlay)
		return;
	
	const AZombiesPlayerCharacter* playerCharacter =  Cast<AZombiesPlayerCharacter>(MeshComp->GetOwner());
	if(!playerCharacter)
		return;
	
	const AZombiesPlayerController* playerController = Cast<AZombiesPlayerController>(playerCharacter->GetController());
	if(!playerController || !playerController->IsLocalPlayerController())
		return;
	
	UGameplayStatics::PlaySound2D(playerCharacter->GetWorld(), SoundToPlay, SoundMultiplier);
}
