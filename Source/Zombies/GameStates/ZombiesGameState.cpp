#include "ZombiesGameState.h"

#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Zombies/Characters/ZombiesCharacter.h"
#include "Zombies/Controllers/ZombiesPlayerController.h"
#include "Zombies/Actors/ZombiesSpawner.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/DataAssets/DA_RoundsManager.h"
#include "Zombies/Managers/ZombiesRoundsManager.h"

void AZombiesGameState::AddDownedPlayerAndCheckEndGame_FromServer(AZombiesPlayerCharacter* playerDowned)
{
	if(PlayersDowned.Contains(playerDowned))
		return;

	PlayersDowned.AddUnique(playerDowned);
	if(PlayersDowned.Num() == GetNumberOfPlayers())
	{
		UZombiesRoundsManager* roundsManager = GetWorld()->GetSubsystem<UZombiesRoundsManager>();
		if(roundsManager)
		{
			roundsManager->EndAllAliveZombiesBehavior();
			ReplicateImportant2dSFX_Multicast(roundsManager->GetGameEndSound(), roundsManager->GetRoundsSFXMultiplier());
		}
		EndGame();
	}
}

void AZombiesGameState::RemoveDownedPlayer_FromServer(AZombiesPlayerCharacter* playerRevived)
{
	if(!PlayersDowned.Contains(playerRevived))
		return;

	PlayersDowned.Remove(playerRevived);
}

TArray<AZombiesPlayerController*> AZombiesGameState::GetPlayerControllers()
{
	TArray<AZombiesPlayerController*> playerControllers;
	for (TObjectPtr<APlayerState> playerStatePtr : PlayerArray)
	{
		AZombiesPlayerController* playerController = Cast<AZombiesPlayerController>(playerStatePtr->GetPlayerController());
		if(playerController)
			playerControllers.Add(playerController);
	}

	return playerControllers;
}

TArray<AZombiesPlayerCharacter*> AZombiesGameState::GetPlayerCharacters()
{
	TArray<AZombiesPlayerCharacter*> playerCharacters;
	for (TObjectPtr<APlayerState> playerStatePtr : PlayerArray)
	{
		AZombiesPlayerCharacter* playerCharacter = Cast<AZombiesPlayerCharacter>(playerStatePtr->GetPawn());
		if(playerCharacter)
			playerCharacters.Add(playerCharacter);
	}

	return playerCharacters;
}

TArray<APawn*> AZombiesGameState::GetPlayerPawns()
{
	TArray<APawn*> playerPawns;
	for (TObjectPtr<APlayerState> playerStatePtr : PlayerArray)
		playerPawns.Add(playerStatePtr->GetPawn());

	return playerPawns;
}

int32 AZombiesGameState::GetNumberOfPlayers() const
{
	return PlayerArray.Num();
}

void AZombiesGameState::AddSpawner(AZombiesSpawner* spawner)
{
	Spawners.AddUnique(spawner);
}

void AZombiesGameState::BroadCastSpawnersReady()
{
	AllSpawnersReady = true;
	OnOnAllSpawnersAddedDelegate.Broadcast();
}

void AZombiesGameState::SpawnPlayer_FromServer(AZombiesPlayerController* playerController)
{
	UClass* classToSpawn = nullptr;
	if(UClass** pointerToClassFound = PlayerClassesMap.Find(playerController))
		classToSpawn = *pointerToClassFound;

	if(!classToSpawn && PlayersToSpawnClasses.Num() > 0)
	{
		const int32 randomClassIndex = FMath::RandRange(0, PlayersToSpawnClasses.Num() - 1);
		classToSpawn = PlayersToSpawnClasses[randomClassIndex].Get();
		PlayerClassesMap.Add(playerController, classToSpawn);

		PlayersToSpawnClasses.RemoveAt(randomClassIndex);
	}

	if(classToSpawn)
	{
		const int32 randomSpawnerIndex = GetValidSpawnerIndex_FromServer();
		AZombiesCharacter* spawnedPlayer = Spawners[randomSpawnerIndex]->SpawnCharacter_FromServer(classToSpawn);

		if(playerController && spawnedPlayer)
		{
			playerController->Possess(spawnedPlayer);
			spawnedPlayer->SetOwner(playerController);
		}
	}
}

void AZombiesGameState::UpdateAllHUD_FromServer()
{
	TArray<FString> namesArray;
	TArray<float> scoresArray;
	
	for (TObjectPtr<APlayerState> playerStatePtr : PlayerArray)
	{
		namesArray.Add(playerStatePtr->GetHumanReadableName());
		scoresArray.Add(playerStatePtr->GetScore());
	}
	
	for (TObjectPtr<APlayerState> playerStatePtr : PlayerArray)
	{
		AZombiesPlayerController* playerController = Cast<AZombiesPlayerController>(playerStatePtr->GetPlayerController());
		if(playerController)
			playerController->UpdatePlayersHUD_Multicast(namesArray, scoresArray);
	}
}

void AZombiesGameState::UpdatePlayerScoreHUD_FromServer(FString& playerName, const float newScore)
{
	for (TObjectPtr<APlayerState> playerStatePtr : PlayerArray)
	{
		AZombiesPlayerController* playerController = Cast<AZombiesPlayerController>(playerStatePtr->GetPlayerController());
		if(playerController)
			playerController->UpdatePlayerPointsHUD_Multicast(playerName, newScore);
	}
}

void AZombiesGameState::UpdatePlayerLifeHUD_FromServer(FString& playerName, const float newLifePercentage)
{
	for (TObjectPtr<APlayerState> playerStatePtr : PlayerArray)
	{
		AZombiesPlayerController* playerController = Cast<AZombiesPlayerController>(playerStatePtr->GetPlayerController());
		if(playerController)
			playerController->UpdatePlayerLifeHUD_Multicast(playerName, newLifePercentage);
	}
}

void AZombiesGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AZombiesGameState, AllSpawnersReady);
}

int32 AZombiesGameState::GetValidSpawnerIndex_FromServer()
{
	int32 randomSpawnerIndex = FMath::RandRange(0, Spawners.Num() - 1);
	
	for(int32 tries = 0; tries < Spawners.Num(); ++tries)
	{
		if(!Spawners[randomSpawnerIndex]->IsPlayerNearby_FromServer())
			return randomSpawnerIndex;

		++randomSpawnerIndex;
		if(randomSpawnerIndex == Spawners.Num())
			randomSpawnerIndex = 0;
	}

	// If all spawners are occupied, spawn anyways in the first one.
	return 0;
}

void AZombiesGameState::StartRoundsManager()
{
	InitializeRoundsManager();
	UZombiesRoundsManager* roundsManager = GetWorld()->GetSubsystem<UZombiesRoundsManager>();
	if(roundsManager)
		roundsManager->StartRound();
}

void AZombiesGameState::InitializeRoundsManager()
{
	UZombiesRoundsManager* roundsManager = GetWorld()->GetSubsystem<UZombiesRoundsManager>();
	if(roundsManager && RoundsManagerDataAsset)
	{
		RoundsManagerDA = RoundsManagerDataAsset.GetDefaultObject();
		if(RoundsManagerDA)
			roundsManager->InitializeFromDataAsset(RoundsManagerDA);
	}
}

bool AZombiesGameState::CheckIsThisServer() const
{
	return HasAuthority();
}

void AZombiesGameState::ReplicateImportant2dSFX_Multicast_Implementation(USoundBase* sfx, float multiplier) const
{
	if(sfx)
		UGameplayStatics::PlaySound2D(GetWorld(), sfx, multiplier);
}

void AZombiesGameState::ReplicateNotImportant3dSFX_Multicast_Implementation(USoundBase* sfx, AActor* actorToAttach) const
{
	if(!sfx || !actorToAttach)
		return;

	UGameplayStatics::SpawnSoundAttached(sfx, actorToAttach->GetRootComponent());
}