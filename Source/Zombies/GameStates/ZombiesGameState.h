#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "ZombiesGameState.generated.h"

class AZombiesPlayerController;
class AZombiesSpawner;
class AZombiesCharacter;
class AZombiesPlayerCharacter;
class UDA_RoundsManager;
class USoundBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAllSpawnersAdded);

UCLASS(Blueprintable, BlueprintType)
class ZOMBIES_API AZombiesGameState : public AGameState
{
	GENERATED_BODY()

public:
	FOnAllSpawnersAdded OnOnAllSpawnersAddedDelegate;

private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UDA_RoundsManager> RoundsManagerDataAsset = nullptr;

	UPROPERTY()
	UDA_RoundsManager* RoundsManagerDA = nullptr;
	
	UPROPERTY()
	TMap<APlayerController*, UClass*> PlayerClassesMap;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<AZombiesPlayerCharacter>> PlayersToSpawnClasses;

	UPROPERTY()
	TArray<AZombiesPlayerCharacter*> PlayersDowned;
	
	UPROPERTY()
	TArray<AZombiesSpawner*> Spawners;

	UPROPERTY(Replicated)
	bool AllSpawnersReady = false;

public:
	/** When a Player is downed, we add it to the list and if all of them are downed, the game ends. */
	UFUNCTION()
	void AddDownedPlayerAndCheckEndGame_FromServer(AZombiesPlayerCharacter* playerDowned);
	UFUNCTION()
	void RemoveDownedPlayer_FromServer(AZombiesPlayerCharacter* playerRevived);
	
	UFUNCTION(BlueprintCallable)
	TArray<AZombiesPlayerController*> GetPlayerControllers();
	
	UFUNCTION(BlueprintCallable)
	TArray<AZombiesPlayerCharacter*> GetPlayerCharacters();

	/** Its the same as GetPlayerCharacters, but without casting the Pawns to AZombiesPlayerCharacter */
	UFUNCTION()
	TArray<APawn*> GetPlayerPawns();

	UFUNCTION()
	int32 GetNumberOfPlayers() const;
	
	UFUNCTION(BlueprintCallable)
	void AddSpawner(AZombiesSpawner* spawner);

	UFUNCTION(BlueprintCallable)
	void BroadCastSpawnersReady();
	
	UFUNCTION()
	void SpawnPlayer_FromServer(AZombiesPlayerController* playerController);

	UFUNCTION()
	bool GetAllSpawnersReady() const { return AllSpawnersReady; }

	/** Updates ALL the HUD for everyone when a new Player joins the game. */
	UFUNCTION()
	void UpdateAllHUD_FromServer();

	UFUNCTION()
	void UpdatePlayerScoreHUD_FromServer(FString& playerName, const float newScore);

	UFUNCTION()
	void UpdatePlayerLifeHUD_FromServer(FString& playerName, const float newLifePercentage);

	UFUNCTION(BlueprintCallable)
	bool CheckIsThisServer() const;

	UFUNCTION(NetMulticast, Reliable)
	void ReplicateImportant2dSFX_Multicast(USoundBase* sfx, float multiplier) const;
	UFUNCTION(NetMulticast, Unreliable)
	void ReplicateNotImportant3dSFX_Multicast(USoundBase* sfx, AActor* actorToAttach) const;
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	UFUNCTION()
	int32 GetValidSpawnerIndex_FromServer();

	UFUNCTION(BlueprintCallable)
	void StartRoundsManager();

	UFUNCTION()
	void InitializeRoundsManager();

	UFUNCTION(BlueprintImplementableEvent)
	void EndGame();
};
