#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ZombiesEnemyBaseController.generated.h"

class AZombiesPlayerCharacter;
class AZombiesEnemyCharacter;

UCLASS()
class ZOMBIES_API AZombiesEnemyBaseController : public AAIController
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	AZombiesEnemyCharacter* ControlledEnemy = nullptr;

	UPROPERTY(EditAnywhere, Category = "AI", meta = (AllowPrivateAccess = true))
	UBehaviorTree* BehaviorTree = nullptr;

public:
	virtual void OnPossess(APawn* InPawn) override;

	UFUNCTION()
	void EndBehavior() const;

	UFUNCTION()
	void SetTargetPlayerAndLocation(AZombiesPlayerCharacter* newTarget);
	UFUNCTION()
	void ClearTargetPlayerAndLocation();

	UFUNCTION()
	void SetIsAttacking(bool isAttacking);

	UFUNCTION(BlueprintCallable)
	AZombiesEnemyCharacter* GetControlledEnemy() { return ControlledEnemy; }
};
