#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ZombiesPlayerController.generated.h"

class UInputMappingContext;
class UInputAction;
class UEnhancedInputComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSpawnAndSetupPlayerFinished);

UCLASS()
class AZombiesPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
	FOnSpawnAndSetupPlayerFinished OnSpawnAndSetupPlayerFinishedDelegate;
	
	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext = nullptr;

private:
	UPROPERTY()
	UEnhancedInputComponent* EnhancedInputComponent = nullptr;

public:
	AZombiesPlayerController();
	
	UFUNCTION(BlueprintCallable)
	void SpawnAndSetupPlayer();

	virtual void SetupInputComponent() override;

	virtual void OnRep_Pawn() override;

	UFUNCTION(NetMulticast, Reliable)
	void UpdatePlayersHUD_Multicast(const TArray<FString>& names, const TArray<float>& scores);

	UFUNCTION(NetMulticast, Reliable)
	void UpdatePlayerPointsHUD_Multicast(const FString& playerName, const float newScore);

	UFUNCTION(NetMulticast, Reliable)
	void UpdatePlayerLifeHUD_Multicast(const FString& playerName, const float newLifePercentage);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void RemoveLobbyHUDAndSpawnPlayer_Blueprint();

	UEnhancedInputComponent* GetEnhancedInputComponent() const {return EnhancedInputComponent;}

protected:
	virtual void OnPossess(APawn* aPawn) override;

	/** Called when this Controller and its pawn have been set up on the start of the game.
	 * It broadcasts a Delegate to indicate it, so other components which are dependent of this Controller can set up. */
	UFUNCTION(Server, Reliable)
	void FinishedSettingUp_Server();

	UFUNCTION(NetMulticast, Reliable)
	void FinishedSettingUp_Multicast();

	/** Updates it when a new Player joins the game. */
	UFUNCTION(BlueprintImplementableEvent)
	void UpdatePlayersHUD_Blueprint(const TArray<FString>& names, const TArray<float>& scores);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdatePlayerPointsHUD_Blueprint(const FString& playerName, const float newScore);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdatePlayerLifeHUD_Blueprint(const FString& playerName, const float newLifePercentage);
	
	UFUNCTION()
	void OnGameStateAllSpawnersReady_FromServer();
	
	UFUNCTION()
	void SetupInputActionBindings();

private:
	UFUNCTION()
	void UpdateHUD_Internal();

	UFUNCTION(Server, Reliable)
	void SpawnPlayerWhenJoining_Server();

	UFUNCTION(Server, Reliable)
	void UpdatePlayerHUDOnStart_Server();

	UFUNCTION(Server, Reliable)
	void ChangeLobbyCameraToPlayCamera_Server();

	UFUNCTION(Client, Reliable)
	void ChangeLobbyCameraToPlayCamera_Client();
};
