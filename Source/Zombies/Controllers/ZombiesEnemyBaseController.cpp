#include "ZombiesEnemyBaseController.h"

#include "BrainComponent.h"
#include "Zombies/Characters/ZombiesEnemyCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"

void AZombiesEnemyBaseController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	AZombiesEnemyCharacter* enemyCharacter = Cast<AZombiesEnemyCharacter>(InPawn);
	if(enemyCharacter)
		ControlledEnemy = enemyCharacter;

	if(BehaviorTree)
		RunBehaviorTree(BehaviorTree);
}

void AZombiesEnemyBaseController::EndBehavior() const
{
	if(BehaviorTree)
		GetBrainComponent()->Cleanup();
}

void AZombiesEnemyBaseController::SetTargetPlayerAndLocation(AZombiesPlayerCharacter* newTarget)
{
	GetBlackboardComponent()->SetValueAsObject("TargetPlayer", newTarget);
	GetBlackboardComponent()->SetValueAsVector("TargetPlayerLocation", newTarget->GetActorLocation());
}

void AZombiesEnemyBaseController::ClearTargetPlayerAndLocation()
{
	GetBlackboardComponent()->ClearValue("TargetPlayer");
	GetBlackboardComponent()->ClearValue("TargetPlayerLocation");
}

void AZombiesEnemyBaseController::SetIsAttacking(bool isAttacking)
{
	GetBlackboardComponent()->SetValueAsBool("IsAttacking", isAttacking);
}
