#include "ZombiesPlayerController.h"

#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Zombies/Characters/ZombiesPlayerCharacter.h"
#include "Zombies/GameStates/ZombiesGameState.h"
#include "Zombies/Components/Players/ZombiesInputActionComponent.h"

AZombiesPlayerController::AZombiesPlayerController()
{
	bShowMouseCursor = false;
	DefaultMouseCursor = EMouseCursor::Default;
	bReplicates = true;
}

void AZombiesPlayerController::SpawnAndSetupPlayer()
{
	//Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}

	OnSpawnAndSetupPlayerFinishedDelegate.AddDynamic(this, &AZombiesPlayerController::FinishedSettingUp_Server);
	SpawnPlayerWhenJoining_Server();
	UpdatePlayerHUDOnStart_Server();
	ChangeLobbyCameraToPlayCamera_Server();
}

void AZombiesPlayerController::SpawnPlayerWhenJoining_Server_Implementation()
{
	if(GetPawn() == nullptr)
	{
		AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
			
		// If the spawners are ready, spawn a player. If not, do it as soon as they are ready.
		if(gameState)
		{
			if(gameState->GetAllSpawnersReady())
				gameState->SpawnPlayer_FromServer(this);
			else
				gameState->OnOnAllSpawnersAddedDelegate.AddDynamic(this, &AZombiesPlayerController::OnGameStateAllSpawnersReady_FromServer);
		}
	}
}

void AZombiesPlayerController::UpdatePlayerHUDOnStart_Server_Implementation()
{
	UpdateHUD_Internal();
}

void AZombiesPlayerController::ChangeLobbyCameraToPlayCamera_Server_Implementation()
{
	ChangeLobbyCameraToPlayCamera_Client();
}

void AZombiesPlayerController::ChangeLobbyCameraToPlayCamera_Client_Implementation()
{
	if(PlayerCameraManager && GetPawn())
		PlayerCameraManager->SetViewTarget(GetPawn());
}

void AZombiesPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent);
}

void AZombiesPlayerController::OnRep_Pawn()
{
	Super::OnRep_Pawn();
	
	if(GetLocalRole() == ROLE_AutonomousProxy)
		SetupInputActionBindings();
}

void AZombiesPlayerController::UpdatePlayersHUD_Multicast_Implementation(const TArray<FString>& names, const TArray<float>& scores)
{
	if(IsLocalPlayerController())
		UpdatePlayersHUD_Blueprint(names, scores);
}

void AZombiesPlayerController::UpdatePlayerPointsHUD_Multicast_Implementation(const FString& playerName, const float newScore)
{
	if(IsLocalPlayerController())
		UpdatePlayerPointsHUD_Blueprint(playerName, newScore);
}

void AZombiesPlayerController::UpdatePlayerLifeHUD_Multicast_Implementation(const FString& playerName, const float newLifePercentage)
{
	if(IsLocalPlayerController())
		UpdatePlayerLifeHUD_Blueprint(playerName, newLifePercentage);
}

void AZombiesPlayerController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);
	if(GetLocalRole() == ROLE_Authority && GetRemoteRole() == ROLE_SimulatedProxy)
	{
		SetupInputActionBindings();
		UpdateHUD_Internal();
	}
}

void AZombiesPlayerController::FinishedSettingUp_Server_Implementation()
{
	OnSpawnAndSetupPlayerFinishedDelegate.RemoveDynamic(this, &AZombiesPlayerController::FinishedSettingUp_Server);
	FinishedSettingUp_Multicast();
}

void AZombiesPlayerController::FinishedSettingUp_Multicast_Implementation()
{
	if(GetPawn())
	{
		AZombiesPlayerCharacter* playerCharacter = Cast<AZombiesPlayerCharacter>(GetPawn());
		if(playerCharacter)
			playerCharacter->SetHasFinishedSetup(true);
	}
}

void AZombiesPlayerController::OnGameStateAllSpawnersReady_FromServer()
{
	AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
	if(gameState)
	{
		gameState->SpawnPlayer_FromServer(this);
		gameState->OnOnAllSpawnersAddedDelegate.RemoveDynamic(this, &AZombiesPlayerController::OnGameStateAllSpawnersReady_FromServer);
	}
}

void AZombiesPlayerController::SetupInputActionBindings()
{
	if (EnhancedInputComponent)
	{
		TArray<UZombiesInputActionComponent*> inputComponents;
		const APawn* controlledPawn = GetPawn();
		if(controlledPawn)
		{
			GetPawn()->GetComponents(inputComponents);

			for(UZombiesInputActionComponent* inputActionComp : inputComponents)
			{
				inputActionComp->SetPlayerController(this);
				inputActionComp->BindInputAction(EnhancedInputComponent);
			}
		}
	}
}

void AZombiesPlayerController::UpdateHUD_Internal()
{
	AZombiesGameState* gameState = Cast<AZombiesGameState>(GetWorld()->GetGameState());
	if(gameState)
		gameState->UpdateAllHUD_FromServer();
}
