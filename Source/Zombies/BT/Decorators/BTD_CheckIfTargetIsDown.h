#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTD_CheckIfTargetIsDown.generated.h"


UCLASS()
class ZOMBIES_API UBTD_CheckIfTargetIsDown : public UBTDecorator
{
	GENERATED_BODY()

public:
	UBTD_CheckIfTargetIsDown();

protected:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	bool IsTargetDown(UBehaviorTreeComponent* OwnerComp) const;
};
