#include "BTD_CheckIfTargetIsDown.h"

#include "Zombies/Characters/ZombiesEnemyCharacter.h"
#include "Zombies/Components/Enemies/ZombiesEnemiesBaseManagementCmp.h"
#include "Zombies/Controllers/ZombiesEnemyBaseController.h"

UBTD_CheckIfTargetIsDown::UBTD_CheckIfTargetIsDown()
{
	bNotifyTick = true;
	NodeName = "Check if Target is down";
}

bool UBTD_CheckIfTargetIsDown::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	return IsTargetDown(&OwnerComp);
}

void UBTD_CheckIfTargetIsDown::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	if(IsInversed() == IsTargetDown(&OwnerComp))
		OwnerComp.RequestExecution(this);
}

bool UBTD_CheckIfTargetIsDown::IsTargetDown(UBehaviorTreeComponent* OwnerComp) const
{
	AZombiesEnemyBaseController* enemyController = Cast<AZombiesEnemyBaseController>(OwnerComp->GetAIOwner());
	if(enemyController)
	{
		const AZombiesEnemyCharacter* enemyCharacter = enemyController->GetControlledEnemy();
		if(enemyCharacter)
		{
			if(enemyCharacter->GetZombiesEnemiesBaseManagementCmp() && enemyCharacter->GetZombiesEnemiesBaseManagementCmp()->IsTargetCharacterDown())
				return true;
		}
	}
	return false;
}
