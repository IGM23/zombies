#include "BTD_CheckHasToChangeTarget.h"

#include "Zombies/Characters/ZombiesEnemyCharacter.h"
#include "Zombies/Components/Enemies/ZombiesEnemiesBaseManagementCmp.h"
#include "Zombies/Controllers/ZombiesEnemyBaseController.h"

UBTD_CheckHasToChangeTarget::UBTD_CheckHasToChangeTarget()
{
	bNotifyTick = true;
	NodeName = "Check Has To Change Target";
}

bool UBTD_CheckHasToChangeTarget::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	return HasToChangeTarget(&OwnerComp);
}

void UBTD_CheckHasToChangeTarget::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	if(IsInversed() == HasToChangeTarget(&OwnerComp))
		OwnerComp.RequestExecution(this);
}

bool UBTD_CheckHasToChangeTarget::HasToChangeTarget(UBehaviorTreeComponent* OwnerComp) const
{
	AZombiesEnemyBaseController* enemyController = Cast<AZombiesEnemyBaseController>(OwnerComp->GetAIOwner());
	if(enemyController)
	{
		const AZombiesEnemyCharacter* enemyCharacter = enemyController->GetControlledEnemy();
		if(enemyCharacter)
		{
			if(enemyCharacter->GetZombiesEnemiesBaseManagementCmp() && enemyCharacter->GetZombiesEnemiesBaseManagementCmp()->HasToChangeTarget())
			{
				enemyCharacter->GetZombiesEnemiesBaseManagementCmp()->ResetChangingTargetTimer();
				return true;
			}
		}
	}
	return false;
}
