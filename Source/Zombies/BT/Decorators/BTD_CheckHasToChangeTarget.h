#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTD_CheckHasToChangeTarget.generated.h"


UCLASS()
class ZOMBIES_API UBTD_CheckHasToChangeTarget : public UBTDecorator
{
	GENERATED_BODY()

public:
	UBTD_CheckHasToChangeTarget();

protected:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	bool HasToChangeTarget(UBehaviorTreeComponent* OwnerComp) const;

};
