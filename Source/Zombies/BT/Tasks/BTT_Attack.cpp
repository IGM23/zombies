#include "BTT_Attack.h"

#include "Zombies/Characters/ZombiesEnemyCharacter.h"
#include "Zombies/Components/ZombiesAttacksComponent.h"
#include "Zombies/Controllers/ZombiesEnemyBaseController.h"

UBTT_Attack::UBTT_Attack()
{
	bNotifyTick = true;
	bNotifyTaskFinished = true;
	bCreateNodeInstance = true;
	NodeName = "Attack";
}

EBTNodeResult::Type UBTT_Attack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AZombiesEnemyBaseController* enemyController = Cast<AZombiesEnemyBaseController>(OwnerComp.GetAIOwner());
	if(enemyController)
	{
		const AZombiesEnemyCharacter* enemyCharacter = enemyController->GetControlledEnemy();
		if(enemyCharacter)
		{
			AttacksComponent = enemyCharacter->GetAttacksComponent();
			if(AttacksComponent)
			{
				enemyCharacter->GetAttacksComponent()->UseRandomAttack_FromServer();
				return EBTNodeResult::InProgress;
			}
		}
	}
	
	return EBTNodeResult::Failed;
}

void UBTT_Attack::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	if(!AttacksComponent)
		FinishLatentTask(OwnerComp, EBTNodeResult::Failed);

	if(!AttacksComponent->GetIsAttacking())
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
}

void UBTT_Attack::OnTaskFinished(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, EBTNodeResult::Type TaskResult)
{
	AttacksComponent = nullptr;
}
