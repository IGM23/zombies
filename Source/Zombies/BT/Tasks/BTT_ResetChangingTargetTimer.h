#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_ResetChangingTargetTimer.generated.h"

UCLASS()
class ZOMBIES_API UBTT_ResetChangingTargetTimer : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTT_ResetChangingTargetTimer();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
