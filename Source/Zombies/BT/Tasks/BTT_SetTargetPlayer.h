#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_SetTargetPlayer.generated.h"

UCLASS()
class ZOMBIES_API UBTT_SetTargetPlayer : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTT_SetTargetPlayer();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
