#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_Attack.generated.h"

class UZombiesAttacksComponent;

UCLASS()
class ZOMBIES_API UBTT_Attack : public UBTTaskNode
{
	GENERATED_BODY()

	UPROPERTY()
	UZombiesAttacksComponent* AttacksComponent = nullptr;

public:
	UBTT_Attack();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	virtual void OnTaskFinished(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, EBTNodeResult::Type TaskResult) override;
};
