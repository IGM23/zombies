#include "BTT_SetTargetPlayer.h"

#include "Zombies/Characters/ZombiesEnemyCharacter.h"
#include "Zombies/Components/Enemies/ZombiesEnemiesBaseManagementCmp.h"
#include "Zombies/Controllers/ZombiesEnemyBaseController.h"

UBTT_SetTargetPlayer::UBTT_SetTargetPlayer()
{
	NodeName = "Set Target Player";
}

EBTNodeResult::Type UBTT_SetTargetPlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AZombiesEnemyBaseController* enemyController = Cast<AZombiesEnemyBaseController>(OwnerComp.GetAIOwner());
	if(enemyController)
	{
		const AZombiesEnemyCharacter* enemyCharacter = enemyController->GetControlledEnemy();
		if(enemyCharacter)
		{
			if(enemyCharacter->GetZombiesEnemiesBaseManagementCmp())
			{
				enemyCharacter->GetZombiesEnemiesBaseManagementCmp()->SearchForClosestPlayer();
				return EBTNodeResult::Succeeded;
			}
		}
	}
	
	return EBTNodeResult::Failed;
}
