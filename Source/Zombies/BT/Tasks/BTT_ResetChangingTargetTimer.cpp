#include "BTT_ResetChangingTargetTimer.h"

#include "Zombies/Characters/ZombiesEnemyCharacter.h"
#include "Zombies/Components/Enemies/ZombiesEnemiesBaseManagementCmp.h"
#include "Zombies/Controllers/ZombiesEnemyBaseController.h"

UBTT_ResetChangingTargetTimer::UBTT_ResetChangingTargetTimer()
{
	NodeName = "Reset Changing Target Timer";
}

EBTNodeResult::Type UBTT_ResetChangingTargetTimer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AZombiesEnemyBaseController* enemyController = Cast<AZombiesEnemyBaseController>(OwnerComp.GetAIOwner());
	if(enemyController)
	{
		const AZombiesEnemyCharacter* enemyCharacter = enemyController->GetControlledEnemy();
		if(enemyCharacter)
		{
			if(enemyCharacter->GetZombiesEnemiesBaseManagementCmp())
			{
				enemyCharacter->GetZombiesEnemiesBaseManagementCmp()->ResetChangingTargetTimer();
				return EBTNodeResult::Succeeded;
			}
		}
	}
	
	return EBTNodeResult::Failed;
}