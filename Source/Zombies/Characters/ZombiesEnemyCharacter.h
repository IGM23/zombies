#pragma once

#include "CoreMinimal.h"
#include "Zombies/Characters/ZombiesCharacter.h"
#include "ZombiesEnemyCharacter.generated.h"

class UZombiesEnemiesBaseManagementCmp;
class UCapsuleComponent;
class UZombiesEnemiesDeathComponent;

UCLASS()
class ZOMBIES_API AZombiesEnemyCharacter : public AZombiesCharacter
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CustomComponents|Enemies", meta = (AllowPrivateAccess = "true"))
	UCapsuleComponent* HeadHitboxComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CustomComponents|Enemies", meta = (AllowPrivateAccess = "true"))
	UZombiesEnemiesBaseManagementCmp* ZombiesEnemiesBaseManagementCmp;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CustomComponents|Enemies", meta = (AllowPrivateAccess = "true"))
	UZombiesEnemiesDeathComponent* EnemyDeathComponent;

public:
	AZombiesEnemyCharacter();

	UFUNCTION()
	UZombiesEnemiesBaseManagementCmp* GetZombiesEnemiesBaseManagementCmp() const { return ZombiesEnemiesBaseManagementCmp; }
	UFUNCTION(BlueprintImplementableEvent)
	UZombiesEnemiesDeathComponent* GetEnemyDeathComponent() const;

	virtual void TryToKillCharacter() override;

protected:
	virtual void BeginPlay() override;
};
