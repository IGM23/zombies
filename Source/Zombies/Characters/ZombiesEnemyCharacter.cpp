#include "ZombiesEnemyCharacter.h"

#include "Components/CapsuleComponent.h"
#include "Zombies/Components/Enemies/ZombiesEnemiesBaseManagementCmp.h"
#include "Zombies/Components/Enemies/ZombiesEnemiesDeathComponent.h"

AZombiesEnemyCharacter::AZombiesEnemyCharacter()
{
	HeadHitboxComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("HeadHitboxComponent"));
	HeadHitboxComponent->SetupAttachment(GetMesh());
	
	ZombiesEnemiesBaseManagementCmp = CreateDefaultSubobject<UZombiesEnemiesBaseManagementCmp>(TEXT("ZombiesEnemiesBaseManagementCmp"));
}

void AZombiesEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	EnemyDeathComponent = GetEnemyDeathComponent();
}

void AZombiesEnemyCharacter::TryToKillCharacter()
{
	EnemyDeathComponent->OnCharacterLifeReachesZero();
}
