#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ZombiesCharacter.generated.h"

class UZombiesCharStatsComponent;
class UZombiesInteractionComponent;
class UZombiesAttacksComponent;
class AZombiesEnemySpawnerArea;

UCLASS(Blueprintable, BlueprintType)
class AZombiesCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = CustomComponents, meta = (AllowPrivateAccess = "true"))
	UZombiesCharStatsComponent* CharStatsComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = CustomComponents, meta = (AllowPrivateAccess = "true"))
	UZombiesInteractionComponent* InteractionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = CustomComponents, meta = (AllowPrivateAccess = "true"))
	UZombiesAttacksComponent* AttacksComponent;

	/** Knowing in which area the characters are helps to do better spawning of the enemies. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	AZombiesEnemySpawnerArea* AreaCurrentlyIn = nullptr;

public:
	AZombiesCharacter();

	UFUNCTION()
	UZombiesCharStatsComponent* GetCharStatsComponent() const { return CharStatsComponent; }

	UFUNCTION()
	UZombiesInteractionComponent* GetInteractionComponent() const { return InteractionComponent; }

	UFUNCTION()
	UZombiesAttacksComponent* GetAttacksComponent() const { return AttacksComponent; }

	/** Called from Server. Will try to kill(enemy) or down(player) the character. */
	UFUNCTION()
	virtual void TryToKillCharacter() PURE_VIRTUAL(AZombiesCharacter::TryToKillCharacter, ;)

	UFUNCTION()
	AZombiesEnemySpawnerArea* GetAreaCurrentlyIn() const { return AreaCurrentlyIn; }
	UFUNCTION()
	void SetAreaCurrentlyIn(AZombiesEnemySpawnerArea* newArea) { AreaCurrentlyIn = newArea; }
};

