#include "ZombiesCharacter.h"

#include "UObject/ConstructorHelpers.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Zombies/Components/ZombiesAttacksComponent.h"
#include "Zombies/Components/ZombiesCharStatsComponent.h"
#include "Zombies/Components/ZombiesInteractionComponent.h"

AZombiesCharacter::AZombiesCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	bReplicates = true;

	CharStatsComponent = CreateDefaultSubobject<UZombiesCharStatsComponent>(TEXT("CharStatsComponent"));
	InteractionComponent = CreateDefaultSubobject<UZombiesInteractionComponent>(TEXT("InteractionComponent"));
	AttacksComponent = CreateDefaultSubobject<UZombiesAttacksComponent>(TEXT("AttacksComponent"));
}
