#include "ZombiesPlayerCharacter.h"

#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Zombies/Components/Players/ZombiesAutoHealComponent.h"
#include "Zombies/Components/Players/ZombiesPerksComponent.h"
#include "Zombies/Components/Players/ZombiesPlayerDeathComponent.h"
#include "Zombies/Components/Players/ZombiesZoomAimingComponent.h"
#include "Zombies/Components/Players/ZombiesUpdateUIComponent.h"
#include "Zombies/Components/Players/InputActions/ZombiesMoveActionComponent.h"
#include "Zombies/Components/Players/InputActions/ZombiesJumpActionComponent.h"
#include "Zombies/Components/Players/InputActions/ZombiesShootActionComponent.h"
#include "Zombies/Components/Players/InputActions/ZombiesAimZoomActionComponent.h"
#include "Zombies/Components/Players/InputActions/ZombiesChangeWeaponActionComp.h"
#include "Zombies/Components/Players/InputActions/ZombiesReloadWeaponActionComp.h"
#include "Zombies/Components/Players/InputActions/ZombiesInteractActionComponent.h"

AZombiesPlayerCharacter::AZombiesPlayerCharacter()
{
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = false;
	
	// Create a camera boom
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a camera
	PlayerCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("PlayerCamera"));
	PlayerCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	PlayerCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	
	// Create the input action components
	MoveInputComponent = CreateDefaultSubobject<UZombiesMoveActionComponent>(TEXT("MoveInputComponent"));
	JumpInputComponent = CreateDefaultSubobject<UZombiesJumpActionComponent>(TEXT("JumpInputComponent"));
	ShootInputComponent = CreateDefaultSubobject<UZombiesShootActionComponent>(TEXT("ShootInputComponent"));
	AimZoomInputComponent = CreateDefaultSubobject<UZombiesAimZoomActionComponent>(TEXT("AimZoomInputComponent"));
	ChangeWeaponInputComponent = CreateDefaultSubobject<UZombiesChangeWeaponActionComp>(TEXT("ChangeWeaponInputComponent"));
	ReloadWeaponInputComponent = CreateDefaultSubobject<UZombiesReloadWeaponActionComp>(TEXT("ReloadWeaponInputComponent"));
	InteractActionComponent = CreateDefaultSubobject<UZombiesInteractActionComponent>(TEXT("InteractActionComponent"));

	// Create other custom components
	AutoHealComponent = CreateDefaultSubobject<UZombiesAutoHealComponent>(TEXT("AutoHealComponent"));
	PerksComponent = CreateDefaultSubobject<UZombiesPerksComponent>(TEXT("PerksComponent"));
	ZoomAimingComponent = CreateDefaultSubobject<UZombiesZoomAimingComponent>(TEXT("ZoomAimingComponent"));
	RevivalAreaComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RevivalAreaComponent"));
	RevivalAreaComponent->SetupAttachment(RootComponent);
}

void AZombiesPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	PlayerDeathComponent = GetPlayerDeathComponent();
}

UZombiesUpdateUIComponent* AZombiesPlayerCharacter::GetUpdateUIComponent() const
{
	TArray<UZombiesUpdateUIComponent*> updateUIComponents;
	GetComponents(updateUIComponents);
	return updateUIComponents[0];
}

void AZombiesPlayerCharacter::SetHasFinishedSetup(bool isFinished)
{
	HasFinishedSetup = isFinished;
	OnPlayerFinishedSetupDelegate.Broadcast();
}

void AZombiesPlayerCharacter::TryToKillCharacter()
{
	PlayerDeathComponent->OnCharacterLifeReachesZero();
}
