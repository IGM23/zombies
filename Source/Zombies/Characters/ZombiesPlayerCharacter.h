#pragma once

#include "CoreMinimal.h"
#include "Zombies/Characters/ZombiesCharacter.h"
#include "ZombiesPlayerCharacter.generated.h"

class USphereComponent;
class UCameraComponent;
class USpringArmComponent;
class UZombiesMoveActionComponent;
class UZombiesJumpActionComponent;
class UZombiesShootActionComponent;
class UZombiesAimZoomActionComponent;
class UZombiesChangeWeaponActionComp;
class UZombiesReloadWeaponActionComp;
class UZombiesInteractActionComponent;
class UZombiesWeaponsManagerCmp;
class UZombiesAutoHealComponent;
class UZombiesPerksComponent;
class UZombiesZoomAimingComponent;
class UZombiesUpdateUIComponent;
class UZombiesPlayerDeathComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayerFinishedSetup);

UCLASS()
class ZOMBIES_API AZombiesPlayerCharacter : public AZombiesCharacter
{
	GENERATED_BODY()

public:
	FOnPlayerFinishedSetup OnPlayerFinishedSetupDelegate;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* PlayerCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputActionComponent, meta = (AllowPrivateAccess = "true"))
	UZombiesMoveActionComponent* MoveInputComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputActionComponent, meta = (AllowPrivateAccess = "true"))
	UZombiesJumpActionComponent* JumpInputComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputActionComponent, meta = (AllowPrivateAccess = "true"))
	UZombiesShootActionComponent* ShootInputComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputActionComponent, meta = (AllowPrivateAccess = "true"))
	UZombiesAimZoomActionComponent* AimZoomInputComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputActionComponent, meta = (AllowPrivateAccess = "true"))
	UZombiesChangeWeaponActionComp* ChangeWeaponInputComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputActionComponent, meta = (AllowPrivateAccess = "true"))
	UZombiesReloadWeaponActionComp* ReloadWeaponInputComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputActionComponent, meta = (AllowPrivateAccess = "true"))
	UZombiesInteractActionComponent* InteractActionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CustomComponents|Players", meta = (AllowPrivateAccess = "true"))
	UZombiesAutoHealComponent* AutoHealComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CustomComponents|Players", meta = (AllowPrivateAccess = "true"))
	UZombiesPerksComponent* PerksComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CustomComponents|Players", meta = (AllowPrivateAccess = "true"))
	UZombiesZoomAimingComponent* ZoomAimingComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CustomComponents|Players", meta = (AllowPrivateAccess = "true"))
	UZombiesPlayerDeathComponent* PlayerDeathComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CustomComponents|Players", meta = (AllowPrivateAccess = "true"))
	USphereComponent* RevivalAreaComponent;

	/** Indicates if the character has finished setting up and has a PlayerController attached.
	 * When this is true, other components which need the PlayerController's existence can be initialized. */
	UPROPERTY()
	bool HasFinishedSetup = false;

public:
	AZombiesPlayerCharacter();
	
	FORCEINLINE class UCameraComponent* GetPlayerCameraComponent() const { return PlayerCameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UFUNCTION(BlueprintCallable)
	UZombiesAutoHealComponent* GetAutoHealComponent() const { return AutoHealComponent; }
	UFUNCTION(BlueprintImplementableEvent)
	UZombiesWeaponsManagerCmp* GetWeaponsManagerComponent() const;
	UFUNCTION(BlueprintCallable)
	UZombiesPerksComponent* GetPerksComponent() const { return PerksComponent; }
	UFUNCTION(BlueprintCallable)
	UZombiesZoomAimingComponent* GetZoomAimingComponent() const { return ZoomAimingComponent; }
	UFUNCTION(BlueprintCallable)
	UZombiesUpdateUIComponent* GetUpdateUIComponent() const;
	UFUNCTION(BlueprintImplementableEvent)
	UZombiesPlayerDeathComponent* GetPlayerDeathComponent() const;
	UFUNCTION(BlueprintCallable)
	USphereComponent* GetRevivalAreaComponent() const { return RevivalAreaComponent; }

	UFUNCTION(BlueprintCallable)
	bool GetHasFinishedSetup() const { return HasFinishedSetup; }
	UFUNCTION(BlueprintCallable)
	void SetHasFinishedSetup(bool isFinished);

	virtual void TryToKillCharacter() override;

protected:
	virtual void BeginPlay() override;
};
