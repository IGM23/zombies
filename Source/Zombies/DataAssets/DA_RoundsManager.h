#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "DA_RoundsManager.generated.h"

class AZombiesEnemyCharacter;
class AZombiesEnemySpawnerArea;

UCLASS(Blueprintable, BlueprintType)
class ZOMBIES_API UDA_RoundsManager : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = "EnemySpawn")
	TArray<TSubclassOf<AZombiesEnemyCharacter>> EnemiesToSpawnClasses;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "EnemySpawn")
	TArray<TSoftObjectPtr<AZombiesEnemySpawnerArea>> EnemySpawnerAreas;

	/** This amount is per Player */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "RoundSetup", meta = (AllowPrivateAccess = "true"))
	int32 ZombiesNumberIncreasePerRound = 20;

	/** Every round, the zombies will multiply their last round health by this value. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "RoundSetup", meta = (AllowPrivateAccess = "true"))
	float ZombiesHealthGainPerRoundMultiplier = 1.2f;
	
	/** How many zombies can be alive spawned at the same time in the map. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "RoundSetup", meta = (AllowPrivateAccess = "true"))
	int32 MaximumZombiesAlive = 20;

	/** How many seconds it takes to start the next round. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "RoundSetup", meta = (AllowPrivateAccess = "true"))
	float SecondsBetweenRounds = 10.f;

	/** SFX Played at the start of the Round. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "RoundSetup|RoundSounds", meta = (AllowPrivateAccess = "true"))
	USoundBase* RoundStartSound = nullptr;

	/** SFX Played at the end of the Round. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "RoundSetup|RoundSounds", meta = (AllowPrivateAccess = "true"))
	USoundBase* RoundEndSound = nullptr;

	/** SFX Played when all players are downed and the game ends. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "RoundSetup|RoundSounds", meta = (AllowPrivateAccess = "true"))
	USoundBase* GameEndSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "RoundSetup|RoundSounds", meta = (AllowPrivateAccess = "true"))
	float RoundsSFXVolumeMultiplier = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "RoundSetup|ZombieSounds", meta = (AllowPrivateAccess = "true"))
	TArray<USoundBase*> ZombiesScreams;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "RoundSetup|ZombieSounds", meta = (AllowPrivateAccess = "true"))
	float TimeBetweenZombiesScreams = 5.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "RoundSetup|ZombieSounds", meta = (AllowPrivateAccess = "true"))
	float DeviationTimeBetweenZombieScreams = 1.f;

	/** For testing and debugging purposes. It enables/disables the Rounds Manager */
	UPROPERTY(EditAnywhere, Category = "Testing", meta = (AllowPrivateAccess = "true"))
	bool ActivateRoundsManager = true;
};
